# MasterThesisRepo

Repository containing data for the master thesis of the FIT CTU. This repository contains the data which is provided (on a media) to the FIT CTU along with the thesis.

Here is a description of the repository structure:
- _```./data```_ - Contains input data for the **Spritesheet manager** application
  - _```./data/frames to spritesheet```_ -  Data for when classic frames shall be converted to a sprite sheet. Does not involve isometric characters.
    - _```./data/frames to spritesheet/input```_ - The input data.
    - _```./data/frames to spritesheet/output*```_ - The expected output data.
  - _```./data/isometricCharacters```_ - Data for when frames of a isometric haracter shall be converted to multiple sprite sheets (for all 8 angles).
    - _```./data/isometricCharacters/player walk/input```_ - The input data.
    - _```./data/isometricCharacters/player walk/output```_ - The expected output data.

  - _```./data/floortile```_ - Data related to the floor tile creation
    - _```./data/floortile/floortiles.blend```_ - Blender file containing a scene from which floor tiles can be rendered
    - _```./data/floortile/output/*.png```_ - The resulting floortile textures
    
  - _```./data/cutTransparent```_ -  Data which can be used to cut off unnecessary transparent pixels from the image, using the **Spritesheet manager**
    - _```./data/cutTransparent/input/*```_ - The input file
    - _```./data/cutTransparent/output/*```_ - The expected output from the **Spritesheet manager**
    
- _```./bin```_ -        Contains executable form of the **Spritesheet manager**, and also blender files, which can be used for exporting the 3D model to a 2D images
  - _```./bin/blender/playerModel```_ - Model of a player
    - _```./bin/blender/playerModel/output_walk```_ - The expected output, after rendering the player's model's walk animation, using the rendering script. This serves as a reference for manual testing
  - _```./bin/blender/template```_ - A file which can be copy pasted to create any new model, and keep the scene setup
  - _```./bin/blender/test```_ - Contains blender file which can run the tests
  - _```./bin/blender/renderAllSides.py```_ - The rendering script, which is referred to from the blender files.
  - _```./bin/SpriteSheetManager1.0.2.exe```_ - The binary of the **Spritesheet manager** application. The newest version.
- _```./src```_ -         Contains the implementation source codes
  - _```./src/gamePrototype```_ -         Source code for the game prototype. This also can be run (as the game prototype is a small game server, which will run locally).
  - _```./src/spriteSheetManager```_ -         Source codes for the **Spritesheet manager** application
- _```./img```_ -         Image files, used in the gitlab's readme

- _```./changelog.md```_ - Contains a brief summary of teh changes done in the last commit
- _```./README.md```_ - The file you are reading right now
- _```./thesis/*.pdf```_ - The master's thesis, in a pdf format
  - _```./thesis/src```_ - The source codes for the master's thesis


A detailed manual for how to use the Blender rendering script and the Sprite sheet manager can be found in file `manual.md`.

# Spritesheet manager
> This version is developed for the **master thesis**, and it is simplified version of the full application. It does not contain various features, developed for personal game projects (such as automaton for processing output for WarShips game, and so on). This is just a note for me as an author of the code.

Spritesheet manager is an application, which processes various image output from **Blender** 3D software, and allows to create sprite sheets or **.json** files containing animation data.

## Troubleshooting
It can happen that an anti-virus program will terminate the application. One of the reasons is that the application is trying to overwrite a file (in which case an antivirus window popped up, informing that the computer was protected from this application).

It also can happen, that the Spritesheet manager crashes upon hitting the **Ok** button, with a Runtime exception.
When an antivirus was disabled, this problem disappeared.
As the runtime exception is related to the interop services (threading), and the crash happens when the application saves the image file, it appears that antivirus is monitoring the thread and disallows to save a file. However, there is no anti-virus message window or event, and the result is just a crash of an application, making it quite challenging to discover the reason for the problem.

# Blender rendering script
The script can be found in `./bin/blender/renderAllSides.py`, and was written for Blender of version **2.82**, but it shall be working even for the newer versions.
The output of the render is saved to the following directory:
 - Windows: `C:\tmp`
 - Linux: `/tmp`

# Game prototype
This project demonstrates the usage of procedural generation of textures, from the 3D models.

## How to run
In order to run the game, Linux machine is required. Also, `npm` appication must be installed on the system. 
Navigate to the `src/gamePrototype` directory. Then, run `npm install`, which downloads all necessary libraries. After that, execute `npm start` to run the server. The server will run on the localhost, on port `1234`, and can be acessed via web browser, at `127.0.0.1:1234`.

## The procedural texture generating 
The final ouput of such a process are files, which you can find here (for case of Player model):
  - assets/img/game/map/character/player/*.png
  - assets/animation/game/map/character/player/*.json

## Game tutorial
To move the main character, you have to perform a left-click on the game area. The goal of this demonstrative project is to collect all 3 brown bags, which are laying on the ground. 
