# Manual
This file contains a brief tutorial on how to use the blender rendering script, and 
also the Spritesheet manager application

## Using an existing 3D model in Blender and exporting to 2D images
This chapter explains how to use an existing model, provided in this repository and to export it to 2D images.
Open a provided file with an existing model. In this example, we will use the ```bin\blender\playerModel\*.blend``` file.
As all the camera positioning, a model position, origin reference object, and sizing is already set up, along with an animated, rigged model, you can directly navigate to the
`Animation` tab and make sure that you are in `DopeSheet` and `Action Editor`.

From the drop down menu, select an animation by name, as displayed in the following image:
![alt text](img/tut5.png "")

It is important to select an animation, which has `F` in front of the name. 
In this example, we choose an animation with the name `Walk`. Such an animation can be already selected, however, upon opening the file.

Set the end of the animation properly to the number of frames of your animation, as displayed in the following figure:

![alt text](img/tut7Marks.png "")

The number of frames can be discovered by moving the cursor to the last set of keyframes, and the cursor will display a number, with the total number of frames of the animation. In the case of the discussed figure, it is number `26`.
Such a number must be set as the `end` value, which can be seen in the right-bottom part of the figure.

Select the `Scripting` tab, and click on the arrow to run the script.

![alt text](img/tut8Mark.png "")

Please note that Blender will freeze until the rendering has finished. There seems to be no way to deal with that from the python script, so wait until the Blender window unfreezes.
The script will create a directory `C://tmp` (if not yet exists), and the resulting images will be saved there.
While the script is rendering, you may open the output directory and explore the images as they are being created.
If you want to change the output directory, you have to open the `renderAllSides.py` script, and at the beginning of the file, change the variable `outDir = 'C:\tmp';` value to the requested output directory. If the output directory does not exist, it is created by the Blender application, which runs the script.

## Creating a custom 3D model in Blender and exporting in to 2D images
This chapter explains how to create your 3D model, along with all the setup. This chapter expects you to create a 3D model, animate and rig it in Blender, and have some experience with Blender or 3D software.
To create a new model for a game, navigate to the `.\bin\blender\template` directory in the provided sources, and create a new copy of the `template` directory.
The new copy of the `template` directory must be in the `.\bin\blender` directory so that the relative path to the rendering script is valid.

Upon opening the copied `template.blend` file, you will see a scene with a cube, as displayed in the following figure:
![alt text](img/tut1.png "")

Delete the cube object, and create, texture, and rig your own model. Enter camera perspective mode by hitting the `0` key on a Numpad.
The created model must be facing left, as shown here:
![alt text](img/tut2.png "")

Also, your model must be inside the `modelCollection` collection.
In the illustration, the model `cow` is a child of the `ArmatureNew`, which is in the `modelCollection`.
The `template.blend` file also contains `originReference` object, which is a sphere with green material. Please do not delete the object and keep it in the scene. Also, change its position to mark the position of the feet of the model. The material of the object is not important. The only important thing is that the `originReference` object does not have transparent material.

If the model does not fit the camera view, move the camera further away (or closer) by changing the Z position of the camera object.
![alt text](img/tut3Mark.png "")

Press the `F12` key to preview the render. This allows you to see the result you got so far. The rendered output reflects how large the monster image will be in your final game (not considering the transparent pixels).

![alt text](img/tut4Mark.png "")

If you want to make the monster smaller or larger, there is no need to scale the model. You can only change the render resolution percentage, which can be seen on the right in the figure.

Select the `Animation` tab, and make sure that you are in `DopeSheet` and `Action Editor`

![alt text](img/tut5.png "")

Create a new action by clicking on the button displayed in the following image:
![alt text](img/tut6Mark.png "")

Then, animate your model. Make sure that the animation is linked to the model's armature before rendering. This is important in the case of having more animation actions for one model.
Then, every time before rendering, you need to select the animation from the action drop-down menu.
Now having the model, you can follow the tutorial described in the previous chapter.

## Creating a sprite sheet and animation data using Spritesheet manager
This chapter explains how to use the Spritesheet manager application to create final sprite sheets and animation data.

### Creating data for isometric characters
This chapter describes the process for the case when the input consists of 2D images, which contain some character, facing different directions. This is always output from the Blender rendering script described in this thesis. An example of such input is available in the `.\data\isometricCharacters\player walk\input` directory. This data can be used right away, when following this tutorial, if you do not want to render any 3D model to 2D images.

Run the application `SpriteSheetmanager1.0.2.exe`.
Select all the rendered images from the Blender, with exception of the `originReference.png` file, and drag and drop these files onto the application.

![alt text](img/tut9.png "Dragging the input images onto to the Spritesheet manager application")

It is crucial that when you are drag and dropping the files, you drag and drop the first file in the sequence. The reason is that windows will pass the file you drag with the mouse as the first one to the application event. This causes that all the files will be sorted by name in the application, except the first one. The application might sort the files automatically, but there are cases when the user wants to define the order of the files by himself by dragging and dropping the files manually, one by one. That is the reason why automatic sorting was not implemented in the application.

Tick the checkbox, which reads `Calculate origin`, and drag and drop the origin  image containing the green circle, onto the textbox.

![alt text](img/tut10.png "Spritesheet manager application interface")

The text in the textbox should change to the filename after drag and dropping it there successfully.

Go to the `Automatic angle processing` tab, and tick the checkbox, which reads `enable`.

![alt text](img/tut11.png "The Automatic angle processing tab")

This means that the application will later create a sprite sheet for each rotation angle of the character and calculate the relevant animation data, as discussed in this thesis.
If that checkbox would not be checked, then only one sprite sheet would be created from all the input data.
The `Number of frames of animation` is the number of images per one rotation of the animation.
As the input images are sequenced by numbers, have a look at the last image that was rendered to the final directory by Blender. Suppose that the last monster frame file has the name `7\_0026.png`. Because the first frame is numbered from 0, then the number of frames of the animation is `26 + 1`, which is `27`. Write the number into the discussed field in the Spritesheet manager.
If the number is incorrect, the application will display an error.

The `Spritesheet name prefix` defines how the final sprite sheet file name will start. This is only for orientation purposes. It is advised to use a text which will describe the animation, such as `walk`, `attack`, `death`, etc.

The resulting final files will be created in the current directory, from where the Spritesheet manager is running, overwriting any already existing files.
Suppose that prefix `run` was chosen in the previous step, then the following files will be created: `run.json`, `runD.png`, `runL.png`, `runR.png`, `runU.png`, `runLU.png`, `runLD.png`, `runRU.png`, `runRD.png`. If you are not sure, move the `SpriteSheetmanager1.0.2.exe` to a different directory, which is empty, and run it from there.

If the input images contain semi-transparent pixels, change the
`alpha sensitivity lower bound` value in the window.

Go to the `Default` tab again, and hit the `Ok` button.

### Creating data for any animation seen from only one angle
This chapter explains how to use the application to create an animation, which is not rotated to different angles. An example of such input can be seen in `.\data\frames to spritesheet\input`.

Drag and drop all the input images, with the exception of the `originReference.png` file, onto the application, as displayed in the following figure (the figure displays different image data, but the idea of the drag and dropping process is the same).

![alt text](img/tut9.png "Dragging the input images onto to the Spritesheet manager application")

As described in the chapter \nameref{sec:createIsoTut}, it is important to drag the first image with the mouse. If an origin point is calculated, you may provide the `originReference.png` file to the application in the same way as described in the chapter \nameref{sec:createIsoTut}.
In the directory described in this tutorial, there is no such file present, as it was not needed, so we will not check the ; `calculate origin` checkbox nor drag and drop the `originReference.png` file.
As this is non-isometric animation, navigate to the tab `Automatic angle processing` and make sure that the `enable` checkbox is unticked.
Then, navigate back to the `default` tab and press the `OK` button.
In this case, the result will be one `spriteSheet.png` and one `spriteSheet.json` file.

![alt text](img/outputFireball.png "Final sprite sheet for non-isometric animation")

### Cutting off transparent pixels from one image
This chapter explains how to use Spritesheet manager to cut off unnecessary transparent pixels from a single image.
Suppose that the input image is a couch, as displayed in following figure:

![alt text](img/couchExampleRender.png "")

This file can be found in `.\data\cutTransparent\input` directory.
Run the Spritesheet manager application. Drag and drop the one image onto the application, in the same way as described in previous chapters.
In this case, no origin reference point is needed, so `calculate origin` checkbox shall be unticked.
Similarly, in the tab `Automatic angle processing`, checkbox `enable` shall be unticked.
In tab `Default`, click the `OK` button.
In this case, the result will be one `spriteSheet.png` and one `spriteSheet.json` file.
As in this case, the `spriteSheet.json` file is not needed and can be deleted. However, if you choose to calculate the origin point, then the JSON file's data can be used later on.

### Expected limitations
This chapter discusses the expected limitations for the Spritesheet manager application. The application's main objective is to help the user create sprite sheets and animation data, which would be a very difficult task if done manually. Considering input data, sometimes the origin point might be outside the minimal bounding box, calculated by the application. However, there can be cases when this is the correct input. So the application processes it anyway.
In short, the user needs to input logical data to get a logical result.
