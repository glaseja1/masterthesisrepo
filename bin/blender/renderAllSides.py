import bpy, math, os
from pathlib import Path

MODEL_COLLECTION_NAME = 'modelCollection';
ORIGIN_REFERENCE = 'originReference';
MODEL_ANGLES = 8;
outDir = str(Path('/tmp')); #'C:\\tmp';
print(outDir);

class ExitOK(Exception):
    pass
class ExitError(Exception):
    pass


def hideRenderCollection(collectionName, visibilityStates, hide):
    """ For a collection name given, hides all child objects from render """
    if not collectionName in bpy.data.collections:
        print('No collection with name [{}] exists. This is not error of script. Check that the wished collection exists'.format(collectionName));
        raise ExitError;

    for obj in bpy.data.collections[collectionName].objects:
        #no need to go recursivelly, as the collection returns all objects recursivelly already
        hideRender(obj, visibilityStates, hide, False);

def revertHideRenderCollection(collectionName, visibilityStates):
    if not collectionName in bpy.data.collections:
        print('No collection with name [{}] exists. This is not error of script. Check that the wished collection exists'.format(collectionName));
        raise ExitError;

    for obj in bpy.data.collections[collectionName].objects:
        #no need to go recursivelly, as the collection returns all objects recursivelly already
        revertHideRender(obj, visibilityStates, False);

#hide / show all children recursivelly from render
def hideRender(ob, visibilityStates, hide, recursivelly = True):
    #save state
    visibilityStates[ob.name] = ob.hide_render;
    ob.hide_render = hide;

    if recursivelly:
        for child in ob.children:
            hideRender(child, visibilityStates, hide, recursivelly);

def revertHideRender(ob, visibilityStates, recursivelly = True):
    ob.hide_render = visibilityStates[ob.name];
    if recursivelly:
        for child in ob.children:
            revertHideRender(child, visibilityStates, recursivelly);



def hideAllRenderables():
    #hide origin reference object
    hideRender(bpy.data.objects[ORIGIN_REFERENCE], {}, True);
    #hide model
    hideRenderCollection(MODEL_COLLECTION_NAME, {}, True);

def renderModel():
    #hide all
    hideAllRenderables();
    #show model
    hideRenderCollection(MODEL_COLLECTION_NAME, {}, False);

    #save rotation
    prevRot = bpy.data.objects['cameraRoot'].rotation_euler;

    imagesTotal = MODEL_ANGLES;
    for i in range(imagesTotal):
        rotZ = -1* i * (360/imagesTotal) * (math.pi/180);

        print('[+] Current angle: ' + str(rotZ) + '°')
        bpy.data.scenes[0].render.filepath = str(Path(outDir + '/' + str(i) + '_'));

        #set rotation
        bpy.data.objects['cameraRoot'].rotation_euler = (
        prevRot[0],
        prevRot[1],
        rotZ);

        #render model
        bpy.ops.render.render( use_viewport = True, write_still=True, animation=True);

    #reset rotation
    bpy.data.objects['cameraRoot'].rotation_euler[2] = 0;


def renderOrigin():
    #hide all
    hideAllRenderables();
    #show origin reference object
    hideRender(bpy.data.objects[ORIGIN_REFERENCE], {}, False);
    bpy.data.scenes[0].render.filepath = str(Path(outDir + '/originReference'));
    #render
    bpy.ops.render.render(use_viewport = True, write_still=True, animation=False);


def main():
    modelCollectionVisiStates = {};
    originReferenceVisiStates = {};

    # show the collections in render to prevent further issues
    bpy.data.collections[MODEL_COLLECTION_NAME].hide_render = False;

    #acquire visibility states
    hideRender(bpy.data.objects[ORIGIN_REFERENCE], originReferenceVisiStates, False);
    hideRenderCollection(MODEL_COLLECTION_NAME, modelCollectionVisiStates, True);

    renderModel();
    renderOrigin();

    #revert visibility states
    #show origin reference
    revertHideRender(bpy.data.objects[ORIGIN_REFERENCE], originReferenceVisiStates);

    revertHideRenderCollection(MODEL_COLLECTION_NAME, modelCollectionVisiStates);
    raise ExitOK;

try:
    print("The output will be saved to:");
    print(outDir);
    main();
except ExitError:
    print("Script ended, but there was an error");
except ExitOK:
    print("Exiting");
