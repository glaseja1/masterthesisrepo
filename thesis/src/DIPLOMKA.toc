\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {subsection}{Citation of this thesis}{vi}{section*.1}% 
\contentsline {chapter}{Introduction}{1}{chapter*.5}% 
\contentsline {section}{The goal of the thesis}{2}{section*.6}% 
\contentsline {chapter}{\chapternumberline {1}Introduction to the topic}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Game engine}{4}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Visual representation of 2D games}{5}{section.1.2}% 
\contentsline {section}{\numberline {1.3}Graphical projection}{5}{section.1.3}% 
\contentsline {subsection}{\numberline {1.3.1}Isometric projection}{6}{subsection.1.3.1}% 
\contentsline {subsection}{\numberline {1.3.2}Dimetric projection}{6}{subsection.1.3.2}% 
\contentsline {subsection}{\numberline {1.3.3}Trimetric projection}{6}{subsection.1.3.3}% 
\contentsline {section}{\numberline {1.4}What is an isometric 2D game}{7}{section.1.4}% 
\contentsline {section}{\numberline {1.5}Blender}{8}{section.1.5}% 
\contentsline {section}{\numberline {1.6}Using 3D modeling tools for the creation of the isometric textures}{9}{section.1.6}% 
\contentsline {section}{\numberline {1.7}The problem of creation of the isometric textures}{9}{section.1.7}% 
\contentsline {chapter}{\chapternumberline {2}Survey of existing solutions}{11}{chapter.2}% 
\contentsline {subsection}{\numberline {2.0.1}Spine}{11}{subsection.2.0.1}% 
\contentsline {subsection}{\numberline {2.0.2}DragonBones}{12}{subsection.2.0.2}% 
\contentsline {subsection}{\numberline {2.0.3}Nima}{13}{subsection.2.0.3}% 
\contentsline {section}{\numberline {2.1}Summary}{13}{section.2.1}% 
\contentsline {chapter}{\chapternumberline {3}Realization}{15}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Used technology}{15}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Exporting a 3D model to 2D images}{16}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Setting up the Blender scene}{16}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}The rendering script}{20}{subsection.3.2.2}% 
\contentsline {section}{\numberline {3.3}Processing the Blender output}{21}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Creating a minified sprite sheet}{22}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}The algorithm}{23}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}The origin point}{23}{subsection.3.3.3}% 
\contentsline {subsection}{\numberline {3.3.4}Calculating the origin point}{25}{subsection.3.3.4}% 
\contentsline {subsection}{\numberline {3.3.5}Alpha sensitivity lower bound}{27}{subsection.3.3.5}% 
\contentsline {section}{\numberline {3.4}Other usage of the Spritesheet manager application}{27}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}Usage for a classic animation}{27}{subsection.3.4.1}% 
\contentsline {subsection}{\numberline {3.4.2}Cutting off the transparent pixels for a single image}{28}{subsection.3.4.2}% 
\contentsline {subsection}{\numberline {3.4.3}Creation of a floor tile}{29}{subsection.3.4.3}% 
\contentsline {chapter}{\chapternumberline {4}Testing}{33}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Blender rendering script}{33}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Spritesheet manager}{33}{section.4.2}% 
\contentsline {chapter}{\chapternumberline {5}Game prototype}{35}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Used technology}{35}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Loading the textures and animation data to a game}{36}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Animation JSON}{36}{section.5.3}% 
\contentsline {subsection}{\numberline {5.3.1}Variant for model facing in 8 directions}{37}{subsection.5.3.1}% 
\contentsline {subsection}{\numberline {5.3.2}Other variant}{38}{subsection.5.3.2}% 
\contentsline {section}{\numberline {5.4}Drawing the textures in a game}{38}{section.5.4}% 
\contentsline {chapter}{Conclusion}{39}{chapter*.13}% 
\contentsline {chapter}{Bibliography}{41}{chapter*.14}% 
\contentsline {appendix}{\chapternumberline {A}Acronyms}{43}{appendix.A}% 
\contentsline {appendix}{\chapternumberline {B}User manual}{45}{appendix.B}% 
\contentsline {section}{\numberline {B.1}Minimal requirements}{45}{section.B.1}% 
\contentsline {subsection}{\numberline {B.1.1}Spritesheet manager}{45}{subsection.B.1.1}% 
\contentsline {subsection}{\numberline {B.1.2}Exporting 3D model to 2D textures using Blender}{45}{subsection.B.1.2}% 
\contentsline {subsection}{\numberline {B.1.3}Game prototype}{46}{subsection.B.1.3}% 
\contentsline {section}{\numberline {B.2}Using an existing 3D model in Blender and exporting to 2D images}{47}{section.B.2}% 
\contentsline {section}{\numberline {B.3}Creating a 3D model in Blender and exporting in to 2D images}{48}{section.B.3}% 
\contentsline {subsection}{\numberline {B.3.1}Expected limitations}{51}{subsection.B.3.1}% 
\contentsline {section}{\numberline {B.4}Creating a floorile texture}{52}{section.B.4}% 
\contentsline {section}{\numberline {B.5}Creating a sprite sheet and animation data using Spritesheet manager}{54}{section.B.5}% 
\contentsline {subsection}{\numberline {B.5.1}Creating data for isometric characters}{54}{subsection.B.5.1}% 
\contentsline {subsection}{\numberline {B.5.2}Creating data for any animation seen from only one angle}{56}{subsection.B.5.2}% 
\contentsline {subsection}{\numberline {B.5.3}Cutting off transparent pixels from one image}{58}{subsection.B.5.3}% 
\contentsline {subsection}{\numberline {B.5.4}Expected limitations}{58}{subsection.B.5.4}% 
\contentsline {section}{\numberline {B.6}Running the game prototype}{59}{section.B.6}% 
\contentsline {section}{\numberline {B.7}Playing the game prototype}{59}{section.B.7}% 
\contentsline {section}{\numberline {B.8}Running the tests}{60}{section.B.8}% 
\contentsline {subsection}{\numberline {B.8.1}Blender rendering script}{60}{subsection.B.8.1}% 
\contentsline {subsection}{\numberline {B.8.2}Tests for the Spritesheet manager}{62}{subsection.B.8.2}% 
\contentsline {appendix}{\chapternumberline {C}Content of the data media, provided along with this thesis}{65}{appendix.C}% 
