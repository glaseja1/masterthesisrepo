﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpriteSheetManager.tests
{
    abstract class TestBase
    {
        protected bool assert(int expect, int actual, ref string output, string errMessage = "")
        {
            if (expect != actual)
            {
                if (errMessage != "")
                    errMessage = " - " + errMessage;
                throw new AssertFailException(String.Format("Expected {0} to equal {1}", actual, expect) + errMessage);

            }
            return true;
        }
        protected bool assert(string expect, string actual, ref string output, string errMessage = "")
        {
            if (expect != actual)
            {
                if (errMessage != "")
                    errMessage = " - " + errMessage;
                throw new AssertFailException(String.Format("Expected {0} to equal {1}", actual, expect) + errMessage);

            }
            return true;
        }
        protected bool assert(bool expect, bool actual, ref string output, string errMessage = "")
        {
            if (expect != actual)
            {
                if (errMessage != "")
                    errMessage = " - " + errMessage;
                throw new AssertFailException(String.Format("Expected {0} to equal {1}", actual, expect) + errMessage);

            }
            return true;
        }
        public abstract void run(ref string output);
    }

    [Serializable]
    class AssertFailException : Exception
    {
        public AssertFailException()
        {

        }

        public AssertFailException(string message)
            : base(String.Format("Assertion error: {0}", message))
        {

        }

    }
}
