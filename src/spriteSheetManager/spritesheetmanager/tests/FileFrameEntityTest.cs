﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpriteSheetManager.framesToSpriteSheet;
using System.Drawing;

namespace SpriteSheetManager.tests
{
    class FileFrameEntityTest : TestBase
    {
        private void testCalculateMinimalBoundingBox(ref string output)
        {
            //100 x 100 image
            Bitmap b = new Bitmap(100,100);
            Graphics g = Graphics.FromImage(b);
            //all transparent
            g.Clear(Color.Transparent);
            //one pixel on the top left corner
            g.FillRectangle(Brushes.Black, 0, 0, 1, 1);
            g.Flush();
            g.Dispose();
            FileFrameEntity ajs = new FileFrameEntity("PathDummy", b);
            this.assert(true, ajs.calculateMinimalBoundingBox(100), ref output);

            this.assert(0, ajs.boundingBox.X, ref output, "Minimal bounding box - X coordinate");
            this.assert(0, ajs.boundingBox.Y, ref output, "Minimal bounding box - Y coordinate");
            this.assert(1, ajs.boundingBox.Width, ref output, "Minimal bounding box - Width");
            this.assert(1, ajs.boundingBox.Height, ref output, "Minimal bounding box - Height");

            output += "  [+] test #1 - passed" + Environment.NewLine;

            //100 x 100 image
            b = new Bitmap(100, 100);
            g = Graphics.FromImage(b);
            //all transparent
            g.Clear(Color.Transparent);
            //one pixel on the top left corner
            g.FillRectangle(Brushes.Black, 14, 15, 1, 1);
            g.Flush();
            g.Dispose();
            ajs = new FileFrameEntity("PathDummy", b);
            this.assert(true, ajs.calculateMinimalBoundingBox(100), ref output);

            this.assert(14, ajs.boundingBox.X, ref output, "Minimal bounding box - X coordinate");
            this.assert(15, ajs.boundingBox.Y, ref output, "Minimal bounding box - Y coordinate");
            this.assert(1, ajs.boundingBox.Width, ref output, "Minimal bounding box - Width");
            this.assert(1, ajs.boundingBox.Height, ref output, "Minimal bounding box - Height");

            output += "  [+] test #2 - passed" + Environment.NewLine;

            output += "  [+] testCalculateMinimalBoundingBox() - passed" + Environment.NewLine;
        }
        public override void run(ref string output)
        {
            output += "[*] FileFrameEntityTest:" + Environment.NewLine;
            try
            {
                this.testCalculateMinimalBoundingBox(ref output);
            }
            catch (AssertFailException e)
            {
                output += "failed" + Environment.NewLine;
                output += Environment.NewLine;
                output += "Details:" + Environment.NewLine;
                output += e.Message + Environment.NewLine;
                output += Environment.NewLine;
            }
            output += Environment.NewLine;
        }
    }
}
