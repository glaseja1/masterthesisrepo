﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpriteSheetManager.tests
{
    public partial class Tester : Form
    {
        public Tester()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AnimationJsonStructTest t = new AnimationJsonStructTest();
            string text = "";
            t.run(ref text);
            FileFrameEntityTest t2 = new FileFrameEntityTest();
            t2.run(ref text);
            this.textBox1.Text = text;
        }
    }
}
