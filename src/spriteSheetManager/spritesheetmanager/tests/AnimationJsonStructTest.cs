﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace SpriteSheetManager.tests
{
    class AnimationJsonStructTest : TestBase
    {        
        private void testConstruction(ref string output)
        {
            AnimationJsonStruct ajs = new AnimationJsonStruct(5, 3, 1, 2);
            this.assert(3, ajs.frameH, ref output, "frameH");
            this.assert(5, ajs.frameW, ref output, "frameW");
            this.assert(2, ajs.framesTotal, ref output, "FramesTotal");
            this.assert(1, ajs.framesPerLine, ref output, "framesPerLine");
            output += "  [+] testConstruction() - passed" + Environment.NewLine;
        }
        private void testSaveAsJSON(ref string output)
        {
            output += "  [+] testSaveAsJSON() - ";
            AnimationJsonStruct ajs = new AnimationJsonStruct(5, 3, 1, 2);
            ajs.saveAsJSON("testfile.json");
            if (!File.Exists("testfile.json"))
            {
                throw new AssertFailException("The file 'testfile.json' was not created. Maybe there are insufficient rights for file creation?");
            }

            //read the file content:
            string text = File.ReadAllText("testfile.json");
            this.assert("{\r\n\"framesTotal\":2,\r\n\"frameSize\": [5, 3],\r\n\"framesPerLine\":1\r\n}\r\n", text, ref output, "testfile.json content");

            //cleanup
            File.Delete("testfile.json");

            //test whether the origin point is being saved aswell 
            ajs = new AnimationJsonStruct(5, 3, 1, 2);
            ajs.originPoint = new Point(12, 3);
            ajs.saveAsJSON("testfile.json");
            if (!File.Exists("testfile.json"))
            {
                throw new AssertFailException("The file 'testfile.json' was not created. Maybe there are insufficient rights for file creation?");
            }

            //read the file content:
            text = File.ReadAllText("testfile.json");
            this.assert("{\r\n\"framesTotal\":2,\r\n\"frameSize\": [5, 3],\r\n\"framesPerLine\":1\r\n,\r\n\"origin\":[12,3]\r\n}\r\n", text, ref output, "testfile.json content");

            //cleanup
            File.Delete("testfile.json");

            output += "passed" + Environment.NewLine;
        }
        public override void run(ref string output)
        {
            output += "[*] AnimationJsonStructTest:" + Environment.NewLine;
            try
            {
                this.testConstruction(ref output);
            }
            catch (AssertFailException e)
            {
                output += "failed" + Environment.NewLine;
                output += Environment.NewLine;
                output += "Details:" + Environment.NewLine;
                output += e.Message + Environment.NewLine;
                output += Environment.NewLine;
            }
            try
            {
                this.testSaveAsJSON(ref output);
            }
            catch (AssertFailException e)
            {
                output += "failed" + Environment.NewLine;
                output += Environment.NewLine;
                output += "Details:" + Environment.NewLine;
                output += e.Message + Environment.NewLine;
                output += Environment.NewLine;
            }
            output += Environment.NewLine;
        }
    }
}
