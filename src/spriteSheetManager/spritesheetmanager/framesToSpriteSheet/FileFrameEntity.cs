﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SpriteSheetManager.shared;

namespace SpriteSheetManager.framesToSpriteSheet
{
    /// <summary>
    /// Represents one image file, that is supposed to be one frame.
    /// Keeps the path to file, and precomputed minimal bounding box
    /// </summary>
    class FileFrameEntity : IDisposable
    {
        private bool disposed = false;
        private bool minimalBoundingBoxCalculated = false;
        private bool minimalBoundingBoxExists = false;
        private Rectangle minimalBoundingBox;
        /// <summary>
        /// The full path to the file
        /// </summary>
        private string path;
        /// <summary>
        /// The loaded image from the file
        /// </summary>
        private Bitmap image;

        public FileFrameEntity(string path, Bitmap image)
        {
            this.path = path;
            this.image = image;
            this.minimalBoundingBoxCalculated = false;
        }
        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //clean up managed objects
                }

                //clean up unmanaged objects
                this.image.Dispose();
                this.disposed = true;
            }
        }       
        /// <summary>
        /// If you call this when the bounding box is not calculated yet, then false is returned
        /// </summary>
        public bool existsBoundingBox
        {
            get
            {
                return this.minimalBoundingBoxExists;
            }
        }
        public Rectangle boundingBox
        {
            get
            {
                return this.minimalBoundingBox;
            }
        }
        public Bitmap imgBimap
        {
            get
            {
                return this.image;
            }
        }

        /// <summary>
        /// Calculates the minimal bounding rectangle, cutting off all transparent pixels
        /// </summary>
        /// <param name="alphaSensitivityLowBound"></param>
        /// <param name="forceRecalculate"></param>
        /// <returns>True, if any bounding box was found, false, if no bounding box was found for the image</returns>
        public bool calculateMinimalBoundingBox(int alphaSensitivityLowBound, bool forceRecalculate = false)
        {
            if (!forceRecalculate && this.isMinimalBoundingBoxCalculated())
                return this.minimalBoundingBoxExists;

            //iterate all pixels of image 
           
            //the smallest X such as, on the X position in any row is first non transparent pixel
            int smallestXFoundInAllRows = Int32.MaxValue;
            //the smallest Y such as, on the Y position in any column is first non transparent pixel
            int smallestYFoundInAllColumns = Int32.MaxValue;
            //the biggest X such as, on the X position in any row is last non transparent pixel, and after that X will not exist any other non transparent pixel in entire image
            int biggestXFoundInAllRows = 0;
            //the biggest Y such as, on the Y position in any column is last non transparent pixel, and after that Y will not exist any other non transparent pixel in entire image
            int biggestYFoundInAllRows = 0;
            for (int y = 0; y < this.image.Height; y++)
            {
                for (int x = 0; x < this.image.Width; x++)
                {
                    Color c = this.image.GetPixel(x, y);
                   System.Drawing.KnownColor cxx= c.ToKnownColor();
                   if (!Functions.isColorTransparent(c) && c.A > alphaSensitivityLowBound)
                    {
                        if (x < smallestXFoundInAllRows)
                            smallestXFoundInAllRows = x;
                        if (y < smallestYFoundInAllColumns)
                            smallestYFoundInAllColumns = y;

                        if (x > biggestXFoundInAllRows)
                            biggestXFoundInAllRows = x;
                        if (y > biggestYFoundInAllRows)
                            biggestYFoundInAllRows = y;
                    }
                }
            }
            this.minimalBoundingBox.X = smallestXFoundInAllRows;
            this.minimalBoundingBox.Y = smallestYFoundInAllColumns;
            this.minimalBoundingBox.Width = biggestXFoundInAllRows - smallestXFoundInAllRows + 1;
            this.minimalBoundingBox.Height = biggestYFoundInAllRows - smallestYFoundInAllColumns + 1;
            this.minimalBoundingBoxCalculated = true;

            this.minimalBoundingBoxExists = (smallestXFoundInAllRows != Int32.MaxValue);
            return this.minimalBoundingBoxExists;
        }
        /// <summary>
        /// Whether the minimal bounding rectngle is calculated already
        /// </summary>
        /// <returns></returns>
        public bool isMinimalBoundingBoxCalculated()
        {
            return this.minimalBoundingBoxCalculated;
        }
    }
}
