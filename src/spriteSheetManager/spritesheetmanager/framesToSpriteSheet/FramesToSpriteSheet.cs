﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using SpriteSheetManager.framesToSpriteSheet;
using SpriteSheetManager.shared;

namespace SpriteSheetManager.framesToSpriteSheet
{
    /// <summary>
    /// Allows to create a sprite sheet image from multiple input images
    /// </summary>
    class FramesToSpriteSheetProcessor : ImageInputProcessorBase
    {
        private int framesperLine = 10;
        public FramesToSpriteSheetProcessor()
        {     }       
        /// <summary>
        /// Runs the algorithm. For all input images, calculates the minimal common rectangle, 
        /// and then puts the images to one common sprite sheet
        /// </summary>
        /// <param name="spriteSheetName">The name of the final image sprite sheet</param>
        /// <param name="alphaSensitivityLowBound"></param>
        /// <param name="referencePointImagePath">Path to an image file, which depicts the reference point (if reference point shall be used at all)</param>
        /// <returns></returns>
        public AnimationJsonStruct createSpriteSheet(string spriteSheetName, 
            int alphaSensitivityLowBound,
            string referencePointImagePath = null)
        {
            if (this.files.Count == 0)
            {
                MessageBox.Show("There are no files to create sprite sheet from!", "fatal error");
                return null;
            }
            if (this.framesperLine > this.files.Count)
                this.framesperLine = this.files.Count;

            //sum the total width and height of final spritesheet
            Rectangle biggestCommonBoundingBoxFound = new Rectangle(Int32.MaxValue, Int32.MaxValue, 0, 0);

            //find biggest found bounding box for all frames
            foreach (FileFrameEntity e in this.files)
            {
                //will be calculated only when not yet calculated
                if (!e.calculateMinimalBoundingBox(alphaSensitivityLowBound))
                {//no bounding box can be calculated
                    MessageBox.Show("No bounding box could be calculated. Please, make sure that the alpha sensitivity lower bound value is not so high, and that the input image does not contain only transparent pixels", "fatal error");
                    return null;
                }

                if (e.boundingBox.X < biggestCommonBoundingBoxFound.X)
                    biggestCommonBoundingBoxFound.X = e.boundingBox.X;
                if (e.boundingBox.Y < biggestCommonBoundingBoxFound.Y)
                    biggestCommonBoundingBoxFound.Y = e.boundingBox.Y;
                if (e.boundingBox.Right > biggestCommonBoundingBoxFound.Width)
                    biggestCommonBoundingBoxFound.Width = e.boundingBox.Right;
                if (e.boundingBox.Bottom > biggestCommonBoundingBoxFound.Height)
                    biggestCommonBoundingBoxFound.Height = e.boundingBox.Bottom;
            }

            biggestCommonBoundingBoxFound.Width = biggestCommonBoundingBoxFound.Width - biggestCommonBoundingBoxFound.X + 1;
            biggestCommonBoundingBoxFound.Height = biggestCommonBoundingBoxFound.Height - biggestCommonBoundingBoxFound.Y + 1;


            int framesPerColumns = this.files.Count / this.framesperLine;
            if (this.files.Count % this.framesperLine > 0)
                framesPerColumns++;

            //create final sprite sheet image
            Bitmap spriteSheet = new Bitmap(biggestCommonBoundingBoxFound.Width * this.framesperLine, biggestCommonBoundingBoxFound.Height * framesPerColumns);
            Graphics g = Graphics.FromImage(spriteSheet);
            g.Clear(Color.Transparent);

            Point frameIndexer = new Point();
            //draw the frames to sprite sheet
            foreach (FileFrameEntity e in this.files)
            {
                g.DrawImage(e.imgBimap,
                    new Rectangle(
                        frameIndexer.X * biggestCommonBoundingBoxFound.Width,
                        frameIndexer.Y * biggestCommonBoundingBoxFound.Height,
                        biggestCommonBoundingBoxFound.Width,
                        biggestCommonBoundingBoxFound.Height),
                        biggestCommonBoundingBoxFound,
                        GraphicsUnit.Pixel);
                frameIndexer.X++;
                if (frameIndexer.X >= framesperLine)
                {
                    frameIndexer.X = 0;
                    frameIndexer.Y++;
                }
            }
            g.Flush();
            spriteSheet.Save(spriteSheetName);
            spriteSheet.Dispose();

            //force freeing memory
            System.GC.Collect();
            AnimationJsonStruct animJson = new AnimationJsonStruct(
                biggestCommonBoundingBoxFound.Width,
                biggestCommonBoundingBoxFound.Height,
                this.framesperLine,
                this.files.Count
                );


            //calculate the origin for that angle
            if (referencePointImagePath != null)
            {
                bool success = false;
                animJson.originPoint = this.calculateOriginPoint(referencePointImagePath, biggestCommonBoundingBoxFound, alphaSensitivityLowBound, out success);
                if (!success)
                    return animJson;
                //MessageBox.Show("Calculated origin point is: " + animJson.originPoint.ToString());
            }
            return animJson;
        }
        public void printHelp()
        {
            MessageBox.Show("Let A be input image, which may contain transparent images. For all input images A, the minimum common bounding box is computed. (finds smallest rectangle to cut off the redundant transparent pixels). Then, the frame images are put to one image sprite sheet.");
        }
        public void setMaxAmountOfFramesPerLine(int n)
        {
            if(n > 0)
                this.framesperLine = n;
        }
    }
}
