﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace SpriteSheetManager.shared
{
    class Functions
    {
        private static Point getMinXY(Bitmap b)
        {
            int minX = int.MaxValue;
            int minY = int.MaxValue;

            for (int y = 0; y < b.Height; y++)
            {
                for (int x = 0; x < b.Width; x++)
                {
                    Color c = b.GetPixel(x, y);
                    if (!Functions.isColorTransparent(c))
                    {
                        if (x < minX)
                            minX = x;
                        if (y < minY)
                            minY = y;
                        break;
                    }
                }
            }
            return new Point(minX, minY);
        }
        public static Rectangle getRectangleAround(Bitmap b,
            int maxTransparentSpaceInPixels = 0)
        {
            Rectangle rec = new Rectangle();

            int TransparentPixelsInRowFound = 0;

            Point minP = getMinXY(b);
            
            int maxX = int.MinValue;
            int maxY = int.MinValue;

            for (int y = minP.Y; y < b.Height; y++)
            {
                for (int x = minP.X; x < b.Width; x++)
                {
                    Color c = b.GetPixel(x, y);
                    if (Functions.isColorTransparent(c))
                    {
                        TransparentPixelsInRowFound++;
                        if (TransparentPixelsInRowFound > maxTransparentSpaceInPixels)
                        {
                            if (x > maxX)
                                maxX = x;
                            if (y > maxY)
                                maxY = y;

                            TransparentPixelsInRowFound = 0;
                        }
                    }
                }
            }

            rec.X = minP.X;
            rec.Y = minP.Y;
            rec.Width = maxX - rec.X;
            rec.Height = maxY - rec.Y;
            return rec;
        }
        public static bool isColorTransparent(Color c)
        {
            return (c.R == c.G && c.G == c.B) && c.A == 0 && (c.R == 255 || c.B == 0);
        }
        public static void printNoSuchFileDir(string p)
        {
            MessageBox.Show(p + " no such file or directory");
        }

    }
}
