﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpriteSheetManager.framesToSpriteSheet;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SpriteSheetManager.shared
{
    /// <summary>
    /// Represents a base class for any class, that takes some files (images)
    /// as input, and then processses them in some way.
    /// Provides some functions, and variables, common for all such classes
    /// </summary>
    class ImageInputProcessorBase : IDisposable
    {
        private bool disposed = false;
        /// <summary>
        /// The loaded files, that are about to be processed
        /// </summary>
        protected List<FileFrameEntity> files = new List<FileFrameEntity>();

        protected ImageInputProcessorBase()
        {}
        /// <summary>
        /// given a path to an image containing the origin point mark, 
        /// will calculate and approximate the origin point
        /// </summary>
        /// <param name="referencePointFilePath"></param>
        /// <param name="biggestCommonBoundingBoxFound"></param>
        /// <param name="alphaSensitivityLowBound"></param>
        /// <returns>the origin point, relative to the image size</returns>
        protected Point calculateOriginPoint(
            string referencePointFilePath,
            Rectangle biggestCommonBoundingBoxFound,
            int alphaSensitivityLowBound,
            out bool success)
        {
            success = true;
            Bitmap b = new Bitmap(Image.FromFile(referencePointFilePath));

            //we use the existing algorithm, so just create a dummy instance...
            FileFrameEntity entity = new FileFrameEntity(referencePointFilePath, b);
            if (!entity.calculateMinimalBoundingBox(alphaSensitivityLowBound, true))
            {//no bounding box can be calculated
                MessageBox.Show("No bounding box could be calculated. Please, make sure that the alpha sensitivity lower bound value is not so high, and that the input image does not contain only transparent pixels", "fatal error");
                success = false;
                return new Point(0, 0);
            }

            //get center point of the found bounding box
            Point center = new Point(entity.boundingBox.X + entity.boundingBox.Width / 2,
                entity.boundingBox.Y + entity.boundingBox.Height / 2);

            //recalculate the origin point with respect to the frame's found minimal bounding box...
            //if the origin point does not lie inside the rectangle, it is not necesarrily a bad thing, and it might actually happen
            /*
             if(!biggestCommonBoundingBoxFound.Contains(center))
             {
                 MessageBox.Show("The origin point does not lay inside the calculated minimal bounding rectangle for the frames.");
             }*/
            Point origin = new Point(center.X - biggestCommonBoundingBoxFound.X,
                center.Y - biggestCommonBoundingBoxFound.Y);
            b.Dispose();
            return origin;
        } 
        /// <summary>
        /// Checks whether the file with the given path really exists. 
        /// </summary>
        /// <param name="f"></param>
        /// <returns>Whether the file exists or not</returns>
        public bool checkIfFileExistsAndPrintMessage(string f)
        {
            if (!File.Exists(f))
            {
                Functions.printNoSuchFileDir(f);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Clears all loaded files (this class will have no input files, until added again)
        /// </summary>
        public void clearFiles()
        {
            foreach (var v in this.files)
                v.Dispose();
            this.files.Clear();
        }
        /// <summary>
        /// Implement IDisposable.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    //clean up managed objects
                }

                //clean up unmanaged objects
                this.clearFiles();
                this.disposed = true;
            }
        }       
        /// <summary>
        /// Loads list of files to be processed
        /// </summary>
        /// <param name="files">List with absolute paths to such files</param>
        /// <returns></returns>
        public bool loadFiles(List<string> files)
        {
            foreach (string f in files)
            {
                //check for file existment
                if (!this.checkIfFileExistsAndPrintMessage(f))
                    return false;
                //create entity and convert to bitmap, to be able to perform pixel operations
                FileFrameEntity e = new FileFrameEntity(f, new Bitmap(Image.FromFile(f)));
                this.files.Add(e);
            }
            return true;
        }
    }
}
