﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using SpriteSheetManager.shared;

namespace SpriteSheetManager
{
    class SpriteSheetToFrames
    {
        public static void printHelp()
        {
            MessageBox.Show("Used for managing incorrectly created spritesheets." + Environment.NewLine +
                "It tries to find probable frames, and saves them all one by one as images to target folder for further processsion.");
        }
        public static bool process(string spritePath, string outputDir)
        {
            if (!File.Exists(spritePath))
            {
                Functions.printNoSuchFileDir(spritePath);
                return false;
            }
            if (!Directory.Exists(outputDir))
            {
                Directory.CreateDirectory(outputDir);
                return false;
            }



            Bitmap b = new Bitmap(Image.FromFile(spritePath));
            for (int y = 0; y < b.Height; y++)
            {
                for (int x = 0; x < b.Width; x++)
                {
                    Color c = b.GetPixel(x, y);
                    if (!Functions.isColorTransparent(c))
                    {
                        Rectangle r = Functions.getRectangleAround(b);

                        if (r.Width < 1 || r.Height < 1)
                            continue;

                        x += r.Width;
                        
                        Bitmap newB = new Bitmap(r.Width, r.Height);
                        Graphics g = Graphics.FromImage(newB);

                        g.Clear(Color.Transparent);
                        g.DrawImage(b, new Rectangle(0, 0, r.Width, r.Height),
                            new Rectangle(0, 0, r.Width, r.Height),
                            GraphicsUnit.Pixel);

                        newB.Save(Path.Combine(outputDir, "1.png"));

                        break;
                    }
                }
            }
            return true;
        }
    }
}
