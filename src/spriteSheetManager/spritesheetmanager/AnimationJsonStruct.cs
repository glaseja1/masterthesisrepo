﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace SpriteSheetManager
{
    /// <summary>
    /// Contains info about the animation 
    /// </summary>
    class AnimationJsonStruct
    {
        /// <summary>
        /// The height of the frame in pixels
        /// </summary>
        public int frameH;
        /// <summary>
        /// The width of the frame in pixels
        /// </summary>
        public int frameW;
        public int framesPerLine;
        public int framesTotal;
        /// <summary>
        /// Only not null in case when origin point is calculated
        /// </summary>
        public Point? originPoint = null;

        public AnimationJsonStruct(int W, int H, int framesPerLine, int framesTotal)
        {
            this.frameW = W;
            this.frameH = H;
            this.framesTotal = framesTotal;
            this.framesPerLine = framesPerLine;
        }
        /// <summary>
        /// Saves the information to separate json file.
        /// If the file exists, it is overwritten
        /// </summary>
        /// <param name="jsonName">If it does not end with .json, it is added</param>
        public void saveAsJSON(string jsonName)
        {
            if (!jsonName.EndsWith(".json"))
                jsonName += ".json";

            StreamWriter sr = new StreamWriter(jsonName);
            sr.WriteLine("{");
            sr.WriteLine("\"framesTotal\":" + this.framesTotal + ",");
            sr.WriteLine("\"frameSize\": [" + this.frameW + ", " + this.frameH + "],");
            sr.WriteLine("\"framesPerLine\":" + this.framesPerLine);

            if(this.originPoint.HasValue)
            {
                sr.WriteLine(",");
                sr.WriteLine("\"origin\":[" + this.originPoint.Value.X + "," + this.originPoint.Value.Y + "]");
            }
            sr.WriteLine("}");
            sr.Close();
        }
        /// <summary>
        /// appends the information to existing json file.
        /// </summary>
        /// <param name="jsonName">If it does not end with .json, it is added</param>
        public void appendToJSON(string jsonName, string angleIdentifier, bool isLast)
        {
            if (!jsonName.EndsWith(".json"))
                jsonName += ".json";

            bool didFileExist = File.Exists(jsonName);

            StreamWriter sr = new StreamWriter(jsonName, true);

            if (!didFileExist)
            {
                sr.WriteLine("{");
                sr.WriteLine("\"animationSpeed\": " + 100 + ",");
            }
            sr.WriteLine("\"" + angleIdentifier + "\": {");
            sr.WriteLine("\"framesTotal\": " + this.framesTotal + ",");
            sr.WriteLine("\"frameSize\": [" + this.frameW + ", " + this.frameH + "],");
            sr.WriteLine("\"framesPerLine\" :" + this.framesPerLine); 
            
            if (this.originPoint.HasValue)
            {
                sr.WriteLine(",");
                sr.WriteLine("\"origin\":[" + this.originPoint.Value.X + "," + this.originPoint.Value.Y + "]");
            }

            if (!isLast)
                sr.WriteLine("},");
            else
            {
                sr.WriteLine("}");
                sr.WriteLine("}");
            } sr.Close();
        }
    }
}
