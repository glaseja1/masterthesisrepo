﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SpriteSheetManager.framesToSpriteSheet;
using System.IO;
using SpriteSheetManager.tests;

namespace SpriteSheetManager
{
    public partial class Form1 : Form
    {
        public static string[] angleNames =
          new string[]{
                 "L", "LD", "D", "RD", "R", "RU", "U", "LU"
           };
        /// <summary>
        /// The version of the application
        /// </summary>
        public const string VERSION = "1.0.2 - stable, master thesis version";

        List<string> files = new List<string>();
        /// <summary>
        /// Not null only if origin point is supposed to be calculated
        /// </summary>
        string originImageFilePath = null;

        FramesToSpriteSheetProcessor framesToSpriteSheet;
        public Form1()
        {
            InitializeComponent();

            this.Text = "version: " + Form1.VERSION;
            this.framesToSpriteSheet = new FramesToSpriteSheetProcessor();
        }

        /// <summary>
        /// When user wants to put frame images to one sprite sheet + create animation data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            bool calculateOrigin = checkBox2.Checked;

            //if origin shall be calculated, validate path
            if (calculateOrigin && !File.Exists(this.originImageFilePath))
            {
               MessageBox.Show("Origin image file was not found at path '" + this.originImageFilePath + "'. Stop");
               return;                    
            }
            
            int alphaSensitivityLowBound = (int)this.numericUpDown3.Value;
            string namePrefix = textBox2.Text;
            this.framesToSpriteSheet.clearFiles();
            
            //if animation shall be created for isometric, 8 - sided game
            if (this.checkBox1.Checked)
            {
                int framesForAnimation = (int)this.numericUpDown2.Value;
                if (this.files.Count % framesForAnimation != 0)
                {
                    MessageBox.Show("Incorrect number of frames for animation was entered, or some input files are missing...");
                    return;
                }
                if (this.files.Count / framesForAnimation != 8)
                {
                    MessageBox.Show("Incorrect number of files. There are 8 sides for renderring animation, so 8 sets of images are needed. Total amount of files / number of frames per animation must be equal to 8");
                    return;
                }

                if (File.Exists(namePrefix + ".json"))
                    File.Delete(namePrefix + ".json");

                for (int i = 0; i < angleNames.Length; i++)
                {
                    List<string> f1 = this.files.GetRange(i * framesForAnimation, framesForAnimation);
                    this.framesToSpriteSheet.clearFiles();
                    this.framesToSpriteSheet.loadFiles(f1);
                    var struc = this.framesToSpriteSheet.createSpriteSheet(namePrefix + angleNames[i] + ".png",
                        alphaSensitivityLowBound, this.originImageFilePath);

                    //create / append to .json file
                    
                    if(struc != null)
                        struc.appendToJSON(namePrefix + ".json", angleNames[i], i == angleNames.Length - 1);
                }
            }
            else
            {                
                //load all files
                this.framesToSpriteSheet.clearFiles();
                this.framesToSpriteSheet.loadFiles(files);
                var struc1 = this.framesToSpriteSheet.createSpriteSheet("spriteSheet.png",
                    alphaSensitivityLowBound, this.originImageFilePath);
                if(struc1 != null)
                    struc1.saveAsJSON("spriteSheet.json");
            }
        }
        /// <summary>
        /// Help button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.framesToSpriteSheet.printHelp();
        }
        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            files = new List<string>();

            string[] arr = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string sas in arr)
                files.Add(sas);

            /*files.Sort(delegate(string p2, string p1)
            {
                return string.Compare(p2, p1);
            }
                );*/

            foreach (string file in files)
                listView1.Items.Add(Path.GetFileName(file));
        }
        /// <summary>
        /// Common callback for any file drag enter event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void fileDropDragEnterCallback(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
        }
        void OriginImageGroupBox_DragDrop(object sender, DragEventArgs e)
        {
            string[] arr = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (arr.Length == 1)
            {
                this.textBox3.Text = arr[0];
                this.originImageFilePath = arr[0];
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            this.AllowDrop = true;
            this.DragEnter += new DragEventHandler(Form1_DragEnter);
            this.DragDrop += new DragEventHandler(Form1_DragDrop);

            //drop allowed when origin calculation is enabled
            this.groupBox2.DragEnter += new DragEventHandler(this.fileDropDragEnterCallback);
            this.groupBox2.DragDrop += new DragEventHandler(OriginImageGroupBox_DragDrop);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.framesToSpriteSheet.clearFiles();
            files.Clear();
            listView1.Items.Clear();
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this.framesToSpriteSheet.setMaxAmountOfFramesPerLine((int)numericUpDown1.Value);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Application will split input files to groups by X, where X is sumber of frames given below. Then, will create a sprite sheet and .json animation info file for each group, naming it in order as *.L.png, *.LD.png, *.D.png *.RD.png *.R.png, *.RU.png, *.U.png and *.LU.png");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            groupBox1.Enabled = ((CheckBox)sender).Checked;
        }
        /// <summary>
        /// [calculate origin] checkbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = ((CheckBox)sender).Checked;
            this.groupBox2.AllowDrop = groupBox2.Enabled;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tester t = new Tester();
            t.Show();
        }
    }
}
