import * as ECSA from '../../libs/pixi-component';
import * as PIXI from 'pixi.js';
import { Player } from '../game/characters/player/Player';
import { DamageEnt } from '../game/characters/damage/DamageEnt';
import { Assets } from '../Constants';
import { PlayerAnimation } from '../anim/PlayerAnimation';

/**
 * Allows to create a player
 */
export class DamageFactory {
  public static loader: PIXI.Loader;
  public static init(loader: PIXI.Loader) {
    this.loader = loader;
  }
  public static createDamageFor(owner: ECSA.GameObject, value: number): void {
    const style = new PIXI.TextStyle({
      fontFamily: 'Arial',
      fontSize: 12,
      fill: '#fff'
    });

    var textObj = new ECSA.Builder(owner.scene)
      //.anchor(0, -1)
      .scale(1)
      .asText('name', `${value}`, style);

    var textureName: string = value != 0 ? 'damage' : 'block';
    if (value != 0) {
      var obj = new ECSA.Builder(owner.scene)
        .withComponent(new DamageEnt())
        //.anchor(-2.0, -3)
        .scale(1)
        .withParent(owner)
        .asSprite(PIXI.Texture.from(textureName))
        .withChild(textObj)
        .build();
    }
    else {
      var obj = new ECSA.Builder(owner.scene)
        .withComponent(new DamageEnt())
        //.anchor(-2.0, -3)
        .scale(1)
        .withParent(owner)
        .asSprite(PIXI.Texture.from(textureName))
        .build();
    }
  }
}
