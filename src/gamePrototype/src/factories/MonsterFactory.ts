import * as ECSA from '../../libs/pixi-component';
import * as PIXI from 'pixi.js';
import { Monster } from '../game/characters/monsters/Monster';
import { Assets } from '../Constants';
import { MonsterAnimation } from '../anim/MonsterAnimation';
import { Names } from '../Constants';
import { Character } from '../game/characters/Character';
import { PlayerFactory } from './PlayerFactory';
import { HealthBarFactory } from './HealthBarFactory';

/**
 * Allows to create a monster
 */
export class MonsterFactory {
  /**
   * Number 0 is dedicated for the player...
   */
  public static idAssigner: number = 1;
  public static loader: PIXI.Loader;
  public static init(loader: PIXI.Loader) {
    this.loader = loader;
  }
  public static createMonster(scene: ECSA.Scene): void {
    var p: Monster = new Monster(this.idAssigner++);

    var obj = new ECSA.Builder(scene)
      .withComponent(p)
      .withComponent(new MonsterAnimation())
      //.anchor(0.5, 0.5)
      .scale(1)
      .withParent(scene.stage.asContainer())
      .asSprite(PIXI.Texture.from(`${Assets.RESOURCE_IDLE}D`))
      .build();

    p.setPositionToVAI(4, 0);
    obj.pixiObj.interactive = true;
    obj.pixiObj.on('click', function() {
      console.warn('attack action')
      //  let pcPlayer = scene.findObjectByName(Names.PC_PLAYER);
      PlayerFactory.pcPlayer.attack(p as Character);
    });
    //NOT USED...DISSICULT TO MAKE WORK IN PIXI
    //HealthBarFactory.createFor(obj);
  }
}
