import * as ECSA from '../../libs/pixi-component';
import * as PIXI from 'pixi.js';
import { Player } from '../game/characters/player/Player';
import { Assets, Names } from '../Constants';
import { PlayerAnimation } from '../anim/PlayerAnimation';

/**
 * Allows to create a player
 */
export class PlayerFactory {
  public static loader: PIXI.Loader;
  public static pcPlayer: Player = null;
  public static init(loader: PIXI.Loader) {
    this.loader = loader;
  }
  public static createPlayer(scene: ECSA.Scene): void {
    //id = 0 for player
    var p: Player = new Player(0);

    this.pcPlayer = p;
    var obj = new ECSA.Builder(scene)
      .withComponent(p)
      .withComponent(new PlayerAnimation())
      //.anchor(0.5, 0.5)
      .scale(1)
      .withParent(scene.stage.asContainer())
      .asSprite(PIXI.Texture.from(`${Assets.RESOURCE_IDLE}D`), Names.PC_PLAYER)
      .build();

    p.setPositionToVAI(4, 0);
  }
}
