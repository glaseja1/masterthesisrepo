import * as ECSA from '../../libs/pixi-component';
import * as PIXI from 'pixi.js';

import { Assets, Names } from '../Constants';
import { HealthBar } from '../game/characters/HealthBar';

/**
 * Allows to create a monster
 */
export class HealthBarFactory {
  public static loader: PIXI.Loader;
  public static init(loader: PIXI.Loader) {
    this.loader = loader;
  }
  public static createFor(owner: ECSA.GameObject): void {
    var healthBarObject = new ECSA.Builder(owner.scene)
      .withComponent(new HealthBar())
      //.anchor(-2.0, -3)
      .scale(1)
      .withParent(owner)
      .asSprite(PIXI.Texture.from(`progressbar`))
      .build();
  }
}
