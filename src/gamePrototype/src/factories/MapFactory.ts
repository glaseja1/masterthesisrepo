import * as ECSA from '../../libs/pixi-component';
import * as PIXI from 'pixi.js';
import { Player } from '../game/characters/player/Player';
import { Assets } from '../Constants';
import { Point } from '../other/math/geometry/Point';
import { PositionModulator } from '../game/map/PositionModulator';
import { NavigationMesh } from '../game/map/navigation/NavigationMesh';
import { PathNode } from '../game/map/navigation/PathNode';
import { Functions } from '../other/Functions';
import { HashMap } from '../other/containers/hashMap/HashMap';
import { DroppedItemManager } from '../game/map/DroppedItemManager';

/**
 * Allows to create map graphics and hook events to them
 */
export class MapFactory {
  private static decorationOffsets: HashMap<string, Point> = new HashMap<string, Point>();

  public static loader: PIXI.Loader;
  public static init(loader: PIXI.Loader) {
    this.loader = loader;

    this.decorationOffsets.insert('d-tree2', new Point(118, 226));
    this.decorationOffsets.insert('d-tree3', new Point(100, 214));
    this.decorationOffsets.insert('d-tree1', new Point(90, 241));
    this.decorationOffsets.insert('d-rock1', new Point(24, 32));
    this.decorationOffsets.insert('d-rock2', new Point(26, 28));
    this.decorationOffsets.insert('d-pickup', new Point(25, 25));
  }
  public static createMap(scene: ECSA.Scene): void {
    //tiles
    for (let i: number = 1; i < 5 + 5; i++)
      this.createTile(scene, new Point(i, -1));
    for (let i: number = 2; i < 5 + 6; i++)
      this.createTile(scene, new Point(i, -2));
    for (let i: number = 3; i < 5 + 5; i++)
      this.createTile(scene, new Point(i, -3));
    for (let i: number = 4; i < 5 + 4; i++)
      this.createTile(scene, new Point(i, -4));
    for (let i: number = 5; i < 5 + 3; i++)
      this.createTile(scene, new Point(i, -5));
    this.createTile(scene, new Point(6, -6));

    for (let i: number = 0; i < 5 + 4; i++)
      this.createTile(scene, new Point(i, 0));
    for (let i: number = 1; i < 5 + 3; i++)
      this.createTile(scene, new Point(i, 1));
    for (let i: number = 2; i < 5 + 2; i++)
      this.createTile(scene, new Point(i, 2));
    for (let i: number = 3; i < 5 + 1; i++)
      this.createTile(scene, new Point(i, 3));
    this.createTile(scene, new Point(4, 4));

    //decoration
    this.createDecoration('d-rock1', scene, new Point(4, 3));
    this.createDecoration('d-rock2', scene, new Point(2, 0));
    this.createDecoration('d-tree1', scene, new Point(2, -1));
    this.createDecoration('d-tree2', scene, new Point(7, -3));
    this.createDecoration('d-tree3', scene, new Point(10, -3));

    //remove path nodes
    let id: string = Functions.createPathNodeKeyFromIndexPoint(new Point(1, -1));
    NavigationMesh.getInstance().deleteNode(id);
    id = Functions.createPathNodeKeyFromIndexPoint(new Point(6, -3));
    NavigationMesh.getInstance().deleteNode(id);
    id = Functions.createPathNodeKeyFromIndexPoint(new Point(9, -3));
    NavigationMesh.getInstance().deleteNode(id);

    //connect all the nodes together
    for (let i = NavigationMesh.getInstance().getAllPathNodes().iterator(); !i.isEnd(); i.next())
      Functions.connectNodeToAllAdjacent(i.key as string);

    //add pickupable items
    this.createPickup(scene, new Point(3, 3));
    this.createPickup(scene, new Point(5, 3));
    this.createPickup(scene, new Point(5, -3));
  }
  private static createDecoration(assetName: string, scene: ECSA.Scene, vai: Point): void {
    var realPos: Point = PositionModulator.getWorldPositionRecctangleUpperLeftCorner(vai.x, vai.y);
    realPos.y *= -1;
    //apply offset
    if (this.decorationOffsets.contains(assetName)) {
      realPos.x -= this.decorationOffsets.get(assetName).x;
      realPos.y -= this.decorationOffsets.get(assetName).y;
    }
    //console.log(realPos)
    var centrePos: Point = PositionModulator.getSquareCentrePosition(vai.x, vai.y);
    centrePos.y *= -1;
    //console.log(centrePos)

    var text = PIXI.Texture.from(assetName);
    var obj = new ECSA.Builder(scene)
      //.anchor(0.5, 0.5)
      .globalPos(realPos.x, realPos.y)
      .scale(1)
      .withParent(scene.stage.asContainer())
      .asSprite(text)
      .build();
  }
  private static createPickup(scene: ECSA.Scene, vai: Point): void {
    var realPos: Point = PositionModulator.getWorldPositionRecctangleUpperLeftCorner(vai.x, vai.y);
    realPos.y *= -1;
    //console.log(realPos)
    var centrePos: Point = PositionModulator.getSquareCentrePosition(vai.x, vai.y);
    centrePos.y *= -1;
    //console.log(centrePos)

    var text = PIXI.Texture.from('d-pickup');
    var obj = new ECSA.Builder(scene)
      //.anchor(0.5, 0.5)
      .globalPos(realPos.x + 25, realPos.y)
      .scale(1)
      .withParent(scene.stage.asContainer())
      .asSprite(text)
      .build();

    DroppedItemManager.getInstance().addItem(vai, obj);
  }
  /**
   *
   * @param scene [description]
   * @param vai   The virtual array index to create the tile at
   */
  private static createTile(scene: ECSA.Scene, vai: Point): void {
    //create path node
    let id: string = Functions.createPathNodeKeyFromIndexPoint(vai);
    let pn: PathNode = new PathNode(id);
    NavigationMesh.getInstance().addNode(pn);

    var realPos: Point = PositionModulator.getWorldPositionRecctangleUpperLeftCorner(vai.x, vai.y);
    realPos.y *= -1;
    //console.log(realPos)
    var centrePos: Point = PositionModulator.getSquareCentrePosition(vai.x, vai.y);
    centrePos.y *= -1;
    //console.log(centrePos)

    var text = PIXI.Texture.from(Assets.TEX_MAP_TILE);
    var obj = new ECSA.Builder(scene)
      //.anchor(0.5, 0.5)
      .globalPos(realPos.x, realPos.y)
      .scale(1)
      .withParent(scene.stage.asContainer())
      .asSprite(text)
      .build();
    obj.asSprite().width = 160;
    obj.asSprite().height = 79;
    /*PositionModulator {
      static readonly MAP_FLOORTILE_W: number = 160;
      static readonly MAP_FLOORTILE_H*/
  }
}
