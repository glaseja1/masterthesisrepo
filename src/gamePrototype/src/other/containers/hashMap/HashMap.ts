export class HashMapIterator<K, V> {
  private dataList: any;
  private keysList: Array<any>;
  private currentKeyIndex: number;

  constructor(list: any) {
    this.keysList = Object.keys(list);
    this.currentKeyIndex = 0;
    this.dataList = list;
  }
  public hasNext(): boolean {
    return this.currentKeyIndex < this.keysList.length;
  }
  /**
   * true if iterator reached the end (the value is undefined...)
   * @return [description]
   */
  public isEnd(): boolean {
    return (this.currentKeyIndex >= this.keysList.length);
  }
  /**
   * Returns true only and only if current iterator is pointing at first element
   * @return [description]
   */
  public isFirst(): boolean {
    if (this.keysList.length == 0)
      return false;
    return (this.currentKeyIndex == 0);
  }
  public get key(): K {
    return this.keysList[this.currentKeyIndex];
  }
  public next(): boolean {
    if (this.isEnd())
      return false;
    this.currentKeyIndex++
    return true;
  }
  public get value(): V {
    return this.dataList[this.key];
  }
}

export class HashMap<K, V>
{
  private _arr: any;
  private _length: number;

  constructor() { this.clear(); }
  public clear(): void {
    this._arr = {};
    this._length = 0;
  }
  public get content(): any { return this._arr; }
  public contains(key: K): boolean { return this._arr[`${key}`] !== undefined; }
  public static createFromItem<K, V>(key: K, value: V): HashMap<K, V> {
    let l: HashMap<K, V> = new HashMap<K, V>();
    l.insert(key, value);
    return l;
  }
  /**
   * [delete description]
   * @param  key [description]
   * @return     Whetehr the element was found and deleted
   */
  public delete(key: K): boolean {
    if (!this.contains(key))
      return false;

    delete this._arr[key];
    this._length--;
    return true;
  }
  public _getArray(): any { return this._arr; }
  public get(key: K): V {
    if (!this.contains(key))
      throw new Error(`get(): Non existent key '${key}' in hashMap`);
    return this._arr[key];
  }
  /**
  * Returns random element
  * @return [description]
  */
  public getAny(): V {
    return this._arr[this.getAnyKey()];
  }
  /**
  * Returns random element
  * @return [description]
  */
  public getAnyKey(): K {
    let keys = Object.keys(this._arr);
    let randomIndex = Math.floor(Math.random() * keys.length);

    if (this.isEmpty())
      throw new Error(`getAny(): No elements in hashMap`);
    return keys[randomIndex] as any;
  }
  public getFirst(): V {
    if (this.length() === 0)
      throw new Error('getFirst(): No elements in hashMap');
    return this._arr[Object.keys(this._arr)[0]];
  }
  public getLast(): V {
    if (this.length() === 0)
      throw new Error('getFirst(): No elements in hashMap');
    return this._arr[Object.keys(this._arr)[this.length() - 1]];
  }
  /**
   * Inserts new element. If the key already exists,
   * element is not inserted and false is returned
   * @param  key   [description]
   * @param  value [description]
   * @return       [description]
   */
  public insert(key: K, value: V): boolean {
    if (this.contains(key))
      return false;
    this._arr[key] = value;
    this._length++;
    return true;
  }
  public insertOverwrite(key: K, value: V): void {
    if (!this.contains(key))
      this._length++;
    this._arr[key] = value;
  }
  public isEmpty(): boolean { return this.length() === 0; }
  public iterator(): HashMapIterator<K, V> {
    return new HashMapIterator<K, V>(this._arr);
  }
  public length(): number { return this._length; }
  public setValue(key: K, val: V): void {
    if (!this.contains(key))
      throw new Error(`setValue(): Non existent key '${key}' in hashMap`);
    this._arr[key] = val;
  }
}
