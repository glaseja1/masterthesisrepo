export class Pair<A, B>
{
  public first: A;
  public second: B;
  public constructor(first: A, second: B) {
    this.first = first;
    this.second = second;
  }
  public deserialize(data: any,
    deserializeValueA: (data: any) => [boolean, A],
    deserializeValueB: (data: any) => [boolean, B]): boolean {
    if (data.length !== 2)
      return false;

    let res = deserializeValueA(data[0]);
    let res2 = deserializeValueB(data[1]);
    if (!res[0] || !res2[0])
      return false;

    this.first = res[1];
    this.second = res2[1];
    return true;
  }
  public serialize(serializeValueA: (val: A) => any,
    serializeValueB: (val2: B) => any): any {
    return [serializeValueA(this.first), serializeValueB(this.second)];
  }
}
