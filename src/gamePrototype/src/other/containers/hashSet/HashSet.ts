import { HashMap } from '../hashMap/HashMap'

export class HashSetIterator<V> {
  private dataList: any;
  private keysList: Array<any>;
  private currentKeyIndex: number;

  constructor(list: any) {
    this.keysList = Object.keys(list);
    this.currentKeyIndex = 0;
  }
  /**
   * Whether if next() would be called, if the value would be defined
   * @return [description]
   */
  public hasNext(): boolean {
    return this.currentKeyIndex < this.keysList.length - 1;
  }
  /**
   * true if iterator reached the end (the value is undefined...)
   * @return [description]
   */
  public isEnd(): boolean {
    return (this.currentKeyIndex >= this.keysList.length);
  }
  public next(): boolean {
    if (this.isEnd())
      return false;
    this.currentKeyIndex++
    return true;
  }
  public get key(): void {
    throw new Error('Logic Error: No "key" getter exists in hashSet. Use "value" getter');
  }
  public get value(): V {
    return this.keysList[this.currentKeyIndex];
  }
}

export class HashSet<K>{

  private hashMap: HashMap<K, boolean>;

  constructor() {
    this.hashMap = new HashMap<K, boolean>();
  }
  public clear(): void { this.hashMap.clear(); }
  public contains(key: K): boolean { return this.hashMap.contains(key); }
  public static createFromItem<K>(key: K): HashSet<K> {
    let l: HashSet<K> = new HashSet<K>();
    l.insert(key);
    return l;
  }
  /**
   *
   * @param  key [description]
   * @return boolean   whether lement was deleted or not
   */
  public delete(key: K): boolean {
    return this.hashMap.delete(key);
  }
  public empty(): boolean { return this.length() === 0; }
  public getAny(): any {
    let keys = Object.keys(this.hashMap._getArray());
    let randomIndex = Math.floor(Math.random() * keys.length);

    if (this.isEmpty())
      throw new Error(`getAny(): No elements in hashMap`);
    return keys[randomIndex];
  }
  public getFirst(): K {
    if (this.empty())
      throw new Error('Container is empty');
    var it = this.hashMap.iterator();
    return it.key;
  }
  public insert(a: K): boolean {
    return this.hashMap.insert(a, true);
  }
  public isEmpty(): boolean { return this.length() === 0; }
  public iterator(): HashSetIterator<K> {
    return new HashSetIterator<K>(this.hashMap._getArray());
  }
  public length(): number {
    return this.hashMap.length();
  }
}
