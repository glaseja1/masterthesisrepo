import { LinkedListQuickSorter } from './LinkedListQuickSorter';
import { LinkedListMergeSorter } from './LinkedListMergeSorter';

export class LinkedListIterator<V> {
  /**
   * reference to the linkedList
   */
  private parentList: LinkedList<V>;
  private _nodePtr: LinkedListNode<V>;

  constructor(_head: LinkedListNode<V>, parentList: LinkedList<V>) {
    this._nodePtr = _head;
    this.parentList = parentList;
  }
  public clone(): LinkedListIterator<V> {
    return new LinkedListIterator<V>(this._nodePtr, this.parentList);
  }
  public hasNext(): boolean { return this._nodePtr !== null; }
  /**
   * true if iterator reached the end (the value is undefined...)
   * @return [description]
   */
  public isEnd(): boolean {
    return (this._nodePtr === null);
  }
  public isFirst(): boolean { return !this._nodePtr.hasPrev(); }
  public get key(): void {
    throw new Error("Iterator of linked list does not have [key]. Use [value] instead...");
  }
  public next(): boolean {
    if (this.isEnd())
      return false;
    this._nodePtr = this._nodePtr.next;
    return true;
  }
  public prev(): boolean {
    if (this._nodePtr === null)
      return false;
    this._nodePtr = this._nodePtr.prev;
    return true;
  }
  /**
   * removes the element from the linked list
   * and advances iterator to next element
   * It is safe to call inside iteration loop.
   * It is thread unsafe
   */
  public remove(): boolean {
    if (this._nodePtr === null)
      return false;

    let nextNode = this._nodePtr.next;
    let res = this.parentList.remove(this._nodePtr);
    if (res)
      this._nodePtr = nextNode;
    return res;
  }
  public get value(): V {
    return this._nodePtr.value;
  }
}

export class LinkedListNode<V> {
  public value: V;
  public next: LinkedListNode<V>;
  public prev: LinkedListNode<V>;

  constructor(value: V) {
    this.value = value;
    this.next = null;
    this.prev = null;
  }
  /**
   * Clears all references this node has
   * @return [description]
   */
  public destruct(): void {
    this.value = null;
    this.next = null;
    this.prev = null;
  }
  public hasNext(): boolean { return this.next !== null; }
  public hasPrev(): boolean { return this.prev !== null; }
}
/**
 * Represents double side linked list
 */
export class LinkedList<V> {
  protected _head: LinkedListNode<V>;
  private _length: number;
  protected _tail: LinkedListNode<V>;

  constructor() {
    this._head = null;
    this._tail = null;
    this._length = 0;
  }
  /**
   * Appends target linked list to this linked list.
   * The list that was appended becomes empty
   * @param l [description]
   */
  public append(l: LinkedList<V>): void {
    if (l._length === 0)
      return;

    if (this._length === 0) {
      this._head = l._head;
      this._tail = l._tail;
      this._length = l._length;
      l.clear();
      return;
    }

    this._tail.next = l._head;
    l._head.prev = this._tail;
    this._tail = l._tail;
    this._length += l._length;
    l.clear();
  }
  public static createFromItem<U>(value: U): LinkedList<U> {
    let l: LinkedList<U> = new LinkedList<U>();
    l.pushBack(value);
    return l;
  }
  public clear(): void {
    if (this._length === 0)
      return;

    //clear the links to prevent memory leaks
    /*var currNode: LinkedListNode<V> = this._head;
    while (currNode.hasNext()) {
      currNode.prev = null;
      let tmp = currNode;
      currNode = currNode.next;
      tmp.destruct();
    }*/
    this._head = null;
    this._tail = null;
    this._length = 0;
  }
  /**
   * Creates the clone of current list. Does not clone the inner objects in the list, only the linked list nodes
   * This will fully wrk only if the content of the list is a primitive (such as string | number)
   * @return [description]
   */
  public clone(): LinkedList<V> {
    let ll = new LinkedList<V>();
    for (let it = this.iterator(); !it.isEnd(); it.next())
      ll.pushBack(it.value);
    return ll;
  }
  public deserialize(data: any, deserializeValue: (data: any) => [boolean, V]): boolean {
    this.clear();

    for (let i: number = 0; i < data.length; i++) {
      let result: [boolean, V] = deserializeValue(data[i]);
      if (!result[0]) {
        this.clear();
        return false;
      } this.pushBack(result[1]);
    }
    return true;
  }
  private insertFirstNode(value: V): void {
    this._head = new LinkedListNode<V>(value);
    this._tail = this._head;
    this._length++;
  }
  /**
   * Inserts the element into this linked list.
   * Expects, that this linked list is sorted before the call.
   * After the call, the element is inserted, and the content of the linked list stays sorted.
   * Norml iteration through thelist is used.
   * Note: Even if Binary search algorithm would be used, the comlexity would be
   * O(log n + n) = O(n)
   * @param val [description]
   */
  public insertIntoSorted(val: V, comparator: (a: V, b: V) => number): LinkedListIterator<V> {
    if (this.isEmpty()) {
      this.pushBack(val);
      return this.iteratorLast();
    }

    var it: LinkedListNode<V> = this._head;
    while (it !== null) {
      if (comparator(it.value, val) >= 0) {
        var node = new LinkedListNode<V>(val);

        if (!it.hasPrev()) {
          this.pushFront(val);
          return this.iterator();
        }
        var prevInChain: LinkedListNode<V> = it.prev;

        this.linkTwoNodes(prevInChain, node);
        this.linkTwoNodes(node, it);
        this._length++;
        return new LinkedListIterator<V>(node, this);
      }
      it = it.next;
    }
    //append to the end...
    this.pushBack(val);
    return this.iteratorLast();
  }
  /**
   * Returns iterator pointing to the first element in list
   */
  public iterator(): LinkedListIterator<V> { return new LinkedListIterator<V>(this._head, this); }
  /**
   * Returns iterator pointing to the last element in list
   */
  public iteratorLast(): LinkedListIterator<V> { return new LinkedListIterator<V>(this._tail, this); }
  public isEmpty(): boolean { return this._length === 0; }
  public length(): number { return this._length; }
  private linkTwoNodes(A: LinkedListNode<V>, B: LinkedListNode<V>): void {
    A.next = B;
    B.prev = A;
  }
  /**
   * Sorts the container content using amerge sort algorithm
   * Preserves the iterators (if yu have iterators before the call, they will be valid even after calling this function)
   * @param  comparator [description]
   * @return            [description]
   */
  public mergeSort(comparator: (a: V, b: V) => number): void {
    if (this._length <= 1)
      return;
    var qs = new LinkedListMergeSorter<V>();
    var newHeadTail = qs.mergesort(this._length, this._head, this._tail, comparator);
    this._head = newHeadTail.first;
    this._tail = newHeadTail.second;
  }
  /**
   * Returns the value of the last element, not modifying the container
   * @return [description]
   */
  public peekBack(): V {
    if (this._length === 0)
      throw new Error('List is empty');
    return this._tail.value;
  }
  public peekFront(): V {
    if (this._length === 0)
      throw new Error('List is empty');
    return this._head.value;
  }
  /**
   * Returns the value of the last element, and also removes the element from the container
   * @return [description]
   */
  public popBack(): V {
    if (this._length === 0)
      throw new Error('List is empty');

    if (this._length === 1) {
      let val = this._head.value;
      this._head = null;
      this._tail = null;
      this._length = 0;
      return val;
    }
    let val = this._tail.value;

    let prevLastNode = this._tail;
    this._tail = prevLastNode.prev;
    this._tail.next = null;

    prevLastNode.prev = null;

    this._length--;
    return val;
  }
  /**
   * Returns the value of the first element, and also removes the element from the container
   * @return [description]
   */
  public popFront(): V {
    if (this._length === 0)
      throw new Error('List is empty');

    if (this._length === 1) {
      let val = this._head.value;
      this._head = null;
      this._tail = null;
      this._length = 0;
      return val;
    }
    let val = this._head.value;

    let prevFirstNode = this._head;
    this._head = prevFirstNode.next;
    this._head.prev = null;

    prevFirstNode.next = null;
    this._length--;
    return val;
  }
  public pushBack(value: V): boolean {
    if (this._head === null) {
      this.insertFirstNode(value);
      return true;
    }

    var node = new LinkedListNode<V>(value);
    this.linkTwoNodes(this._tail, node);
    this._tail = node;

    this._length++;
    return true;
  }

  public pushFront(value: V): boolean {
    if (this._head === null) {
      this.insertFirstNode(value);
      return true;
    }
    var node = new LinkedListNode<V>(value);
    this.linkTwoNodes(node, this._head);
    this._head = node;

    this._length++;
    return true;
  }
  /**
   * sorts the linked list using the quick sort algorithm
   * By sorting the container, any existing iterators are INVALIDATED
   * @param comparator Function which returns -1 if A preceeds B, 0 if they equal, 1 if B preceeds A
   */
  public quickSort(comparator: (a: V, b: V) => number): void {
    if (this._length <= 1)
      return;
    var qs = new LinkedListQuickSorter<V>(this.swap);
    qs.quicksort(comparator, this._head, this._tail);
  }
  /**
   * removes the node from linked list.
   * If you pass here incorrect node, or node from different linked list,
   * or node that already was removed,
   * then the behavior is not defined.
   *
   * Called mainly from iterator
   *
   * O(1)
   * @param  n [description]
   * @return   Whether the node was removed
   */
  public remove(n: LinkedListNode<V>): boolean {
    if (this._length === 0)
      return false;
    if (this._length === 1) {
      this.popFront();
      return true;
    }

    //if the node is not last and is not first one
    if (n.prev !== null && n.next !== null) {
      n.prev.next = n.next;
      n.next.prev = n.prev;
      n.next = null;
      n.prev = null;
      //reduce count
      this._length--;
      return true;
    }
    //if the node is first one
    if (n.prev === null) {
      this.popFront();
      return true;
    }
    //if the node is last one
    if (n.next === null) {
      this.popBack();
      return true;
    }
    return false;
  }
  /**
   * O(n)
   * @param pos [description]
   */
  public removeNth(pos: number): void {
    if (pos < 0 || pos >= this._length)
      return;

    if (pos === 0) {
      this.popFront();
      return;
    }
    if (pos === this._length - 1) {
      this.popBack();
      return;
    }
    var curr: LinkedListNode<V> = this._head;
    var c = 0;
    while (c !== pos) {
      curr = curr.next;
      c++;
    }
    curr.prev.next = curr.next;
    curr.next.prev = curr.prev;
  }
  /**
   * [serialize description]
   * @param  serializeValue Callback to function, where the value is serialized
   * @return                [description]
   */
  public serialize(serializeValue: (val: V) => any): any {
    let arr = [];
    for (let it = this.iterator(); !it.isEnd(); it.next())
      arr.push(serializeValue(it.value));
    return arr;
  }
  /**
   * Allows to shuffle a linked list in O(n * log n) time using a merge sort algorithm
   * Shuffles the content of the list
   * Does preserve the iterators
   */
  public shuffle(): void {
    if (this._length <= 1)
      return;
    var qs = new LinkedListMergeSorter<V>();
    var flipCoin = function(a: V, b: V): number {
      return Math.random() > 0.5 ? 1 : -1;
    }
    var newHeadTail = qs.mergesort(this._length, this._head, this._tail, flipCoin);
    this._head = newHeadTail.first;
    this._tail = newHeadTail.second;
  }
  /**
   * Protected to be able to test it
   *
   * The two nodes will exchange thier places.
   * If the two nodes are same, then nothing happens.
   *
   * If each node belongs to different linked list, then behavior is UNDEFINED!
   * If you iterate linked list, and in the loop you swap elements, it will not propagate to iterators!
   * @param a [description]
   * @param b [description]
   */
  protected swap(a: LinkedListNode<V>, b: LinkedListNode<V>): void {
    if ((a.next === b.next && a.prev === b.prev) || this._length < 2)
      return;

    //swap values
    let tmp: V = a.value;
    a.value = b.value;
    b.value = tmp;
  }
  /**
   * Creates an array of elements reflecting the content of this container
   * Used mostly in tests...
   * O(n)
   * @return [description]
   */
  public toArray(): Array<V> {
    var arr: Array<V> = new Array<V>();
    for (let it = this.iterator(); !it.isEnd(); it.next())
      arr.push(it.value);
    return arr;
  }
  public toString(): string {
    let s = `length: ${this.length()},\ncontent:\n`;
    for (let i = this.iterator(); !i.isEnd(); i.next())
      s += `${i.value}, `
    s += `\n`
    return s;
  }
}
