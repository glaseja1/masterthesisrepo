import { LinkedList, LinkedListNode, LinkedListIterator } from './LinkedList';

export class LinkedListQuickSorter<V> {
  private swap: (a: LinkedListNode<V>, b: LinkedListNode<V>) => void;

  public constructor(swap: (a: LinkedListNode<V>, b: LinkedListNode<V>) => void) {
    this.swap = swap;
  }
  public quicksort(comparator: (a: V, b: V) => number,
    low: LinkedListNode<V>, high: LinkedListNode<V>): void {
    this.quickSortRecursive(comparator, low, high);
  }

  /**
   * selects pivot, and rearranges container such as elements smaller than
   * pivot are on the left, greater than are on the right
   */
  public quickSortPartition(comparator: (a: V, b: V) => number,
    low: LinkedListNode<V>, high: LinkedListNode<V>): LinkedListNode<V> {
    //choose last element as pivot (for now...)
    var pivot: LinkedListNode<V> = high;
    //console.log("selected pivot: " + pivot.value);

    //console.log(low)
    //console.log(high)
    high = high.prev;
    while (low !== high) {
      //console.log('Comparing ' + low.value + " ?> " + pivot.value)
      //if element is not larger than pivot
      if (comparator(low.value, pivot.value) === -1) {
        low = low.next;
        //console.log('low: ' + low.value)
        continue;
      }
      /*    if (low == null)
            console.log('low: null')
          else
            console.log('low: ' + low.value)
          if (high == null)
            console.log('high: null')
          else
            console.log('high: ' + high.value)
      */
      //console.log('Comparing ' + high.value + " ?< " + pivot.value)
      //if element is not smaller than pivot or equal
      let cmprd: number = comparator(high.value, pivot.value);
      if (cmprd !== -1) {
        high = high.prev;
        //  console.log('high: ' + high.value)
        continue;
      }

      //they can be swopped
      //console.log('swopping ' + low.value + " and " + high.value);
      this.swap(low, high);

      //console.log(this.toString());

      low = low.next;
      if (high == low)
        break;
      high = high.prev;
    }
    //high and low met

    //swop pivot and low
    if (comparator(low.value, pivot.value) === -1) {
      //console.log('swopping ' + low.next.value + " and " + pivot.value);
      this.swap(pivot, low.next);
      return low.next;
    }
    else {
      //console.log('swopping ' + low.value + " and " + pivot.value);
      this.swap(pivot, low);
    }
    return low;
  }
  private quickSortRecursive(comparator: (a: V, b: V) => number,
    low: LinkedListNode<V>, high: LinkedListNode<V>): void {
    if (low == null || high == null || low == high)
      return;

    //console.log(`sorting from ${low.value} to ${high.value}`)
    let newPivot = this.quickSortPartition(comparator, low, high);
    //console.log("after partition")
    //console.log(this.toString());
    //console.log()
    /*var pivotIndex = 0;
    var tmp = newPivot;
    while (tmp.prev != null) {
      tmp = tmp.prev;
      pivotIndex++;
    }*/
    //console.log('pivot ' + newPivot.value + ' at  index ' + pivotIndex);
    //if only 2 elements were sorted, then partition took care of that
    if (low.next == high || high.prev == low)
      return;

    //console.log('recursion')
    //sort left part
    if (low != newPivot)
      this.quickSortRecursive(comparator, low, newPivot.prev);
    //sort right part
    if (newPivot != high)
      this.quickSortRecursive(comparator, newPivot.next, high);
    //console.log("exit recursion")
  }
}
