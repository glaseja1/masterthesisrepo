import { LinkedList, LinkedListNode } from './LinkedList';
import { Pair } from '../pair/Pair';
/**
 * Allows to sort a linked list using ma merge sort algorithm
 */
export class LinkedListMergeSorter<V> {

  public constructor() { }
  /**
   * Executes out-of-place merge sort algoritnhm.
   * At the end, the input linked list is sorted
   * @param  l          [description]
   * @param  comparator [description]
   * @return            [description]
   */
  public mergesort(length: number, head: LinkedListNode<V>, tail: LinkedListNode<V>,
    comparator: (a: V, b: V) => number): Pair<LinkedListNode<V>, LinkedListNode<V>> {
    if (length <= 1)
      return new Pair<LinkedListNode<V>, LinkedListNode<V>>(head, tail);
    return this.mergesortRecursive(length, head, tail, comparator);
  }
  private mergesortRecursive(length: number, head: LinkedListNode<V>, tail: LinkedListNode<V>,
    comparator: (a: V, b: V) => number): Pair<LinkedListNode<V>, LinkedListNode<V>> {
    if (length <= 1)
      return new Pair<LinkedListNode<V>, LinkedListNode<V>>(head, tail);

    var leftHead: LinkedListNode<V> = null;
    var leftTail: LinkedListNode<V> = leftHead;
    var rightHead: LinkedListNode<V> = null;
    var rightTail: LinkedListNode<V> = rightHead;

    var counter: number = 0;
    var half: number = Math.floor(length / 2);
    for (let it = head; it !== null; it = it.next) {
      if (counter < half) {
        //  console.log("adding to left")
        if (leftTail == null) {
          leftHead = it;
          leftTail = leftHead;
          counter++;
          continue;
        }
        leftTail.next = it;
        it.prev = leftTail;
        leftTail = it;
      }
      else {
        //  console.log("adding to right")
        if (rightTail == null) {
          rightHead = it;
          rightTail = rightHead;
          counter++;
          continue;
        }
        rightTail.next = it;
        it.prev = rightTail;
        rightTail = it;
      }
      counter++;
    }
    if (leftTail !== null)
      leftTail.next = null;
    if (rightHead !== null)
      rightHead.prev = null;

    /*  console.log(leftHead)
      console.log(rightHead)
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()
      console.log()*/

    //recursivelly sort both sublists
    let res1 = this.mergesortRecursive(half, leftHead, leftTail, comparator);
    /*console.log('from result')
    console.log(res1.first)
    console.log(res1.second)*/
    leftHead = res1.first;
    leftTail = res1.second;

    res1 = this.mergesortRecursive(length - half, rightHead, rightTail, comparator);
    /*  console.log('from result 2')
      console.log(res1.first)
      console.log(res1.second)*/
    rightHead = res1.first;
    rightTail = res1.second;

    //merge the sorted lists
    return this.merge(leftHead, leftTail, rightHead, rightTail, comparator);
  }
  private merge(
    l: LinkedListNode<V>, lTail: LinkedListNode<V>,
    r: LinkedListNode<V>, rTail: LinkedListNode<V>,
    comparator: (a: V, b: V) => number): Pair<LinkedListNode<V>, LinkedListNode<V>> {
    /*  console.log('mergeNEW()')
      console.log(l)
      console.log(lTail)
      console.log(r)
      console.log(rTail)*/
    var resHead: LinkedListNode<V> = null;
    var resTail: LinkedListNode<V> = resHead;

    while (l !== null && r !== null) {
      if (comparator(l.value, r.value) <= 0) {
        //console.log("A")
        if (resTail == null) {
          resHead = l;
          resTail = resHead;
        }
        else {
          resTail.next = l;
          l.prev = resTail;
          resTail = l;
        }
        l = l.next;
      } else {
        //console.log("B")
        if (resTail == null) {
          resHead = r;
          resTail = resHead;
        }
        else {
          resTail.next = r;
          r.prev = resTail;
          resTail = r;
        }
        r = r.next;
      }
    }
    /*  console.log(resHead)
      console.log(resTail)*/
    if (l !== null) {
      if (resTail == null) {
        resHead = l;
        resTail = lTail;
      }
      else {
        resTail.next = l;
        l.prev = resTail;
        resTail = lTail;
      }
    }
    if (r !== null) {
      if (resTail == null) {
        resHead = r;
        resTail = rTail;
      }
      else {
        resTail.next = r;
        r.prev = resTail;
        resTail = rTail;
      }
    }
    return new Pair<LinkedListNode<V>, LinkedListNode<V>>(resHead, resTail);
  }
}
