/**
 * @param toWait Time in miliseconds
 */
export class Timer {
  private _toWait: number;
  private _functionCallback: () => void = null;
  private _running: boolean = false;
  private _startTicks: number = 0;
  private _currentTicks: number;

  /**
   * Creates the timer, and starts computing time.
   * You must call update() to update the time measurement
   * @param toWait time in miliseconds
   */
  constructor(toWait: number) {
    if (toWait < 1)
      toWait = 0;

    this._toWait = toWait;
    this.reset();
  }
  //======================================================================
  public set functionCallback(f: () => void) {
    this._functionCallback = f;
  }
  private _getTimeNow(): number { return new Date().getTime(); }
  /**
   * returns false if time is up
   * @return [description]
   */
  public isRunning(): boolean { return this._running; }
  public reset(): void {
    this._startTicks = this._getTimeNow();
    this._currentTicks = this._startTicks;
    this._running = true;
  }
  public setTime(toWait: number, resetTimer: boolean): void {
    if (resetTimer)
      this.reset();

    this._toWait = toWait;
  }
  public stop(): void { this._running = false; }
  /**
   * in miliseconds
   * @return [description]
   */
  public get timeElapsed(): number { return this._getTimeNow() - this._startTicks; }
  public get timeLeft(): number {
    if (this._toWait - (this._currentTicks - this._startTicks) < 0)
      return 0;
    return this._toWait - (this._currentTicks - this._startTicks);
  }
  public get timeToWait(): number { return this._toWait; }
  /**
   * True if time is up
   * @return [description]
   */
  public update(): boolean {
    /*if (!this.isRunning())
      return false;*/
    this._currentTicks = this._getTimeNow();
    if (this._currentTicks - this._startTicks >= this._toWait) {
      this._running = false;

      if (this._functionCallback !== null)
        this._functionCallback();
      return true;
    }
    return false;
  }
}
