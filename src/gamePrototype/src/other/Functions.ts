import { Point } from './math/geometry/Point';
import { DIRECTION } from '../Constants';
import { NavigationMesh } from '../game/map/navigation/NavigationMesh';
import { PathNodeTransition } from '../game/map/navigation/PathNodeTransition';
import { PathNode } from '../game/map/navigation/PathNode';
import { PositionModulator } from '../game/map/PositionModulator';

/**
 * MODIFIED (LIKE ALL FILES) TO WORK WITH PIXIJS
 *
 * various functions,
 * used by both server && client (therefore there are no functions that would be using client / server specific classes)
 */
export class Functions {
  public static MATH_PRECISION_CIPHERS = 8;
  /**
   * Given 2 VAI points, A, B, returns true, if
   * Exists any node N (in following diagram)
   *    O     O     O
   * O     N     O
   *    N     N     O
   * N     A     N
   *    N     N     O
   * O     N     O
   *    O     O     O
   *
   * such as N = B
   *
   * Note: because of speed optimalization, this is not using any other function!
   * @param  x1 x coordinate of node A
   * @param  y1 y coordinate of node A
   * @param  x2 x coordinate of node B
   * @param  y2 y coordinate of node B
   * @return    [description]
   */
  public static areVaiInAroundRelation(x1: number, y1: number, x2: number, y2: number): boolean {
    //for some odd reason, if you type return (expression), it returns undefined.
    //So put it in variable first...
    let a: boolean = this.areVaiInORelation(x1, y1, x2, y2);
    let b: boolean = ((x1 + 1 == x2) && (y1 + 1 == y2)) ||
      ((x1 - 1 == x2) && (y1 - 1 == y2));
    let c: boolean = a || b;
    return c;
  }
  /**
   * Given 2 VAI points, A, B, returns true, if
   * Exists any node N (in following diagram)
   *    O     O     O
   * O     N     O
   *    N     N     O
   * O     A     O
   *    N     N     O
   * O     N     O
   *    O     O     O
   *
   * such as N = B
   *
   * Note: because of speed optimalization, this is not using any other function!
   * @param  x1 x coordinate of node A
   * @param  y1 y coordinate of node A
   * @param  x2 x coordinate of node B
   * @param  y2 y coordinate of node B
   * @return    [description]
   */
  public static areVaiInORelation(x1: number, y1: number, x2: number, y2: number): boolean {
    //for some odd reason, if you type return (expression), it returns undefined.
    //So put it in variable first...
    var v: boolean = ((x1 + 1 == x2) && (y1 == y2)) ||
      ((x1 - 1 == x2) && (y1 == y2)) ||
      ((x1 == x2) && (y1 + 1 == y2)) ||
      ((x1 == x2) && (y1 - 1 == y2)) ||
      ((x1 - 1 == x2) && (y1 + 1 == y2)) ||
      ((x1 + 1 == x2) && (y1 - 1 == y2));
    return v;
  }
  /**
   * Given 2 VAI points, A, B, returns true, if
   * Exists any node N (in following diagram)
   *    O     O     O
   * O     O     O
   *    N     N     O
   * O     A     O
   *    N     N     O
   * O     O     O
   *    O     O     O
   *
   * such as N = B
   *
   * Note: because of speed optimalization, this is not using any other function!
   * @param  x1 x coordinate of node A
   * @param  y1 y coordinate of node A
   * @param  x2 x coordinate of node B
   * @param  y2 y coordinate of node B
   * @return    [description]
   */
  public static areVaiInXRelation(x1: number, y1: number, x2: number, y2: number): boolean {
    //for some odd reason, if you type return (expression), it returns undefined.
    //So put it in variable first...
    var v = ((x1 + 1 == x2 && y1 == y2) ||
      (x1 - 1 == x2 && y1 == y2) ||
      (x1 == x2 && y1 + 1 == y2) ||
      (x1 == x2 && y1 - 1 == y2));
    return v;
  }
  public static connectNodeToAllAdjacent(nodeID: string): void {
    //console.log('connectNodeToAllAdjacent')
    //console.log(nodeID)
    var navMesh = NavigationMesh.getInstance();
    var thisNode: PathNode = navMesh.getPathNode(nodeID);

    let virtArrayIndex: Point = this.getVaiFromPathNodeKey(nodeID);
    //console.log(virtArrayIndex)
    let arr = [];
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x + 1, virtArrayIndex.y));
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x - 1, virtArrayIndex.y));
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x, virtArrayIndex.y + 1));
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x, virtArrayIndex.y - 1));

    for (let i: number = 0; i < arr.length; i++) {
      //console.log(`*********** ${arr[i]}`)
      if (!navMesh.existsNode(arr[i]))
        continue;

      //console.log(` -- connecting ${thisNode.id} to ${arr[i]}`)
      let targetNode = navMesh.getPathNode(arr[i]);
      thisNode.addPathNodeLink(targetNode, new PathNodeTransition(1));
      targetNode.addPathNodeLink(thisNode, new PathNodeTransition(1));
    }

    arr = [];
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x + 1, virtArrayIndex.y - 1));
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x + 1, virtArrayIndex.y + 1));
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x - 1, virtArrayIndex.y - 1));
    arr.push(Functions.createPathNodeKeyFromIndex(virtArrayIndex.x - 1, virtArrayIndex.y + 1));

    for (let i: number = 0; i < arr.length; i++) {
      if (!navMesh.existsNode(arr[i]))
        continue;

      //console.log(`connecting ${thisNode.id} to ${arr[i]}`)
      let targetNode = navMesh.getPathNode(arr[i]);
      thisNode.addPathNodeLink(targetNode, new PathNodeTransition(2));
      targetNode.addPathNodeLink(thisNode, new PathNodeTransition(2));
    }
  }
  /**
   * Given position (ment for virtual array index), creates SHORT and unique node indetifier
   * ANY pathnode has key defined using this function!!!!!!
   * @param  x virtual array index, x
   * @param  y virtual array index, y
   * @return   [description]
   */
  public static createPathNodeKeyFromIndex(x: number, y: number): string { return `N-${x}-${y}`; }
  public static createPathNodeKeyFromIndexPoint(p: Point): string { return this.createPathNodeKeyFromIndex(p.x, p.y); }
  /**
   *
   * @param  x1 [description]
   * @param  y1 [description]
   * @param  x2 [description]
   * @param  y2 [description]
   * @return    True, if (x2, y2) is directly above (x1, y1)   (with distance of 1)
   */
  public static isVaiAbove(x1: number, y1: number, x2: number, y2: number): boolean {
    return (x2 + 1 == x1 && y2 - 1 == y1);
  }
  public static checkForListExistenKeys(variable: any, requiredKeys: Array<any>): boolean {
    if (!this.isVariableList(variable))
      return false;

    for (let i: number = 0; i < requiredKeys.length; i++) {
      if (variable[requiredKeys[i]] === undefined)
        return false;
    }
    return true;
  }
  public static getDirectionFromAngle(angle: number): DIRECTION {
    //as character is moving on the grid made from pathnodes, the angles are as follows (+- few degrees)
    // UP - 90
    // LEFT_UP - 153
    // LEFT - 180
    // LEFT_DOWN - 206.27
    // DOWN - 270
    // RIGHT_DOWN - 333.72
    // RIGHT - 0
    // RIGHT_UP - 26.27

    //define Epsylon as tolerance one side of interval
    var eps: number = 12;

    //console.log("characterClient: angle " + angle)
    if (angle >= 90 - eps && angle <= 90 + eps)
      return DIRECTION.U;
    else if (angle >= 153 - eps && angle <= 153 + eps)
      return DIRECTION.LEFT_UP;
    else if (angle >= 180 - eps && angle <= 180 + eps)
      return DIRECTION.L;
    else if (angle >= 206 - eps && angle <= 206 + eps)
      return DIRECTION.LEFT_DOWN;
    else if (angle >= 270 - eps && angle <= 270 + eps)
      return DIRECTION.D;
    else if (angle >= 333 - eps && angle <= 333 + eps)
      return DIRECTION.RIGHT_DOWN;
    else if (angle >= 0 - eps && angle <= 0 + eps)
      return DIRECTION.R;
    else //for better performance...    if (angle >= 26 - eps && angle <= 26 + eps)
      return DIRECTION.RIGHT_UP;
  }

  /**
   * Given VAI point A, returns set N of nodes such as:
   *
   *    O     O     O
   * O     N     O
   *    N     N     O
   * O     A     O
   *    N     N     O
   * O     N     O
   *    O     O     O
   *
   * O - nodes which are not returned
   * N - nodes which are returned
   * A - input node
   * @param  x [description]
   * @param  y [description]
   * @return [Point]  [description]
   */
  public static getVaiAdjacentO(x: number, y: number): Array<Point> {
    var arr = [];
    arr.push(new Point(x + 1, y));
    arr.push(new Point(x - 1, y));
    arr.push(new Point(x, y + 1));
    arr.push(new Point(x, y - 1));
    arr.push(new Point(x - 1, y + 1));
    arr.push(new Point(x + 1, y - 1));
    return arr;
  }
  /**
   * Given VAI point A, returns set N of nodes such as:
   * - all nodes in N are adjacent to A
   * - all nodes in N and node A form letter X
   * - N does not contain A
   *
   * example:
   * O     O     O
   *    N     N     O
   * O     A     O
   *    N     N     O
   * O     O     O
   *
   * O - nodes which are not returned
   * N - nodes which are returned
   * A - input node
   * @param  x [description]
   * @param  y [description]
   * @return [Point]  [description]
   */
  public static getVaiAdjacentX(x: number, y: number) {
    var arr = [];
    arr.push(new Point(x + 1, y));
    arr.push(new Point(x - 1, y));
    arr.push(new Point(x, y + 1));
    arr.push(new Point(x, y - 1));
    return arr;
  }
  /**
   * Given VAI point A, returns set N of nodes such as:
   *
   *    O     O     O
   * O     N     O
   *    N     N     O
   * N     A     N
   *    N     N     O
   * O     N     O
   *    O     O     O
   *
   * O - nodes which are not returned
   * N - nodes which are returned
   * A - input node
   * @param  x [description]
   * @param  y [description]
   * @return [Point]  [description]
   */
  public static getVaiAround(x: number, y: number): Array<Point> {
    var arr = [];
    arr.push(new Point(x + 1, y));
    arr.push(new Point(x - 1, y));
    arr.push(new Point(x, y + 1));
    arr.push(new Point(x, y - 1));
    arr.push(new Point(x - 1, y + 1));
    arr.push(new Point(x + 1, y - 1));
    arr.push(new Point(x + 1, y + 1));
    arr.push(new Point(x - 1, y - 1));
    return arr;
  }
  /**
   * Inverse to createPathNodeKeyFromIndex()
   * @param  nodeKey [description]
   * @return         NULL if parsing fails
   */
  public static getVaiFromPathNodeKey(nodeKey: string): Point {
    nodeKey = nodeKey.toString();
    var split = nodeKey.split("N-");
    let p = new Point();
    p.x = parseInt(split[1]);
    let rest = split[1].substring(`${p.x}`.length + 1);
    p.y = parseInt(rest);
    return p;
  }
  /**
   * Because number can be 0.00000000000000000000000000000000000000001 and therefore would not equal to zero,
   * This rounds the number to X digits to check if it is close to zero. If so, returns true
   * @param  n [description]
   * @return   [description]
   */
  public static equalsZero(n: number): boolean {
    return Math.abs(n).toFixed(this.MATH_PRECISION_CIPHERS) == (0).toFixed(this.MATH_PRECISION_CIPHERS);
  }
  /**
   * This will hash position.
   * It will work even in case when input is 0 or -0
   * @param  x [description]
   * @param  y [description]
   * @return   [description]
   */
  public static hashPoisition(x: number, y: number): string {
    return `${x}*${y}`;
  }
  /**
   * Allows to revert function hashPoisition()
   * @param  h [description]
   * @return   [description]
   */
  public static hashPoisitionInverse(h: string): Point {
    var split = h.split("*");
    return new Point(parseInt(split[0]), parseInt(split[1]));
  }
  public static isVariableArray(variable: any): boolean {
    return Array.isArray(variable) && variable !== null;
  }
  public static isVariableBoolean(variable: any): boolean {
    return typeof variable === 'boolean';
  }
  /**
   * Checks, if content of variable is whole number, without exponent
   * @param  variable [description]
   * @return          [description]
   */
  public static isVariableIntegerNumber(variable: any): boolean {
    if (!this.isVariableNumber(variable))
      return false;
    //console.log(variable)
    //console.log(variable % 1);
    if (variable % 1 == 0)
      return true;
    return false;
  }
  public static isVariableList(variable: any): boolean {
    return typeof variable === 'object' && variable !== null;
  }
  /**
   * Given string or number type, if the string is number
   * "1" = true
   * 1 = true
   * "abc90" = false
   * @param  variable [description]
   * @return          [description]
   */
  public static isVariableNumeric(variable: any): boolean {
    return !isNaN(parseFloat(variable)) && !isNaN(variable - 0);
  }
  /**
  * Returns true if the variable is type of number
   * @param  variable [description]
   * @return          [description]
   */
  public static isVariableNumber(variable: any): boolean {
    return (typeof variable === 'number');
  }
  public static isVariableNumberOrString(variable: any): boolean {
    return Functions.isVariableNumber(variable) || this.isVariableString(variable);
  }
  /**
   * Returns true if the variable is type of string
   * @param  variable [description]
   * @return          [description]
   */
  public static isVariableString(variable: any): boolean {
    return (typeof variable === 'string');
  }
  /**
   * Generates number N such as:
   * [+] N is whole number
   * [+] N is within <start, end>
   * @param  start [description]
   * @param  end   [description]
   * @return       [description]
   */
  public static generateRandomNumber(start: number, end: number): number {
    return Math.floor(Math.random() * (end - start + 1) + start);
  }
  public static isFloat(n: number) { return n % 1 !== 0; }
  /**
   * Converts miliseconds to readable format
   * @param  ms [description]
   * @return    [description]
   */
  public static msToReadable(ms: number): string {
    let d = new Date(ms);
    let text: string = "";
    if (d.getUTCHours() > 0)
      text += `${d.getUTCHours()}h `;
    if (d.getUTCMinutes() > 0)
      text += `${d.getUTCMinutes()}m `;
    if (d.getUTCSeconds() > 0)
      text += `${d.getUTCSeconds()}s `;
    text += `${d.getUTCMilliseconds()}ms`;
    return text;
  }
  public static roundNumber(n: number): number {
    return Number(n.toFixed(this.MATH_PRECISION_CIPHERS));
  }
  /**
   * Capitalizes the first letter of an input string
   * @param  s [description]
   * @return   The input string with the first letter capitalized
   */
  public static ucfirst(s: string): string {
    return s.charAt(0).toUpperCase() + s.slice(1);
  }
}
