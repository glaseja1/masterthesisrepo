import { Point } from './Point';
import { Circle } from './Circle';

export class Triangle {
  private _A: Point;
  private _B: Point;
  private _C: Point;

  public constructor(A: Point = null, B: Point = null, C: Point = null) {
    if (A === null)
      A = new Point();
    if (B === null)
      B = new Point(100, 100);
    if (C === null)
      C = new Point(50, 50);

    this._A = A;
    this._B = B;
    this._C = C;

    if (A.equals(B) || B.equals(C) || C.equals(A))
      throw new Error('Triangle: Invalid points');
  }
  public get A(): Point { return this._A; }
  public get B(): Point { return this._B; }
  public get C(): Point { return this._C; }

  /*public get centre(): Point {
    return this._centre;
  }*/

  /**
   * Returns true if one triangle collides the other
   * If two triangle only touch each other, false is returned
   *
   * If triangles are same, they are considered to be collising
   *
   *  test if one of the triangles has a side with all of the other triangles points on the outside.  Assume that we know all the normals of ech triangle, which tells us the direction that the points are specified.
   var trianglesIntersect3 = function(t0, t1) {
   * @param  c [description]
   * @return   [description]
   */
  public collides(t: Triangle): boolean {
    if (this.equals(t))
      return true;

    return true;
    /*  return !(this.cross2(this, t) ||
        this.cross2(t, this));*/
    /*let normal0: number = (t.b.x - t.a.x) * (t.c.y - t.a.y) -
      (t.b.y - t.a.y) * (t.c.x - t.a.x);

    let normal1: number = (this.b.x - this.a.x) * (this.c.y - this.a.y) -
      (this.b.y - this.a.y) * (this.c.x - this.a.x);

    return !(this.cross(this, t.a, t.b, normal0) ||
      this.cross(this, t.b, t.c, normal0) ||
      this.cross(this, t.c, t.a, normal0) ||
      this.cross(t, this.a, this.b, normal1) ||
      this.cross(t, this.b, this.c, normal1) ||
      this.cross(t, this.c, this.a, normal1));*/
  }
  /**
   * If they touch, false is returned
   * @param  C [description]
   * @return   [description]
   */
  public collidesCircle(C: Circle): boolean {
    return C.collidesTriangle(this);
  }
  /**
   * Returns if the point lies inside triangle.
   * If the point is ON the triangle, or equals any edge of triangle, then returns false
   * @param  p [description]
   * @return   [description]
   */
  public isPointInTriangle(p: Point): boolean {
    //p = A + w1(B-A) + w2(C-A)
    //px = Ax + w1(Bx-Ax) + w2(Cx-Ax)
    //py = Ay + w1(By-Ay) + w2(Cy-Ay)
    //w2 = (py - Ay - w1(By - Ay)) / (Cy - Ay)
    //.
    //..
    //..
    //..
    //w1 = (Ax(Cy-Ay) + (Py - Ay) * (Cx -Ax) - Px(Cy - Ay))    /    (By - Ay)(Cx - Ax) - (Bx - Ax)(Cy - Ay)
    let cyMinusAy = this._C.y - this._A.y;
    let cxMinusAx = this._C.x - this._A.x;
    let w1 = (this._A.x * cyMinusAy + (p.y - this._A.y) * cxMinusAx - p.x * cyMinusAy) /
      (((this._B.y - this._A.y) * cxMinusAx) - (this._B.x - this._A.x) * (this._C.y - this._A.y));
    let w2 = (p.y - this._A.y - w1 * (this._B.y - this._A.y)) / (this._C.y - this._A.y);

    //point is in ABC when
    return w1 > 0 && w2 > 0 && (w1 + w2) < 1;
  }
  /*private cross(t: Triangle, b: Point, c: Point, normal: number): boolean {
    let bx = b.x;
    let by = b.y;
    let cyby = c.y - by;
    let cxbx = c.x - bx;
    let pa = t.a;
    let pb = t.b;
    let pc = t.c;
    return !(
      (((pa.x - bx) * cyby - (pa.y - by) * cxbx) * normal < 0) ||
      (((pb.x - bx) * cyby - (pb.y - by) * cxbx) * normal < 0) ||
      (((pc.x - bx) * cyby - (pc.y - by) * cxbx) * normal < 0));
  }*/
  public equals(o: Triangle): boolean {
    return this._A.equals(o._A) && this._B.equals(o._B) && this._C.equals(o._C);
  }
}
