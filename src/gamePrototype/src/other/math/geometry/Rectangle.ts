import { Point } from './Point';
import { Vector } from './Vector';
import { TOUCH_TYPE } from '../../../Constants';
import { Shape, SHAPE_TYPE } from "./Shape";

/**
 * Represents a rectangle.
 * You can query it for collisions or whether rectangles are touching
 */
export class Rectangle extends Shape {
  private _upper_left_corner_x: number = 0;
  private _upper_left_corner_y: number = 0;
  private _width: number = 1;
  private _height: number = 1;
  //private TOUCH_TYPE = TOUCH_TYPE;

  constructor(x: number = 0, y: number = 0, w: number = 100, h: number = 100) {
    super(SHAPE_TYPE.RECTANGLE);
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  public static fromCentre(c_x: number, c_y: number, w: number, h: number): Rectangle {
    return new Rectangle(c_x - w / 2, c_y + h / 2, w, h);
  }
  public clone(): Shape {
    return new Rectangle(this._upper_left_corner_x,
      this._upper_left_corner_y,
      this.w,
      this.h) as Shape;
  }
  /**
   * Returns whether two rectangles collide or not.
   * If two rectangles only touch each other, false is returned
   * @param  {[type]} r [description]
   * @return {[type]}   [description]
   */
  public collides(r: Rectangle): boolean {
    var XOverlap: boolean = this._valueInRangeIncluded(r.x, this.x, this.getRight()) || this._valueInRangeIncluded(this.x, r.x, r.getRight());
    var YOverlap: boolean = this._valueInRangeIncluded(r.y, this.y, this.getDown()) || this._valueInRangeIncluded(this.y, r.y, r.getDown());

    if (XOverlap && YOverlap && !this.touches(r))
      return true;
    return false;
  }
  public countTouchType(r: Rectangle): TOUCH_TYPE {
    var collides = this.collides(r);
    if (collides)
      return TOUCH_TYPE.NONE;

    if (this.getPointUpperLeft().equals(r.getPointDownRight()))
      return TOUCH_TYPE.CORNER_UP_LEFT;

    if (this.getPointDownRight().equals(r.getPointUpperLeft()))
      return TOUCH_TYPE.CORNER_DOWN_RIGHT;

    if (this.getPointUpperRight().equals(r.getPointDownLeft()))
      return TOUCH_TYPE.CORNER_UP_RIGHT;

    if (this.getPointDownLeft().equals(r.getPointUpperRight()))
      return TOUCH_TYPE.CORNER_DOWN_LEFT;

    if (this._valueInRangeIncluded(this.y, r.y + this.h, r.y - r.h)) {
      //left side
      if (this.x == r.getRight())
        return TOUCH_TYPE.LEFT_SIDE;
      //right side
      if (this.getRight() == r.x)
        return TOUCH_TYPE.RIGHT_SIDE;
    }

    if (this._valueInRangeIncluded(this.x, r.x - this.w, r.getRight())) {
      //upper side
      if (this.getDown() === r.y)
        return TOUCH_TYPE.DOWN_SIDE;
      //down side
      if (this.y === r.getDown())
        return TOUCH_TYPE.UP_SIDE;
    }

    if (!collides)
      return TOUCH_TYPE.NONE;

    throw new Error('It should never get here');
  }
  public deserialize(data: any): boolean {
    if (data.length !== 4)
      return false;
    this._upper_left_corner_x = data[0];
    this._upper_left_corner_y = data[1];
    this._width = data[2];
    this._height = data[3];
    return true;
  }
  public equals(r): boolean {
    return this._upper_left_corner_x === r._upper_left_corner_x &&
      this._upper_left_corner_y === r._upper_left_corner_y &&
      this.w === r.w &&
      this.h === r.h;
  }
  public getDown(): number { return this.y - this.h; }
  public getPointDownRight(): Point {
    return new Point(this.getRight(), this.getDown());
  }
  public getPointDownLeft(): Point {
    return new Point(this.x, this.getDown());
  }
  public getPointUpperLeft(): Point {
    return new Point(this._upper_left_corner_x, this._upper_left_corner_y);
  }
  public getPointUpperRight(): Point { return new Point(this.getRight(), this.y); }
  public getRight(): number { return this.x + this.w; }

  /**
   * Returns false if point is ON rectangle or outside
   * @return [description]
   */
  public isPointInsideRectangle(p: Point): boolean {
    let xInRange: boolean = this._valueInRangeExcluded(p.x, this.x, this.getRight());
    let yInRange: boolean = this._valueInRangeExcluded(p.y, this.y, this.getDown());
    return xInRange && yInRange;
  }
  /**
   * Returns true if point is ON rectangle
   * @return [description]
   */
  public isPointOnRectangle(p: Point): boolean {
    let xInRange: boolean = this._valueInRangeIncluded(p.x, this.x, this.getRight());
    let yInRange: boolean = this._valueInRangeIncluded(p.y, this.y, this.getDown());

    let optiomn1 = xInRange && (p.y === this.y || p.y === this.getDown());
    let optiomn2 = yInRange && (p.x === this.x || p.x === this.getRight());
    return optiomn1 || optiomn2;
  }
  public isPointOutsideRectangle(p: Point): boolean {
    return !this.isPointInsideRectangle(p) && !this.isPointOnRectangle(p);
  }
  get x(): number { return this._upper_left_corner_x; }
  get y(): number { return this._upper_left_corner_y; }
  get h(): number { return this._height; }
  get w(): number { return this._width; }
  set x(x: number) { this._upper_left_corner_x = x; }
  set y(y: number) { this._upper_left_corner_y = y; }
  set h(h: number) {
    if (h < 1)
      throw 'bad height';
    this._height = h;
  }
  set w(w: number) {
    if (w < 1)
      throw 'bad width';
    this._width = w;
  }
  public moveBy(vector: Vector): void {
    this.x = (this.x + vector.x);
    this.y = (this.y + vector.y);
  }
  public serialize(): any {
    return [this._upper_left_corner_x,
    this._upper_left_corner_y,
    this._width,
    this._height];
  }
  public toString(): string {
    return `[x: ${this.x}, y: ${this.y}, w: ${this.w}, h: ${this.h}]`;
  }
  public touches(r: Rectangle): boolean {
    //left side                 right side
    if (this.x === r.getRight() || this.getRight() === r.x) {
      if (this._valueInRangeIncluded(this.y, r.y + this.h, r.y - r.h))
        return true;
    }
    //upper side                  down side
    if (this.getDown() === r.y || this.y === r.getDown()) {
      if (this._valueInRangeIncluded(this.x, r.x - this.w, r.getRight()))
        return true;
    }
    return false;
  }
  private _valueInRangeExcluded(value: number, minimum: number, maximum: number): boolean {
    return (value > Math.min(minimum, maximum)) && (value < Math.max(minimum, maximum));
  }
  private _valueInRangeIncluded(value: number, minimum: number, maximum: number): boolean {
    return (value >= Math.min(minimum, maximum)) && (value <= Math.max(minimum, maximum));
  }
}
//constants
/*TOUCH_TYPE_NONE = 0;
TOUCH_TYPE.CORNER_DOWN_LEFT = 1;
TOUCH_TYPE.CORNER_DOWN_RIGHT = 2;
TOUCH_TYPE.CORNER_UP_LEFT = 3;
TOUCH_TYPE.CORNER_UP_RIGHT = 4;
TOUCH_TYPE_DOWN_SIDE = 5;
TOUCH_TYPE.LEFT_SIDE = 6;
TOUCH_TYPE.RIGHT_SIDE = 7;
TOUCH_TYPE_UP_SIDE = 8;*/
