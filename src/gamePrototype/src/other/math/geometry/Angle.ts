import { Point } from './Point';
import { ANGLE_TYPE } from '../../../Constants';

/**
 * Represents physics. Can be added to any object to make it mobile
 * Allows to add force to the object. The final force is product of the force vectors added
 */
export class Angle {
  //static ANGLE_TYPE = ANGLE_TYPE;
  static M_PI = 3.1415926579793238462643383279502884;
  private valueInRads: number = 0;

  constructor() { }

  public static createFromPoints(point1: Point, point2: Point): Angle {
    var angle = new Angle();
    var deltaY = point2.y - point1.y;
    var deltaX = point2.x - point1.x;

    angle.valueInRads = Math.atan2(deltaY, deltaX);

    if (angle.valueInRads < 0)
      angle.valueInRads = 2 * Angle.M_PI + angle.valueInRads;

    //angle.valueInRads *= -1;
    return angle;
  }
  /**
   * Creates an angle from having 2 vectors
   * @param  {[type]} v1              [description]
   * @param  {[type]} v2              [description]
   * @param  {[type]} angle_type_enum [description]
   * @return {[type]}                 [description]
   */
  public static createFromVectors(v1: any, v2: any, angle_type: ANGLE_TYPE): Angle {
    if (v1.length() == 0 || v2.length() == 0)
      throw new Error("Can not calculate angle from 2 vectors");

    var a = new Angle();

    /*console.log(v1.toString())
    console.log(v2.toString())*/
    var tmp1 = v1.multiplyByVector(v2);
    /*console.log(tmp1)
    console.log(v1.length())
    console.log(v2.length())*/
    let toAcos = tmp1 / (v1.length() * v2.length());
    if (toAcos > 1)
      toAcos = 1;
    /*  console.log(`acos(${toAcos}) = ${Math.acos(toAcos)}`);
      console.log(`acos of ${Math.round(Math.acos(toAcos))}`);*/
    a.valueInRads = /*Math.round(*/Math.acos(toAcos)/*)*/;

    var degrees = a.getValueDegrees();
    //console.log(degrees)
    /*  if (degrees === 360 || degrees === 0)
        return a;*/

    var deg2 = 360.0 - degrees;
    if (angle_type === ANGLE_TYPE.SMALLER)
      degrees = Math.min(deg2, degrees);
    else if (angle_type === ANGLE_TYPE.GREATER)
      degrees = Math.max(deg2, degrees);

    /*  console.log('degree')
      console.log(degrees)*/
    a.changeAngleDegrees(degrees);
    return a;
  }
  public changeAngleDegrees(newDegrees: number): void {
    if (newDegrees > 360.0 || newDegrees < 0.0)
      throw new Error();

    this.valueInRads = this._degreesToRadians(newDegrees);
  }
  private _degreesToRadians(degrees: number): number {
    return degrees * Angle.M_PI / 180.0;
  }
  public equals(other: Angle): boolean {
    return this.valueInRads === other.valueInRads;
  }
  public getValueRadians(): number {
    return this.valueInRads;
  }
  public getValueDegrees(): number {
    return (this.valueInRads * 180.0 / Angle.M_PI) % 360.0;
  }
  public isZero(): boolean {
    return this.valueInRads.toFixed(2) === (0).toFixed(2);
  }
}
