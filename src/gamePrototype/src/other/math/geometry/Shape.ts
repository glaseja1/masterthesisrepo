import { Vector } from './Vector';

export enum SHAPE_TYPE {
  RECTANGLE,
  CIRCLE,
  LINE,
  LINESEGMENT
}
export abstract class Shape {
  private shapeType: SHAPE_TYPE;

  public constructor(t: SHAPE_TYPE) { this.shapeType = t; }
  public abstract clone(): Shape;

  public isCircle(): boolean {
    return this.shapeType === SHAPE_TYPE.CIRCLE;
  }
  public isLineSegment(): boolean {
    return this.shapeType === SHAPE_TYPE.LINESEGMENT;
  }
  public isRectangle(): boolean {
    return this.shapeType === SHAPE_TYPE.RECTANGLE;
  }
  /**
   * Moves the shape in that direction...
   * @param v [description]
   */
  public abstract moveBy(v: Vector): void;
  protected setShapeType(t: SHAPE_TYPE): void {
    this.shapeType = t;
  }

}
