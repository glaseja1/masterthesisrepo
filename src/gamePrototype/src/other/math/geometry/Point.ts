/**
 * Represents a point primitive
 */
export class Point {
  public x: number;
  public y: number;

  constructor(x: number = 0, y: number = 0) {
    this.x = x;
    this.y = y;
  }
  public clone(): Point { return new Point(this.x, this.y); }
  public distance(p: Point): number {
    return Math.sqrt((this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y));
  }
  public equals(point: Point): boolean { return (this.x == point.x && this.y == point.y); }
  /**
   * Considering that this point and target point are in VAI:
   * Given another Point,
   * Computes air distance between this and target point, in VAI
   * @param  n [description]
   * @param  round [description]
   * @return   [description]
   */
  public getAirVaiDistanceFrom(n: Point, round: boolean): number {
    let xDiff: number = Math.abs(this.x - n.x);
    let yDiff: number = Math.abs(this.y - n.y);
    if (round)
      return Math.floor(Math.sqrt(xDiff * xDiff + yDiff * yDiff));
    return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
  }
  public isZero(): boolean { return this.x == 0 && this.y == 0; }
  /**
   * For visual / debug / console logging only
   * @return [description]
   */
  public toString(): string { return `[${this.x};${this.y}]`; }
}
