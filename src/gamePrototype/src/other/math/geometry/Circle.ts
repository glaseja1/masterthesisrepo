import { Point } from './Point';
import { Triangle } from './Triangle';
import { Vector } from './Vector';
import { Line } from './Line';
import { Shape, SHAPE_TYPE } from "./Shape";
import { LineSegment } from "./LineSegment";
import { Rectangle } from "./Rectangle";

export class Circle extends Shape {
  private _centre: Point;
  public radius: number;

  public constructor(centreX: number = 0, centreY: number = 0, radius: number = 50) {
    super(SHAPE_TYPE.CIRCLE);

    this._centre = new Point(centreX, centreY);
    this.radius = radius;

    if (radius <= 0)
      throw new Error('Circle: radius <= 0');
  }
  public get centre(): Point {
    return this._centre;
  }
  public get center(): Point {
    return this._centre;
  }
  public clone(): Shape {
    return new Circle(this._centre.x,
      this._centre.y,
      this.radius) as Shape;
  }
  /**
   * Returns true if one circle collides the other
   * If two circles only touch each other, false is returned
   * @param  c [description]
   * @return   [description]
   */
  public collides(c: Circle): boolean {
    return (c.centre.distance(this.centre) < this.radius + c.radius);
  }
  /**
   * If the circle touches the line, false is returned.
   * @return [description]
   */
  public collidesLine(l: Line): boolean {
    return l.collidesCircle(this);
  }
  public collidesLineSegment(l: LineSegment): boolean {
    return l.collidesCircle(this);
  }
  public collidesRectangle(r: Rectangle): boolean {
    let l1 = new LineSegment(r.getPointUpperLeft(), r.getPointUpperRight());
    let l2 = new LineSegment(r.getPointDownLeft(), r.getPointDownRight());
    let l3 = new LineSegment(r.getPointUpperLeft(), r.getPointDownLeft());
    let l4 = new LineSegment(r.getPointUpperRight(), r.getPointDownRight());
    if (this.collidesLineSegment(l1) ||
      this.collidesLineSegment(l2) ||
      this.collidesLineSegment(l3) ||
      this.collidesLineSegment(l4))
      return true;

    //or whatever point must be inside circle
    let a = this.isPointInCircle(r.getPointUpperLeft());
    //or whatever point of circle must be inside rectangle
    let b = this.isPointInCircle(r.getPointUpperLeft());
    return a || b;
  }
  /**
   * not used. Outdated
   * @param  t [description]
   * @return   [description]
   */
  public collidesTriangle(t: Triangle): boolean {
    //if any corner point is inside this circle
    if (this.isPointInCircle(t.A) ||
      this.isPointInCircle(t.B) ||
      this.isPointInCircle(t.C))
      return true;

    //if circle centre is inside the triangle
    if (this.isCentreWithinTriangle(t))
      return true;

    //if circle intersects edge of triangle
    let L1 = new Line(t.A, t.B);
    let L2 = new Line(t.B, t.C);
    let L3 = new Line(t.C, t.A);
    /*console.log()
    console.log()
    console.log(`collision with line ${L1.toString()} : result ${this.collidesLine(L1)}`)
    console.log()
    console.log()
    console.log(`collision with line ${L2.toString()} : result ${this.collidesLine(L2)}`)
    console.log()
    console.log()
    console.log(`collision with line ${L3.toString()} : result ${this.collidesLine(L3)}`)*/
    /*if (this.collidesLine(L1) ||
      this.collidesLine(L2) ||
      this.collidesLine(L3))
      return true;*/
    return false;
  }
  /**
   * if it touches the line, false is returned
   * @param  l [description]
   * @return   [description]
   */
  /*public collidesLine(l: LineSegment): boolean {
    //let C be this circle with centre Oc.
    //Let L be target line LineSegment
    //let O be point on L such as line |O Oc| is perpendicular to L

    //calculate point O
    let vectorOc_O: Vector = new Vector();

}*/
  public equals(c: Circle): boolean {
    return this.radius === c.radius && this._centre.equals(c._centre);
  }
  public getIntersectionPointWithLine(L: Line): Array<Point> {
    return L.getIntersectionPointWithCircle(this);
  }
  /**
   * Given point A which is on circle C, returns line L which is tangent to C going through A
   * @param  a [description]
   * @return   [description]
   */
  public getTangentInPoint(a: Point): Line {
    //use naive approach for now (no derivatives....)
    let tmp: Line = new Line(this._centre, a);
    let tangent: Line = tmp.createPerpendicular(a);
    return tangent;
  }
  public isCentreWithinTriangle(t: Triangle): boolean {
    //check if the centre of circle is inside triangle
    return t.isPointInTriangle(this._centre);
  }
  /**
   * If point would lie on the circle, false is returned
   * @param  p [description]
   * @return   [description]
   */
  public isPointInCircle(p: Point): boolean {
    return (this.centre.distance(p) < this.radius);
  }
  public isPointOnCircle(p: Point): boolean {
    //console.log(`Point ${p} ${this.centre} has distance ${this.centre.distance(p)}, result ${(this.centre.distance(p) < this.radius)}`)
    return (this.centre.distance(p) === this.radius);
  }
  public isPointOutsideCircle(p: Point): boolean {
    return (this.centre.distance(p) > this.radius);
  }
  public moveBy(vector: Vector): void {
    this.centre.x += vector.x;
    this.centre.y += vector.y;
  }
  /*  public touchesLine(l: LineSegment): boolean {

  }*/
  public get x(): number {
    return this.centre.x;
  }
  /**
   * Sets the centre of circle
   * @param  v [description]
   * @return   [description]
   */
  public set x(v: number) {
    this.centre.x = v;;
  }
  public get y(): number {
    return this.centre.y;
  }
  /**
   * Sets the centre of circle
   * @param  v [description]
   * @return   [description]
   */
  public set y(v: number) {
    this.centre.y = v;
  }
  public toString(): string {
    return `${this.centre.toString()}, radius: ${this.radius}`;
  }
  public touches(c: Circle): boolean {
    return (c.centre.distance(this.centre) === this.radius + c.radius);
  }
  /**
   * If circle and triangle collide, returns false
   * @param  t [description]
   * @return   [description]
   */
  /*public touchesTriangle(t: Triangle): boolean {
    console.log('Warning: Circle:touchesTriangle() i snot optimized');
    if (!this.collidesTriangle(t))
      return false;

    //if any corner point is on this circle
    if (this.isPointOnCircle(t.A) ||
      this.isPointOnCircle(t.B) ||
      this.isPointOnCircle(t.C))
      return true;

    //if circle centre is inside the triangle
    if (this.isCentreWithinTriangle(t))
      return true;

    //if circle intersects edge of triangle
    let L1 = new Line(t.A, t.B);
    let L2 = new Line(t.B, t.C);
    let L3 = new Line(t.C, t.A);

    if (this.collidesLine(L1) ||
      this.collidesLine(L2) ||
      this.collidesLine(L3))
      return true;
    return false;
  }*/
}
