import { Line } from './Line';
import { Shape, SHAPE_TYPE } from './Shape';
import { Point } from './Point';
import { Circle } from './Circle';
import { Vector } from './Vector';
import { Functions } from '../../Functions';

export class LineSegment extends Line {
  private intervalXMin: number;
  private intervalXMax: number;
  private intervalYMin: number;
  private intervalYMax: number;

  public constructor(from: Point = null, to: Point = null) {
    super(from, to);
    super.setShapeType(SHAPE_TYPE.LINESEGMENT);

    this.calculateInvertalsForPoints();
  }
  private calculateInvertalsForPoints(): void {
    this.intervalXMin = Math.min(this.from.x, this.to.x);
    this.intervalXMax = Math.max(this.from.x, this.to.x);
    this.intervalYMin = Math.min(this.from.y, this.to.y);
    this.intervalYMax = Math.max(this.from.y, this.to.y);
  }
  public clone(): Shape {
    return new LineSegment(this.from, this.to) as Shape;
  }
  public collidesCircle(c: Circle): boolean {
    let intersects = this.getIntersectionPointWithCircle(c);
    //get only these intersection points that are on the line segment
    let intersectionsOnLineSeg = new Array<Point>();
    for (let i: number = 0; i < intersects.length; i++) {
      if (this.isPointOnLine(intersects[i].x, intersects[i].y))
        intersectionsOnLineSeg.push(intersects[i]);
    }

    if (intersectionsOnLineSeg.length == 0)
      return false;

    if (intersectionsOnLineSeg.length == 2 &&
      this.isPointOnLine(intersectionsOnLineSeg[0].x, intersectionsOnLineSeg[0].y) &&
      this.isPointOnLine(intersectionsOnLineSeg[1].x, intersectionsOnLineSeg[1].y)) {
      return true;
    }

    //only one intersection point
    let A = this.getPointA();
    let B = this.getPointB();

    if (c.isPointInCircle(A) && c.isPointInCircle(B))
      return true;
    return false;
    //create perpendicular line that runs through centre of this circle
    /*let perpendicular: Line = this.createPerpendicular(c.centre);
    /*  console.log('collidesCircle: this:')
      console.log(this)
      console.log('collidesCircle: prependicular:')
      console.log(perpendicular)*/
    //get intersection of the lines
    /*let intersection = perpendicular.getIntersectionPoint(this);

    //in case the intersection point is NOT on the line segment
    if (!this.isPointOnLine(intersection.x, intersection.y)) {
      //the circle eighter does not collide the line segment, or
      //it collides the end of the line and therefore the end point must be within circle.
      return c.isPointInCircle(this.from) || c.isPointInCircle(this.to);
    }

    let distance: number = intersection.distance(c.centre);
    return distance < c.radius;*/
  }
  public static createFromABC(a: number, b: number, c: number, p: Point): Line {
    let l: Line = super.createFromABC(a, b, c, p);
    return LineSegment.fromLine(l) as Line;
  }
  /*public createPerpendicular(p: Point): Line {
    var line: Line = super.createPerpendicular(p);
    return LineSegment.fromLine(line) as Line;
  }*/
  public static fromLine(line: Line): LineSegment {
    var ls: LineSegment = new LineSegment(line.from, line.to);
    ls.calculateInvertalsForPoints();
    return ls;
  }
  /**
   * If there is no intersection point, returns null
   * @param  o [description]
   * @return   [description]
   */
  public getIntersectionPoint(o: Line): Point {
    let p = super.getIntersectionPoint(o);
    if (this.isPointOnLine(p.x, p.y))
      return p;
    return null;
  }
  public getPointA(): Point {
    return this._from;
  }
  public getPointB(): Point {
    return this._to;
  }
  public getSmallestX(): number { return this.intervalXMin; }
  public getSmallestY(): number { return this.intervalYMin; }
  public getBiggestX(): number { return this.intervalXMax; }
  public getBiggestY(): number { return this.intervalYMax; }

  public isLineSegment(): boolean { return true; }

  public intersects(other: Line): boolean {
    throw new Error('Not defined yet')
  }
  /**
   * If point is on the line segment, returns true.
   * If point equals the edge pointds of line segment, returns true aswell
   * @param  x [description]
   * @param  y [description]
   * @return   [description]
   */
  public isPointOnLine(x: number, y: number): boolean {
    if (!super.isPointOnLine(x, y))
      return false;

    //console.log(`${x} ${y}`);
    x = Functions.roundNumber(x);
    y = Functions.roundNumber(y);
    //console.log(`${x} ${y}`);

    /*console.log(`${this.intervalXMax} ${this.intervalXMin}`);
    console.log(`${this.intervalYMin} ${this.intervalYMax}`);
*/
    if (x >= this.intervalXMin && x <= this.intervalXMax &&
      y >= this.intervalYMin && y <= this.intervalYMax)
      return true;
    return false;
  }
  public moveBy(v: Vector): void {
    throw new Error('Not supported');
  }
  public touchesCircle(c: Circle): boolean {
    //get intersection points of circle and LINE (not line segment, LINE)

    let line = this as Line;
    let pts = line.getIntersectionPointWithCircle(c);
    /*  console.log()
      console.log('Intersections with circle')
      console.log(pts);*/

    //eliminate intersections that are not on this limited line
    let intersections = new Array<Point>();
    for (let i: number = 0; i < pts.length; i++) {
      if (this.isPointOnLine(pts[i].x, pts[i].y))
        intersections.push(pts[i]);
    }
    /*console.log('Pruned intersections with circle')
    console.log(intersections);*/

    //no intersection point
    if (intersections.length === 0)
      return false;
    //only one intersection point
    if (intersections.length === 1) {
      if (c.isPointInCircle(this.getPointA()) ||
        c.isPointInCircle(this.getPointB())) {
        return false;
      }
      return true;
    }
    //two intersection points
    if (intersections.length == 2) {
      let A = this.getPointA();
      let B = this.getPointB();

      //If one of the intersection points is A, and the other intersection point does not lie on the line segment Ls and the B point lies
      //outside the circle, then they touch

      //find which one of the intersection points is equal to A or B (if any)
      let intersectionLineSegmentPoint = null;
      let otherIntersection = null;
      if (intersections[0].equals(this.getPointA()) || intersections[0].equals(this.getPointB())) {
        intersectionLineSegmentPoint = intersections[0];
        otherIntersection = intersections[1];
      }
      else if (intersections[1].equals(this.getPointA()) || intersections[1].equals(this.getPointB())) {
        intersectionLineSegmentPoint = intersections[1];
        otherIntersection = intersections[0];
      }

      if (intersectionLineSegmentPoint !== null && intersectionLineSegmentPoint.equals(this.getPointA()) && !this.isPointOnLine(otherIntersection.x, otherIntersection.y) && !c.isPointInCircle(B))
        return true;

      if (intersectionLineSegmentPoint !== null && intersectionLineSegmentPoint.equals(this.getPointB()) && !this.isPointOnLine(otherIntersection.x, otherIntersection.y) && !c.isPointInCircle(A))
        return true;

      //if none of intersection points lies on the line segment, they do not touch
      if (!this.isPointOnLine(intersections[0].x, intersections[0].y) && !this.isPointOnLine(intersections[1].x, intersections[1].y))
        return false;
    }
    return false;
  }
}
