import { Angle } from './Angle';
import { Point } from './Point';
import { ANGLE_TYPE } from '../../../Constants';

/**
*/
export class Vector {
  private velocity_x: number;
  private velocity_y: number;

  /**
   * Creates a zero vector, unless you specify the x, y
   */
  constructor(x: number = 0, y: number = 0) {
    this.velocity_x = x;
    this.velocity_y = y;
  }
  /**
   * Given points A, B, creates vector AB
   * @param  a The start point
   * @param  b The end point
   * @return   [description]
   */
  public static createFromPoints(a: Point, b: Point): Vector { return new Vector(b.x - a.x, b.y - a.y); }
  /**
   * Adds vector to this vector
   * @param  v [description]
   * @return   [description]
   */
  public addVector(v: Vector): Vector {
    this.x += v.x;
    this.y += v.y;
    return this;
  }
  /**
   * Adds vector to this vector
   * @param  v [description]
   * @return   [description]
   */
  public addVector_X_Y(x: number, y: number): Vector {
    this.velocity_x += x;
    this.velocity_y += y;
    return this;
  }
  public clone(): Vector { return new Vector(this.x, this.y); }
  /**
   * Divides the vector by a number specified.
   * @param  n If is zero, the operation returns clone of this vector, unchanged
   * @return   The vector after division
   */
  public divide(n: number): Vector {
    if (n == 0) return this.clone();
    return new Vector(this.x / n, this.y / n);
  }
  public equals(vector: Vector): boolean { return (vector.x === this.x && vector.y === this.y); }
  get x(): number { return this.velocity_x; }
  get y(): number { return this.velocity_y; }
  set x(x: number) { this.velocity_x = x; }
  set y(y: number) { this.velocity_y = y; }
  /*isParallel(vector) {
    Angle a( * this, vector, SMALLER);
    return (a.GetValueDegrees() == 180 || a.GetValueRadians() == 0);
  }*/
  public isZero(): boolean { return (this.x === 0 && this.y === 0); }
  /**
   * Returrns the length of the vector
   * @return {[type]} [description]
   */
  public length(): number {
    if (this.x === 0)
      return Math.abs(this.y);
    else if (this.y === 0)
      return Math.abs(this.x);

    var a = this.x * this.x + this.y * this.y;
    return Math.sqrt(a);
  }
  /**
   * Multiplies this vector by number
   * @param  i [description]
   * @return   [description]
   */
  public multiplyByNumber(i: number): Vector {
    this.x *= i;
    this.y *= i;
    return this;
  }
  public multiplyByVector(v: Vector): number {
    return this.x * v.x + this.y * v.y;
  }
  /**
   * Subtracts vector from this vector
   * @param  x [description]
   * @param  y [description]
   * @return   [description]
   */
  public subtractVector(x: number, y: number): Vector {
    this.x -= x;
    this.y -= y;
    return this;
  }
  /**
   * This vector stays unchainged. Returns new vector:
   *
   * Let v=(vx,vy) be this vector.
   *
   * if [vx] or [vy] is decimal number, the final vector will NOT contain decimal number.
   * example:
   * (1.00024, 5)  =>   (1, 5)
   * (1.999999999999, 0.00000000000000000000001)  =>   (1, 0)
   *
   * ATTENTION!
   * This is NOT rounding the decimal, it is removing all the decimal places!
   * @return [description]
   */
  public removeDecimalPart(): Vector {
    return new Vector(Math.trunc(this.x), Math.trunc(this.y));
  }
  /**
   * Resizes the vector in a way that after resizing, the x of the vector will equal the argument specified
   * @param x [description]
   */
  public resizeX(x: number): void {
    if (x === 0) {
      this.x = 0;
      this.y = 0;
      return;
    } else if (x === this.x)
      return;

    var alpha = Angle.createFromVectors(this, new Vector(1, 0), ANGLE_TYPE.SMALLER);

    var multiplier = (this.y < 0) ? -1 : 1;

    this.x = x;

    //tan(alpha) == Y / X
    this.y = multiplier * Math.tan(alpha.getValueRadians()) * Math.abs(this.x);
  }
  /**
   * Resizes the vector in a way that after resizing, the y of the vector will equal the argument specified
   * @param y [description]
   */
  public resizeY(y: number): void {
    if (y === 0) {
      this.x = 0;
      this.y = 0;
      return;
    } else if (y === this.y)
      return;

    var alpha = Angle.createFromVectors(this, new Vector(0, 1), ANGLE_TYPE.SMALLER);

    var multiplier = (this.x < 0) ? -1 : 1;

    this.y = y;

    //tan(alpha) == Y / X
    this.x = multiplier * Math.tan(alpha.getValueRadians()) * Math.abs(this.y);
  }
  /**
   * For visual / debug / console logging only
   * @return [description]
   */
  public toString(): string {
    return `[${this.x};${this.y}]`;
  }
}
