import { Shape, SHAPE_TYPE } from './Shape';
import { Point } from './Point';
import { Circle } from './Circle';
import { Vector } from './Vector';
import { Functions } from '../../Functions';

export class Line extends Shape {
  /**
   * The a, b, c variables in line equation
   */
  private a: number;
  /**
   * The a, b, c variables in line equation
   */
  private b: number;
  /**
   * The a, b, c variables in line equation
   */
  private c: number;

  protected _from: Point;

  protected _to: Point;

  public constructor(from: Point = null, to: Point = null) {
    super(SHAPE_TYPE.LINE);

    if (from === null || to === null) {
      from = new Point();
      to = new Point(100, 0);
    }

    if (from.equals(to))
      throw new Error('Line: Invalid points');

    this._from = from;
    this._to = to;

    this.a = to.y - from.y;
    this.b = from.x - to.x;
    this.c = -1 * (this.a * from.x + this.b * from.y);

    /*console.log(this.a)
    console.log(this.b)
    console.log(this.c)*/

    if (this.b == 0) {
      this.a = 1;
      this.c = -from.x;
    }
    else if (this.a == 0) {
      this.b = 1;
      this.c = -from.y;
    }
    /*console.log()
    console.log(this.a)
    console.log(this.b)
    console.log(this.c)*/
  }
  /**
   * Given points A, B, and line L (this), returns true if A and B lie on same side of line L.
   * If A or B lie ON the line L, then error is thrown
   * @param  A [description]
   * @param  B [description]
   * @param  supressErrors In case when error would be thrown, if this is true, then no error is thrown,
   * and true is returned, understanding that it can be incorrect result.
   * Use this in cases where, if the point would be on line), it really does not matter if true / false is returned....
   * @return   [description]
   */
  public areTwoPointsOnSameSide(A: Point, B: Point, supressErrors: boolean = false): boolean {
    if (this.isPointOnLine(A.x, A.y) || this.isPointOnLine(B.x, B.y)) {
      if (supressErrors)
        return true;
      throw new Error("Point A or B is on the line....");
    }
    let sign1 = this.getSideOnWhichIsPoint(A);
    let sign2 = this.getSideOnWhichIsPoint(B);

    /*console.log(sign1)
    console.log(sign2)*/
    return (sign1 > 0 && sign2 > 0) || (sign1 < 0 && sign2 < 0);
  }
  public calculateY(x: number): number {
    //there are infinite amount of possible y
    if (this.b == 0)
      return 1;
    return ((-1) * this.a * x - this.c) / this.b;
  }
  public clone(): Shape {
    return new Line(this.from, this.to) as Shape;
  }
  public collidesCircle(c: Circle): boolean {
    let intersects = this.getIntersectionPointWithCircle(c);
    return intersects.length == 2;

    //create perpendicular line that runs through centre of this circle
    /*let perpendicular: Line = this.createPerpendicular(c.centre);
    //console.log(`perpend ${perpendicular}`)
    //get intersection of the lines
    let intersection = perpendicular.getIntersectionPoint(this);
    //console.log(`B ${intersection}`)
    //by definition, the intersection point always exists!

    /*console.log(`line ${l.toString()}`);
    console.log(`perpendicular line ${perpendicular.toString()}`);
    console.log(`intersection point ${intersection.toString()}`);
    //get distance of points*/
    /*  let distance: number = intersection.distance(c.centre);
      //console.log(`C ${distance}`)
      /*console.log(`distance ${distance}`);*/
    //return distance < c.radius;*/
  }
  public static createFromABC(a: number, b: number, c: number, p: Point): Line {
    let l = new Line();
    l.a = a;
    l.b = b;
    l.c = c;
    l._from = p.clone();
    l._to = new Point(p.x + 5, l.calculateY(p.x + 5));
    return l;
  }
  public static createFromVector(p: Point, v: Vector): Line {
    return new Line(p, new Point(p.x + v.x, p.y + v.y));
  }
  public createParalell(p: Point): Line {
    if (this.isPointOnLine(p.x, p.y))
      return new Line(this.from, this.to);

    let perp: Line = this.createPerpendicular(p);
    return perp.createPerpendicular(p);
  }
  /**
   * Let L be this line.
   *
   * Creates line LP which is perpendicular to L, and is passing through point p
   * @param  p [description]
   * @return   [description]
   */
  public createPerpendicular(p: Point): Line {
    //this line is parallel with Y axis
    //console.log(`b: ${this.b}`)
    if (this.b === 0)
      return new Line(p, new Point(p.x + 1, p.y));
    let slope = this.getSlope();
    //console.log(`slope: ${slope}`)
    //this line is parallel with X axis
    if (slope === 0) {

      return new Line(p, new Point(p.x, p.y + 1));
    }
    //return Line.createFromABC(1, 0, -p.y, p);

    let slope2 = -1 / slope;
    //get the C parameter
    let newC = p.y - slope2 * p.x;
    return Line.createFromABC(slope2, -1, newC, p);
  }
  private get delta(): Point {
    return new Point(this.to.x - this.from.x, this.to.y - this.from.y)
  }
  /**
   * Let point P be point
   * Let L be this line segment.
   * Let Lp be line, which is parallel to this line, and P lies on Lp.
   * Let O be intersection point between Lp and L (yes, even though the Line is Line segment, then the intersection will not lie on the line segment, but on line created from the line segment)
   * Then returned distance is distance from P to O
   * @param  x [description]
   * @param  y [description]
   * @return   [description]
   */
  public distanceFromPoint(p: Point): number {
    //create perpendicular line to this one, running through desired point
    let perp = this.createPerpendicular(p);
    //console.log(this.toString());
    //console.log(perp.toString());

    let intersectionPoint = perp.getIntersectionPoint(this);

    //if they touch each other
    if (intersectionPoint === null)
      throw new Error('Perpendicular line L1 does not intersect line L? What?');

    //return distance between points
    return intersectionPoint.distance(p);
  }
  public isLineSegment(): boolean { return false; }
  /**
   * Whether the 2 line equations define the same line
   */
  public equals(o: Line): boolean {
    return this.a === o.a && this.b === o.b && this.c === o.c;

    //return this.from.equals(o.from) && this.to.equals(o.to);
  }
  public get from(): Point {
    return this._from;
  }
  public getA(): number {
    return this.a;
  }
  public getB(): number {
    return this.b;
  }
  public getC(): number {
    return this.c;
  }
  /**
   * Returns null if the lines do not intersect.
   * Else, returns the intersection point
   * @return [description]
   */
  public getIntersectionPoint(o: Line): Point {
    let p0 = o.to;
    let p1 = o.from;
    let p2 = this.from;
    let p3 = this.to;
    //  console.log(`p0: ${p0.toString()}, p1: ${p1.toString()}, p2: ${p2.toString()}, p3: ${p3.toString()}`);

    var A1 = p1.y - p0.y;
    var B1 = p0.x - p1.x;
    var A2 = p3.y - p2.y;
    var B2 = p2.x - p3.x;
    /*  console.log(A1);
      console.log(B1);
      console.log(A2);
      console.log(B2);*/
    var denominator = A1 * B2 - A2 * B1;

    if (denominator === 0)
      return null;

    var C2 = A2 * p2.x + B2 * p2.y;
    var C1 = A1 * p0.x + B1 * p0.y;
    /*    console.log(C2);
        console.log(C1);*/
    return new Point((B2 * C1 - B1 * C2) / denominator,
      (A1 * C2 - A2 * C1) / denominator);
  }
  /**
   * Tested, proven to be correct. Do not touch it if you do not have to
   *
   * The discriminant is rounded using precision constant (since discriminant of
   * value 0.000000000000000001 would not equal zero, giving 2 results (both slightly different, but correct))
   * However, for computations that rely on number of solutions, received from this function it would not work....
   * @param  C [description]
   * @return   [description]
   */
  public getIntersectionPointWithCircle(C: Circle): Array<Point> {
    /*  console.log(`getIntersectionPointWithCircle():`)
      console.log(this.toString())
      console.log(C.toString())*/
    let ret = new Array<Point>();

    //************************
    //defines
    //************************
    let c_x = C.x;
    let c_y = C.y;
    let a = this.getA();
    let b = this.getB();
    let c = this.getC();
    let c_r = C.radius;
    //************************

    //circle:  (x - c_x)^2 + (y - c_y)^2 - c_r^2 = 0
    //line:    ax + by + c = 0
    //*******************************

    //if x can be expressed
    if (a != 0) {
      //line:    x = (-b * y - c) / a
      //equation for line + circle
      //        (( (-b * y - c) / a ) - c_x)^2 + (y - c_y)^2 - c_r^2 = 0

      //following was tested by wolfram mathematica for expression equality
      //create polynom in y from equation above
      let polynomA = 1 + (b * b) / (a * a);
      //console.log(`polynomA = ${polynomA}`);
      let polynomB = (2 * b * c) / (a * a) + (2 * b * c_x) / a - 2 * c_y;
      //console.log(`polynomB = ${polynomB}`);
      let polynomC = (c * c) / (a * a) - c_r * c_r + (2 * c * c_x) / a + c_x * c_x + c_y * c_y;
      //console.log(`polynomC = ${polynomC}`);

      let discriminant = polynomB * polynomB - 4 * polynomA * polynomC;

      //console.log(`discriminant is (close to) zero? => ${Functions.equalsZero(discriminant)}`);
      //if discriminant is close to zero, set it to zero
      if (Functions.equalsZero(discriminant))
        discriminant = 0;

      //  console.log(`discriminant = ${discriminant}`);

      //no solution
      if (discriminant < 0)
        return ret;

      let y1 = (-polynomB + Math.sqrt(discriminant)) / (2 * polynomA);
      let y2 = (-polynomB - Math.sqrt(discriminant)) / (2 * polynomA);

      //calculate x
      let x1 = (-b * y1 - c) / a;
      let x2 = (-b * y2 - c) / a;

      ret.push(new Point(x1, y1));
      //if the 2 solutions are different
      if (discriminant != 0)
        ret.push(new Point(x2, y2));
      return ret;
    }
    //if y can be expressed
    else if (b != 0) {
      //line:    y = (-c - a*x) / b
      //equation for line + circle
      //        (x - c_x)^2 + (( (-c - a*x) / b ) - c_y)^2 - c_r^2 = 0

      //following was tested by wolfram mathematica for expression equality
      //create polynom in x from equation above
      let polynomA = 1 + (a * a) / (b * b);
      //console.log(`polynomA = ${polynomA}`);
      let polynomB = (2 * a * c) / (b * b) + (2 * a * c_y) / b - 2 * c_x;
      //console.log(`polynomB = ${polynomB}`);
      let polynomC = (c * c) / (b * b) - c_r * c_r + (2 * c * c_y) / b + c_y * c_y + c_x * c_x;
      //console.log(`polynomC = ${polynomC}`);

      let discriminant = polynomB * polynomB - 4 * polynomA * polynomC;

      //console.log(`discriminant is (close to) zero? => ${Functions.equalsZero(discriminant)}`);

      //if discriminant is close to zero, set it to zero
      if (Functions.equalsZero(discriminant))
        discriminant = 0;

      //  console.log(`discriminant = ${discriminant}`);

      //no solution
      if (discriminant < 0)
        return ret;

      let x1 = (-polynomB + Math.sqrt(discriminant)) / (2 * polynomA);
      let x2 = (-polynomB - Math.sqrt(discriminant)) / (2 * polynomA);

      //calculate y
      let y1 = (-c - a * x1) / b;
      let y2 = (-c - a * x2) / b;

      ret.push(new Point(x1, y1));
      //if the 2 solutions are different
      if (discriminant != 0)
        ret.push(new Point(x2, y2));
      return ret;
    }
    //should never get here, and if so, that means that A, B are both zero, which means that Line is not valid
    //return no intersection points....
    return ret;
  }
  /**
   * Given point P and line L (this), returns 0 if P is on L,
   * negative number if P is on one side of L and positive number otherwise
   * DO NOT RELY ON THIS IN A WAY using it like if result > 0 then do something.
   *
   * The sign does not tell anything by itself, you need context.
   * Use functions that determine if two points are on same side of line, for example.
   * @param  p [description]
   * @return   [description]
   */
  public getSideOnWhichIsPoint(p: Point): number {
    let Xab = this.from.x - this.to.x;
    let Yab = this.from.y - this.to.y;
    let Yap = p.y - this.to.y;
    let Xap = p.x - this.to.x;
    let whatSideIsPointOnInRelatonToLine = Xab * Yap - Xap * Yab;

    /*console.log()
    console.log(whatSideIsPointOnInRelatonToLine);*/
    return whatSideIsPointOnInRelatonToLine;
  }
  public getSlope(): number {
    return -this.a / this.b;
  }
  /**
   * Returns true if the lines do have intersection point
   * If two lines are the same, then returns true
   * @param  other [description]
   * @return       [description]
   */
  public intersects(other: Line): boolean {
    return this.equals(other) || this.getIntersectionPoint(other) !== null;
  }
  public isPointOnLine(x: number, y: number): boolean {
    //console.log(`${this.a} * ${x} + ${this.b} * ${y} + ${this.c} === 0`)
    return Functions.equalsZero(this.a * x + this.b * y + this.c);

    //this is working solution, but because of very small numbers,
    //return (this.a * x + this.b * y + this.c=== 0);
  }
  public moveBy(v: Vector): void {
    throw new Error('Not supported');
  }
  public get to(): Point {
    return this._to;
  }
  public toString(): string {
    return `${this.a} * x + ${this.b} * y + ${this.c} = 0; points from: ${this._from}, to: ${this._to}`;
  }
}
