import { CharacterAnimationBase } from './CharacterAnimationBase';

export class PlayerAnimation extends CharacterAnimationBase {
  public constructor() {
    super('player', true);
  }
}
