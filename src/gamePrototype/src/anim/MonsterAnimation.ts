import { CharacterAnimationBase } from './CharacterAnimationBase';

export class MonsterAnimation extends CharacterAnimationBase {
  public constructor() {
    super('monster', false);
  }
}
