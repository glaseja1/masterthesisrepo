import * as ECSA from '../../libs/pixi-component';
import { Assets, Messages, DIRECTION } from '../Constants';
import { GraphicsAnimation, ANIMATION_DIRECTION, ANIM_MODE } from '../game/graphics/animation/GraphicsAnimation';
import { Point } from '../other/math/geometry/Point';
//only for being able to access the resource loader
import { PlayerFactory } from '../factories/PlayerFactory';

export class CharacterAnimationBase extends ECSA.Component {
  public animation: GraphicsAnimation = null;
  private characterPrefix: string;
  private currAnimAssetName: string = Assets.RESOURCE_IDLE;
  private currentDirection: string;
  private _isPlayer: boolean;

  public constructor(characterPrefix: string, _isPlayer: boolean) {
    super();
    this.characterPrefix = characterPrefix;
    this._isPlayer = _isPlayer;
  }

  public onInit(): void {
    super.onInit();
    //mesages
    this.subscribe(Messages.ANIM_CHANGE);
    this.subscribe(Messages.DIRECTION_CHANGE);

    //defaults
    this.changeAnimationToDirection(DIRECTION.D);

    this.animation.setDirection(ANIMATION_DIRECTION.FORWARD_THEN_BACKWARD);

    this.texture.frame.width = this.animation.getFrameWidth();
    this.texture.frame.height = this.animation.getFrameHeight();

    //this.sendMessage(Messages.UNIT_KILLED, unit);
    this.scene.sendMessage(new ECSA.Message(Messages.TEST, null, null));
  }
  private changeAnimationToDirection(dir: string): void {
    let target: string = `${this.characterPrefix}-${this.currAnimAssetName}-anim`;
    //console.log(target)
    if (PlayerFactory.loader.resources[target] === undefined)
      throw new Error(`Asset '${target}' does not exist...`);

    let JSON: any = PlayerFactory.loader.resources[target].data;
    if (JSON[dir] === undefined)
      throw new Error(`Direction of string [${dir}] is not existent in loaded json...`);

    this.currentDirection = dir;

    //this.texture = PIXI.Texture.from(`${Assets.TEX_PLAYER_GENERAL}${dir}`);
    let sprite = <ECSA.Sprite>this.owner.pixiObj;
    sprite.texture = PIXI.Texture.from(`${this.characterPrefix}-${this.currAnimAssetName}${dir}`);

    this.animation = new GraphicsAnimation(
      JSON[dir]['framesTotal'],
      JSON[dir]['frameSize'][0],
      JSON[dir]['frameSize'][1],
      JSON[dir]['framesPerLine'],
      JSON['animationSpeed']
    );

    var origin: Point = null;
    if (JSON[dir]['origin'] !== undefined) {
      origin = new Point(JSON[dir]['origin'][0], JSON[dir]['origin'][1]);
      this.scene.sendMessage(new ECSA.Message(Messages.ANIM_ORIGIN, null, null, origin));

      this.animation.origin = origin.clone();
    }

    //refresh w-h
    this.texture.frame.width = this.animation.getFrameWidth();
    this.texture.frame.height = this.animation.getFrameHeight();

    if (this.currAnimAssetName == Assets.RESOURCE_ATTACK) {
      this.animation.setDirection(ANIMATION_DIRECTION.FORWARD);
      this.animation.setAnimationMode(ANIM_MODE.FINITE);
    } else if (this.currAnimAssetName == Assets.RESOURCE_IDLE) {
      this.animation.setDirection(ANIMATION_DIRECTION.FORWARD_THEN_BACKWARD);
      this.animation.setAnimationMode(ANIM_MODE.INFINITE);
    } else if (this.currAnimAssetName == Assets.RESOURCE_WALK) {
      this.animation.setDirection(ANIMATION_DIRECTION.FORWARD);
      this.animation.setAnimationMode(ANIM_MODE.INFINITE);
    }
  }
  private isPlayer(): boolean { return this._isPlayer; }
  public onMessage(msg: ECSA.Message): void {
    var id = msg.data.id;
    //if the message is not for this object
    if (id == 'player' && !this.isPlayer())
      return;

    var data = msg.data.data;
    if (msg.action === Messages.DIRECTION_CHANGE) {
      //console.log('character animation received DIRECTION_CHANGE message with data ' + data);
      this.changeAnimationToDirection(data);
    }
    else if (msg.action === Messages.ANIM_CHANGE) {
      if (this.currAnimAssetName == data)
        return;
      //console.log('character animation received ANIM_CHANGE message with data ' + data);
      this.currAnimAssetName = data;

      //update gfx
      this.changeAnimationToDirection(this.currentDirection);
    }
  }
  onUpdate(delta: number, absolute: number) {
    if (this.animation == null) return;
    //owner.pixiObj
    //console.log(this.owner.asSprite().texture)
    if (this.animation.update()) {
      //change frame
      this.texture.frame.x = this.animation.getDrawSrcPosition().x;
      this.texture.frame.y = this.animation.getDrawSrcPosition().y;
      this.texture.updateUvs();

      //if the animation ended, and it was attack animation
      if (this.currAnimAssetName == Assets.RESOURCE_ATTACK && this.animation.isFinished()) {//change anim to idle
        this.currAnimAssetName = Assets.RESOURCE_IDLE;
        //update gfx
        this.changeAnimationToDirection(this.currentDirection);
      }
    }
  }
  private get texture(): PIXI.Texture {
    return this.owner.asSprite().texture;
  }
}
