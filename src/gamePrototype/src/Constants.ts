export enum Assets {
  TEX_MAP_TILE = 'tex-maptile',
  RESOURCE_IDLE = 'idle',
  RESOURCE_WALK = 'walk',
  RESOURCE_ATTACK = 'attack'
}

export enum Attributes {
  DAMAGE_FACTORY = 'attr_damage_factory'
}
/**
 * used for math
 */
export enum ANGLE_TYPE {
  SMALLER,
  GREATER
}
/**
 * To be able to recalculate position to the PIXI's cartesian system
 * @param  angle [description]
 * @return       [description]
 */
export enum POS_CONSTS {
  SHIFT_X = 0,
  SHIFT_Y = 79
}
export enum Messages {
  TEST = 'a',
  ANIM_CHANGE = 'b',
  DIRECTION_CHANGE = 'c',
  MOVE_TO = 'd',
  ANIM_ORIGIN = 'e',
  DAMAGE_RECEIVED = 'f',
  MONSTER_KILLED = 'g'
}
export enum Names {
  PC_PLAYER = 'a'
}

/**
 * Direction (angle) of aniation.
 * Also used for the wall direction.
 * It is shared, because it is used in the editor wall edit packet, so must be known to the server aswell
 */
export enum DIRECTION {
  U = 'U',
  R = 'R',
  L = 'L',
  D = 'D',
  LEFT_UP = 'LU',
  LEFT_DOWN = 'LD',
  RIGHT_UP = 'RU',
  RIGHT_DOWN = 'RD'
}

export enum NODE_TRANSITION {
  MOVE,
  USE_ITEM
}
/**
 * For rectangles and math
 */
export enum TOUCH_TYPE {
  NONE,
  CORNER_UP_LEFT,
  CORNER_DOWN_RIGHT,
  CORNER_UP_RIGHT,
  CORNER_DOWN_LEFT,
  LEFT_SIDE,
  RIGHT_SIDE,
  UP_SIDE,
  DOWN_SIDE
}
