import { ComponentBase } from './ComponentBase';

/**
 * Singleton
 *
 * Allows to control console
 */
export class GameConsoleHTML extends ComponentBase {
  private editorTabEnabled: boolean = false;

  private static instance: GameConsoleHTML;
  private lastSystemCmd: string = "";
  private rowPrototype: any;
  /**
   * The html containers, sorted by tab enum
   */
  private messageTabContainer: any;

  constructor() {
    super('#HUD-gameConsole');

    this.rowPrototype = this.htmlComponent.find('#prototype').find('.row');

    this.messageTabContainer = this.htmlComponent.find('#chat-text-container');
  }
  public clear(): void {
    this.messageTabContainer.empty();
  }
  /**
   * Displays message for player on this PC, informing about some game event
   * @param messageText  raw text to display
   * @param class_       the html class for the row. Use this to style the text
   */
  public displayMessageRaw(messageText: string, class_: string = null,
    username: string = null, isAdmin: boolean = false): void {
    let copy: any = this.rowPrototype.clone();
    copy.attr('id', '');

    if (class_ !== null)
      copy.addClass(class_);

    if (username == null) {
      copy.find('#username').addClass("hidden");
      copy.find('#separator').empty();
    }
    else {
      copy.find('#username').text(username);
      if (isAdmin)
        copy.find('.adminSig').removeClass("hidden");
    }
    copy.find('#text').text(`${messageText}`);

    if (class_ != null)
      copy.find('#text').addClass(class_);

    this.messageTabContainer.append(copy);
    this.scrollToBottom(this.messageTabContainer);
  }
  /**
   * displays message in the SYSTEM tab
   * @param m [description]
   */
  public displaySystemMessage(m: string): void { this.displayMessageRaw(m, null); }
  public static getInstance(): GameConsoleHTML {
    if (this.instance === undefined)
      this.instance = new GameConsoleHTML();
    return this.instance;
  }
  private scrollToBottom(container: any): void {
    //get container scroll height
    var scrollHeight: number = container[0].scrollHeight;
    container.scrollTop(scrollHeight);
  }
}
