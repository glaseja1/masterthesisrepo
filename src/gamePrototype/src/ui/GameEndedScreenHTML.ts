import { ComponentBase } from './ComponentBase';

/**
 * Singleton
 *
 */
export class GameEndedScreenHTML extends ComponentBase {
  private static instance: GameEndedScreenHTML;

  constructor() {
    super('#HUD-GameEndedScreen');

    this.htmlComponent.find('.restartBtn').click(function() {
      location.reload();
    });
  }
  public static getInstance(): GameEndedScreenHTML {
    if (this.instance === undefined)
      this.instance = new GameEndedScreenHTML();
    return this.instance;
  }
}
