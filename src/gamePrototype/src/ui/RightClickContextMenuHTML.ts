import { ComponentBase } from './ComponentBase';

/**
 * Allows to show / hide and setup right click context menu in game world.
 * Singleton, but init() must be called in order to supply console reference.
 * If you do not do it, and continue using the object anyway, then behavior is undefined
 */
export class RightClickContextMenuHTML extends ComponentBase {
  private static instance: RightClickContextMenuHTML;
  /**
   * the html element containing the content of the menu
   */
  private htmlContent: any;
  /**
   * the html element
   */
  private optionPrototype: any;

  private constructor() {
    //request to hide this component using opacity rather than display: none
    super(".HUD-rightClickCtxMenu", true);

    this.htmlContent = this.htmlComponent.find('.menuContent');
    this.optionPrototype = this.htmlComponent.find('#optionPrototype');

    var self: any = this;
    this.htmlComponent.on("mouseleave", function() {
      self.hide();
    });
  }
  public static getInstance(): RightClickContextMenuHTML {
    if (this.instance === undefined)
      this.instance = new RightClickContextMenuHTML();
    return this.instance;
  }
  /**
   * Will delete any options in this class, and will add the options requested
   * @param arrTmp the options must be in array, since the indexer, ehich is passed
   * to a function callback must be primitive (=number), because then it is copied
   * If it would be iterator object, you get reference, and as iteration continues,
   * you never would be able to acess the correct GameInputOption instance
   */
  /*public addOptions(arrTmp: Array<GameInputOption>): void {
    this.clear();

    var self: any = this;
    for (let i: number = 0; i < arrTmp.length; i++) {
      //copy the prototype
      let copy = this.optionPrototype.clone();
      copy.attr('id', '');
      copy.removeClass("hidden");

      if (arrTmp[i].hasLevel()) {
        copy.find('#level-text').removeClass('hidden');
        if (arrTmp[i].isLevelSmallerThan())
          copy.find('#level-text').addClass('-green');
        else if (arrTmp[i].isLevelGreaterThan())
          copy.find('#level-text').addClass('-red');
        else
          copy.find('#level-text').addClass('-orange');

        copy.find('#targetLevel').text(`${arrTmp[i].level}`);
      }

      copy.find('#text').text(`${arrTmp[i].actionWord} `);
      if (arrTmp[i].hasObjectToInteractWith())
        copy.find('.objectToInteract').text(`${arrTmp[i].objectToInteractWith}`);

      copy.click(function() {
        self.hide();
        if (arrTmp[i].callback)
          arrTmp[i].callback();
      });
      this.htmlContent.append(copy);
    }
  }*/
  /**
   * Adds option to the context menu.
   * The options are then displayed from top down in order how they were added
   * @param  mainText                The main text
   * @param  objectToInteract Name of object to interact with (only for displaying purposes, can be empty)
   * @return                  [description]
   */
  public addOption(mainText: string, objectToInteract: string, onClickCallback: () => void): void {
    //copy the prototype
    let copy = this.optionPrototype.clone();
    copy.removeClass("hidden");

    var self: any = this;
    copy.click(function() {
      self.hide();

      if (onClickCallback)
        onClickCallback();
    });

    this.fillRowPrototypeWithData(copy, mainText, objectToInteract);
    this.htmlContent.append(copy);
  }
  /**
   * removes all content from the menu
   */
  public clear(): void {
    this.htmlContent.empty();
  }
  private fillRowPrototypeWithData(prototypeObject: any, actionText: string, objectToInteract: string,
    level: number = null): void {
    prototypeObject.find('#text').text(`${actionText} `);
    prototypeObject.find('.objectToInteract').text(`${objectToInteract}`);
    if (level !== null)
      prototypeObject.find('#targetLevel').text(`${level}`);
  }
  /**
   * Set postition of context menu to specified position.
   * If the menu would be displayed outside of screen, the position is altered to prevent it
   * (except when menu is so big so that is must overflow)
   *
   * The window will be displayed so that it's X centre is on X specified
   * @param x position within the screen
   * @param y position within the screen
   */
  public setPosition(x: number, y: number): void {
    //in order to get the correct component size,
    //the html element must NOT have display: none.

    //console.log("Setting right click menu position:");

    //convert   12px -> 12
    let h: number = parseInt(this.htmlComponent.css('height'), 10);
    let w: number = parseInt(this.htmlComponent.css('width'), 10);

    let W_h: number = $(window).height();
    let W_w: number = $(window).width();

    /*console.log("Component size: " + w + ", " + h);
    console.log("Window size: " + W_w + ", " + W_h);*/

    if (x + w / 2 > W_w)
      x = W_w - w / 2;
    if (y + h > W_h)
      y = W_h - h;

    x -= w / 2;
    if (x < 0)
      x = 0;
    if (y < 0)
      y = 0;

    this.htmlComponent.css('left', x);
    //small offset for y aswell
    this.htmlComponent.css('top', y - 2);
  }
}
