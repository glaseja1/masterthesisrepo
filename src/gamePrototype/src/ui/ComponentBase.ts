/**
 * Allows to hide / show the component or ask whether it is visible
 */
export class ComponentBase {
  private hideUsingOpacity: boolean;
  private htmlSelector: string;
  /**
   * The JQUERY element found using the selector provided in the constructor
   */
  protected htmlComponent: JQuery<HTMLElement>;
  protected setToCentreOfScreenWhenShownX: boolean = false;
  protected setToCentreOfScreenWhenShownY: boolean = false;
  private visible: boolean = false;

  /**
   * [constructor description]
   * @param htmlSelector     [description]
   * @param hideUsingOpacity if true, the html element will not be hidden by setting display to none, but using opacity.
   * This is needed in case of getting sizes of html component, before showing it (right click contect menu).
   * And if the element is absolutely positioned, is does not matter, that it will be taking space in DOM container
   */
  constructor(htmlSelector: string, hideUsingOpacity: boolean = false) {
    this.htmlSelector = htmlSelector;
    this.htmlComponent = $(this.htmlSelector);
    if (this.htmlComponent.length === 0)
      console.error(`HTML component with selector '${this.htmlSelector}' was not found`);
    this.hideUsingOpacity = hideUsingOpacity;
  }
  /**
   * Will set the [left] and [top] css arguments of the htmlControl to position the HTML control
   * in the centre of the screen
   */
  /*protected calcPositionToCentreOfScreen(): void {
    this.htmlComponent.css('left', $(window).width() / 2 - this.htmlComponent.width() / 2);
    this.htmlComponent.css('top', $(window).height() / 2 - this.htmlComponent.height() / 2);
  }*/
  public getHtmlSelector(): string { return this.htmlSelector; }
  public hide(): void {
    this.visible = false;
    if (this.hideUsingOpacity)
      this.htmlComponent.addClass('hidden-opacity');
    else
      this.htmlComponent.addClass('hidden');
  }
  public isVisible(): boolean { return this.visible; }
  /**
   * Called AFTER the window is shown, when it was hidden.
   * You can override it
   */
  protected onShow(): void { }
  /**
   * If true, then every time, when show() is called,
   * the position of the control is calculated so that it will be at the screen centre.
   * Does not work for window resizing
   */
  protected setToCentreOfScreenWhenShown(state: boolean = true): void {
    this.setToCentreOfScreenWhenShownX = state;
    this.setToCentreOfScreenWhenShownY = state;
  }
  public show(): void {
    this.visible = true;
    if (this.hideUsingOpacity)
      this.htmlComponent.removeClass('hidden-opacity');
    else
      this.htmlComponent.removeClass('hidden');
    if (this.setToCentreOfScreenWhenShownX)
      this.htmlComponent.css('left', $(window).width() / 2 - this.htmlComponent.width() / 2);
    if (this.setToCentreOfScreenWhenShownY)
      this.htmlComponent.css('top', $(window).height() / 2 - this.htmlComponent.height() / 2);
    this.onShow();
  }
  /**
   * [showOrHide description]
   * @param state If true, this component will be shown. Else, it will be hidden
   */
  public showOrHide(state: boolean): void { if (state) this.show(); else this.hide(); }
  protected showInputIsRequired(toggle: boolean, component: any): void {
    if (toggle)
      component.addClass('error-required');
    else
      component.removeClass('error-required');
  }
  /**
   * If component is hidden, then it will be shown and vice versa
   */
  public toggleShow(): void {
    if (!this.visible)
      this.show();
    else
      this.hide();
  }
}
