import { Point } from '../other/math/geometry/Point';
import { PositionModulator } from './map/PositionModulator';

/**
 * @brief Base for any object that will have a position in world and will have to be translated to Screen coordinates for drawing.
Also, can have various collisions (more collision rectangles and collision types).
Also, can have a physics enabled for gravity movement.
Another movement (moving left, right) is implemented in Character
*
* <b> Segment collision resolving </b>
* From all rectangles bound to this item (draw rectangle, collision rectangles (use, physics...)) \n
* Is created new rectangle such as it contains all other rectangles inside.
* That rectabngle is later reffered to for finding out collision with segment.
* Finding such rectangle is quite slow operation, and should not be done alot.
* However, rectangle will be recounted <b> every time, size of any rectangle changes, or upon calling RecountSegmentColRectangle() </b>
* \n \n
* In game, sizes of rectangles are not being changed, only positions, but in way that would not cause change of segment collision rectangle.
* In game is relied on that, and the rectangle is <b>not</b> automatically recounted. \n
* Only when size is changed.
* Constructors create object with no angle rotation when drawing, no flip, and [0, 0] point as origin (wich is upper left corner) \n
* Origin is counted towards WorldCollisionRectangle
 */
export class GameObject {
  private _id: number | string;

  /**
   * The position of the object, in real world coordinates
   */
  private _origin: Point;
  /**
   * The position of the object, in virtual array index coordinates
   * (modulated to the grid, so grid indexes...)
   * It is always kept up to date (good cache for mostly static objects....)
   */
  private _originVAI: Point;
  /**
   */
  constructor(id: number | string) {
    this._id = id;
    this._origin = new Point();
    this._originVAI = new Point();
  }
  //***************************************************************************
  /**
   * Computes the distance of line connecting origins of the two objects
   * @param  {[type]} otherGameObject [description]
   * @return {Number}                 Number telling the distance of line connecting origins of the two objects
   * In real numbers (not in VAI coordinates)
   */
  public distance(otherGameObject: GameObject): number {
    if (otherGameObject.equals(this))
      return 0;
    return this.distanceFromPoint(otherGameObject.x, otherGameObject.y)
  }
  /**
   * Computes the distance of line connecting point and origin of the object
   * @param  {[type]} otherGameObject [description]
   * @return {Number}                 Number telling the distance of line connecting origins of the two objects
   * In real numbers (not in VAI coordinates)
   */
  public distanceFromPoint(x: number, y: number): number {
    let difx: number = this.x - x;
    let dify: number = this.y - y;
    return Math.sqrt(difx * difx + dify * dify);
  }
  /**
   * Returns whether the object IDs equal / differ
   * @param  obj [description]
   * @return     [description]
   */
  public equals(obj: GameObject): boolean { return obj.id === this.id; }
  public get id(): string | number { return this._id; }

  /**
   * Called when position of this object changes. Updates all the positions of the collision rectngles and shapes connected to this object
   * @return {[type]} [description]
   */
  protected _onPositionChanged(): void { }
  /**
   * You may override this from other classes....
   * Called when VAI index changes...
   */
  protected _onVaiPositionChanged(): void { }
  /**
   * [*] Changes the position of the origin and fires _onPositionChanged() event,
   * if the position is not same as the position before call
   * [*] updates the object virtual array index position,
   * and fires _onVaiPositionChanged(), if the vai index changes...
   * [*] raises flag in network cache, for position data to be sent
   * @param  x [description]
   * @param  y [description]
   * @param  interpolate Sent to the client (if data is sent at all) along with the position.
   * If false, then client will not interpolate this position change
   * @return {[type]}   [description]
   */
  public setPositionOrigin(x: number, y: number, interpolate: boolean = true): void {
    if (this._origin.x === x && this._origin.y === y)
      return;
    this._origin.x = x;
    this._origin.y = y;
    this._onPositionChanged();

    //update vai (if needed)
    //Note: the functions in PositionModulator are sortof computation heavy....
    //make sure all is cached to minimize call rate...if possible
    let vai: Point = PositionModulator.getVirtualArrayIndex(x, y);
    if (this._originVAI.equals(vai))
      return;

    this._originVAI = vai;
    this._onVaiPositionChanged();
  }
  /**
   * Sets the object position origin to be at centre of the VAI square
   * @param  x vai coordinate
   * @param  y vai coordinate
   * @param  interpolate Sent to the client (if data is sent at all) along with the position.
   * If false, then client will not interpolate this position change
   */
  public setPositionVai(x: number, y: number, interpolate: boolean = true): void {
    let p: Point = PositionModulator.getSquareCentrePosition(x, y);
    this.setPositionOrigin(p.x, p.y, interpolate);
  }
  /**
   * Allows to get the object's position within the grid (VAI).
   * This is returned as independent clone, and it is impossible to use this getter to modify the position
   * of this object
   * @return Clone of the virtual array index position
   */
  public get vai(): Point { return this._originVAI.clone(); }
  /**
   * x position of object (in real numbers)
   * @return [description]
   */
  public get x(): number { return this._origin.x; }
  /**
   * y position of object (in real numbers)
   * @return [description]
   */
  public get y(): number { return this._origin.y; }
}
