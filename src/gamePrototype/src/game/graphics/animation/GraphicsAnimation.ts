import { Point } from '../../../other/math/geometry/Point';
import { Timer } from '../../../other/Timer';
import { Functions } from '../../../other/Functions';

export enum ANIMATION_DIRECTION {
  FORWARD,
  BACKWARD,
  FORWARD_THEN_BACKWARD
}
export enum ANIM_MODE {
  INFINITE,
  FINITE
}
/**
 * Can not be called as 'Animation' because of collision with name in typescript
 * Represents an animation. Can draw animation frames
 * in different modes. uses Graphics to draw them.
 *
 * Essentially, just cuts of some image portion from animation sheet and draws it
 * If an instance of GraphicsAnimation is created, it can be used for drawing and updating one animation. However, if there are eight animations of a character (each from a different angle and having other sprite sheet), 8 instances of GraphicsAnimation class must be created. Then, some game logic must be implemented in your project to choose which one of these animations to use, based on the character movement currently. 
 */
export class GraphicsAnimation {
  /**
   * True if the animation was originaly set as ANIMATION_DIRECTION.FORWARD_THEN_BACKWARD
   */
  private animationBackAndForth: boolean = false;
  private animationDirection: ANIMATION_DIRECTION;
  /*
  * if true, the animation will never switch a frame, until unfreezed
   */
  private animationFrozen: boolean = false;
  private animMode: ANIM_MODE;
  /**
   * First frame is 0
   */
  private currentFrame: number = 0;

  /**
   * You can choose custom end frame.
   * Can be null
   */
  private customEndFrame: number = null;
  private customStartFrame: number = null;

  private drawSrcPosition: { x: number, y: number };

  private _flippedVertically: boolean = false;
  private frameH: number;
  private frameW: number;

  private framesPerLine: number;
  private framesTotal: number;
  /**
   * Origin offset for drawing the animation
   */
  public origin: Point = new Point();
  private speed: number;
  private timer: Timer;

  /**
   * The constructor takes the animation data as arguments. Namely, it is the total amount of frames in the sprite sheet, width and height of one frame (in pixels),
number of frames per line in the sprite sheet, and the animation speed (in milliseconds). This data can be found in the animation JSON, which is created along with the sprite sheet texture.
   * @param framesTotal
   * @param frameW
   * @param frameH
   * @param framesPerLine
   * @param speed
   */
  public constructor(framesTotal: number,
    frameW: number,
    frameH: number,
    framesPerLine: number,
    speed: number) {
    this.animationDirection = ANIMATION_DIRECTION.FORWARD;

    this.animMode = ANIM_MODE.INFINITE;
    this.frameW = frameW;
    this.frameH = frameH;
    this.framesTotal = framesTotal;
    this.framesPerLine = framesPerLine;

    this.drawSrcPosition = {
      x: 0,
      y: 0
    }

    this.speed = speed;
    this.timer = new Timer(speed);
  }
  /**
   * This function advances the animation to the next frame. It respects the animation mode (finite/infinite) and animation direction (forward/backward).
   */
  private advanceToNextFrame(): void {
    if (this.animationBackAndForth) {
      if (this.animationDirection === ANIMATION_DIRECTION.FORWARD) {
        if (this.currentFrame >= this.getLastFrame()) //this was the last frame
        {
          this.animationDirection = ANIMATION_DIRECTION.BACKWARD;
          this.currentFrame--;
        }
        else
          this.currentFrame++;
      }
      else if (this.animationDirection === ANIMATION_DIRECTION.BACKWARD) {
        if (this.currentFrame <= this.getFirstFrame()) //this was the first frame
        {
          if (this.animMode === ANIM_MODE.INFINITE) {
            this.animationDirection = ANIMATION_DIRECTION.FORWARD;
            this.currentFrame++;
          }
        }
        else
          this.currentFrame--;
      }
    }
    else if (this.animationDirection === ANIMATION_DIRECTION.FORWARD) {
      if (this.currentFrame >= this.getLastFrame()) //this was the last frame
      {
        if (this.animMode === ANIM_MODE.INFINITE)
          this.currentFrame = this.getFirstFrame();
      }
      else
        this.currentFrame++;
    }
    else {//backward
      if (this.currentFrame === this.getFirstFrame()) //this was the first frame
      {
        if (this.animMode === ANIM_MODE.INFINITE)
          this.setFrameLast();
      }
      else
        this.currentFrame--;
    }
    this.computeDrawSrcPosition();
  }
  /**
   * Will set custom boundaries for the animation.
   * For example, having five frames of animation, we can force the instance to animate only from frame 2 to 4 by calling animateFromTo(2, 4).
   * @param start
   * @param end
   */
  public animateFromTo(start: number, end: number): void {
    if (start < 0)
      throw new Error(`min frame allowed: 0`);

    this.setFrame(start);

    if (end >= this.framesTotal)
      throw new Error(`max frame allowed: ${this.framesTotal - 1}`);
    this.customStartFrame = start;
    this.customEndFrame = end;
  }
  /**
   * Creates an independent copy of the instance.
   */
  public clone(): GraphicsAnimation {
    let a: GraphicsAnimation = new GraphicsAnimation(
      this.framesTotal,
      this.frameW,
      this.frameH,
      this.framesPerLine,
      this.speed
    );
    a.animMode = this.animMode;
    a.customStartFrame = this.customStartFrame;
    a.customEndFrame = this.customEndFrame;
    a.animationBackAndForth = this.animationBackAndForth;
    a.origin = this.origin.clone();
    return a;
  }
  /**
   * Will calculate the X and Y coordinates within the sprite sheet texture. This position refers to the upper left corner of the current animation frame and can be used for drawing.
This function is called automatically when a frame changes. For retrieving this position, call getDrawSrcPosition().
   */
  public computeDrawSrcPosition(): void {
    this.drawSrcPosition.x = (this.currentFrame % this.framesPerLine) * this.frameW;
    this.drawSrcPosition.y = (Math.floor(this.currentFrame / this.framesPerLine)) * this.frameH;
  }
  /**
   * Freezes the animation at current frame.
   * Frozen animation does not change frame untill unfrozen
   * Unfreezing can be achieved by calling this function with an argument with a value of false.
   * @param state
   */
  public freeze(state: boolean): void { this.animationFrozen = state; }
  public getCurrentFrame(): number { return this.currentFrame; }
  /**
   * Returns the X / Y coord of the texture src rect
   * @return
   */
  public getDrawSrcPosition(): { x: number, y: number } {
    return this.drawSrcPosition;
  }
  /**
   * Will return a zero-based number, representing the first frame. If animateFromTo() was called to modify the start frame, the used value is returned.
   * @return
   */
  private getFirstFrame(): number {
    if (this.customStartFrame !== null)
      return this.customStartFrame;
    return 0;
  }
  /**
   * Returns the number of frames per line in the sprite sheet.
   */
  public getFramesPerLine(): number { return this.framesPerLine; }
  /**
   * Returns the total number of frames in the sprite sheet.
   */
  public getFramesTotal(): number { return this.framesTotal; }
  /**
   * Returns the height of frames in the sprite sheet. The value is in pixels.
   */
  public getFrameHeight(): number { return this.frameH; }
  /**
   * Returns the width of frames in the sprite sheet. The value is in pixels.
   */
  public getFrameWidth(): number { return this.frameW; }
  /**
   * Will return a zero based number, representing the last frame. If animateFromTo() was called to modify the end frame, the used value is returned.
   */
  private getLastFrame(): number {
    if (this.customEndFrame !== null)
      return this.customEndFrame;
    return this.framesTotal - 1;
  }
  /**
   * Returns the speed of the animation
   * @return
   */
  public getSpeed(): number { return this.speed; }
  /**
   * If the animation mode is INFINITE, never returns true;
   * Else, If the last / first frame was reached, returns true
   * @return
   */
  public isFinished(): boolean {
    if (this.animMode === ANIM_MODE.INFINITE)
      return false;

    return (this.animationDirection === ANIMATION_DIRECTION.BACKWARD && this.currentFrame === this.getFirstFrame()) ||
      (this.animationDirection === ANIMATION_DIRECTION.FORWARD && this.currentFrame == this.getLastFrame());
  }
  /**
   * Selects any random valid frame to start animation at...
   */
  public randomizeStart(): void {
    this.setFrame(Functions.generateRandomNumber(this.getFirstFrame(), this.getLastFrame()));
  }
  /**
   * Sets the animation’s mode. Finite and infinite modes are available. Finite mode means that after reaching the last frame of the animation, the instance will no longer be changing to the next frame. If the mode is infinite, then the first frame is chosen again after reaching the last frame, causing the animation to repeat infinitely (or until stopped).
   */
  public setAnimationMode(m: ANIM_MODE): void { this.animMode = m; }
  /**
   * Sets the animation direction (foward / backward)
   * @param d
   */
  public setDirection(d: ANIMATION_DIRECTION): void {
    if (d == ANIMATION_DIRECTION.FORWARD_THEN_BACKWARD) {
      this.animationBackAndForth = true;
      this.animationDirection = ANIMATION_DIRECTION.FORWARD;
      return;
    }
    this.animationDirection = d;
  }
  public setFlippedVertically(v: boolean): void { this._flippedVertically = v; }
  /**
   * Allows setting animation’s current frame manually. Will throw an error if the animation frame is not valid. Also, automatically calls computeDrawSrcPosition().
   */
  public setFrame(frame: number): void {
    if (frame >= this.framesTotal)
      throw new Error(`max frame allowed: ${this.framesTotal - 1}`);
    if (frame < 0)
      throw new Error(`min frame allowed: ${0}`);
    this.currentFrame = frame;
    //this.customEndFrame = null;
    //this.customStartFrame = null;
    this.computeDrawSrcPosition();
  }
  /**
   * Sets the current frame to the first one
   */
  public setFrameFirst(): void { this.setFrame(0); }
  /**
   * Sets the current frame to the last one
   */
  public setFrameLast(): void { this.currentFrame = this.getLastFrame(); }
  /**
   * Sets the speed of the animation in milliseconds.
   */
  public setSpeed(s: number): void { this.speed = s; }
  /**
   * Updates the animation frames / timer
   * Must be called whenever the game update loop executes. Calculates the time delay between the last call and switches to the next frame of the animation if the time is up. The time to wait between frames can be set using the setSpeed() function or constructor. The update function returns true if the animation frame was changed or false otherwise.
   * @return true if the animation changed frame
   */
  public update(): boolean {
    if (this.animationFrozen)
      return false;

    if (!this.isFinished() && this.timer.update()) {
      this.advanceToNextFrame();
      this.timer.reset();
      return true;
    }
    return false;
  }
}
