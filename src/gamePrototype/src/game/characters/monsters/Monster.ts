import * as ECSA from '../../../../libs/pixi-component';
import { MonsterInfoDAO } from './MonsterInfoDAO';
import { Character } from '../Character';
import { Messages } from '../../../Constants';

/**
 * Represents any monster
 * By default, it does not have prayer available
 */
export class Monster extends Character {
  /**
   * Whether monster is agressive towards all players
   */
  public aggressive: boolean = false;

  /**
   * Info about monster, taken from database
   */
  public infoDAO: MonsterInfoDAO;
  /**
   * Allows to set callback listener for when the monster is killed.
   * Used by:
   * Spawn points
   * Useable items guarded by packs of monsters
   */
  public onKilled: () => void = null;

  constructor(_id: number) {
    super(_id, false);

    this.infoDAO = new MonsterInfoDAO();
  }
  public isPoisonous(): boolean { return this.infoDAO.isPoisonous(); }
  public get level(): number { return this.infoDAO.level; }
  /**
   * OVERRIDE
   * Drops an item.
   * If this monster is used on client, override this method to do nothing
   */
  protected _onHealthZero(): void {
    this.scene.sendMessage(new ECSA.Message(Messages.MONSTER_KILLED, null, null, this));
    //remove from scene
    this.owner.remove();
  }
  public update(): boolean {
    if (this.isDead())
      return false;
    return super.update();
  }
}
