/**
 * Represents info structure for a monster.
 * Also is used for getting data from the database.
 * Array of this is also sent to client on startup
 */
export class MonsterInfoDAO {
  /**
   * used for referring to specific monster from code (better than using an ID)
   * can be null
   */
  public codeTag: string = null;
  public description: string;
  /**
   * name displayed to the client
   */
  public displayName: string;

  /**
   * NOT SERIALIZED as it is not needed
   * ID of drop table from which drop is chosen
   */
  public dropTableID: string;

  /**
   * Health this monster has.
   * The health is passed to character class, however, it needs to be sent to client somehow anyway...
   */
  public healthMax: number;

  /**
   * The level of monster
   */
  public level: number;
  /**
   * The sql id of the monster
   */
  public monsterID: number = 0;

  /**
   * NOT SERIALIZED as it is not needed
   * if true, the monster is not allowed to mmove at all
   */
  public movementDisabled: boolean = false;
  /**
   * can be 0, which means that monster is not poisonous,
   * so > 0 which means that monster can deal poison damage of that value
   */
  public poisonMaxDamage: number = 0;

  /**
   * Where appropriate textures / audio for that monster can be found....
   */
  public resourceDir: string;
  public xpMod: number = 1;

  public constructor() { }
  public isPoisonous(): boolean {
    return (this.poisonMaxDamage != 0);
  }
}
