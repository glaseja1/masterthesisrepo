import * as ECSA from '../../../libs/pixi-component';
import { Timer } from '../../other/Timer';
import { HashSet } from '../../other/containers/hashSet/HashSet';
import { HashMap } from '../../other/containers/hashMap/HashMap';
import { Player } from './player/Player';
import { Assets, Messages, Attributes } from '../../Constants';
import { Point } from '../../other/math/geometry/Point';
import { PositionModulator } from '../../game/map/PositionModulator';
import { DamageFactory } from '../../factories/DamageFactory';
import { Functions } from '../../other/Functions';
import { GameConsoleHTML } from '../../ui/GameConsoleHTML';

/**
 * Creates character that has already basic setup.
 * It has Hit_BOX collision rectangle already added
 */
export class Character extends ECSA.Component {
  /**
   * Not null, if this character attacks someone
   */
  private attackTarget: Character = null;
  /**
   * Shared timer for:
   * [*] combat attacks
   * [*] consimable eating
   *
    * Used for managing delay between item use / attacks
    * Controller can acess this, and even change the time delay
   */
  public attackTimer: Timer = new Timer(3000);
  /**
   * The current point for animation
   */
  protected currentAnimationOrigin: Point = new Point();

  /**
   * Used for detemining, who will see the drop (in case of player, used only in PVP or minigame)
   * key is ID of player, value is number representing the total damage done.
   */
  protected damageReceivedFromPlayers: HashMap<string | number, number> = new HashMap<string, number>();

  protected factoryDamage: DamageFactory;
  protected feetPosition: Point = new Point();
  //public futureDamage: CharacterFutureDamage;
  /**
   * Whether the character is invincible.
   * Used by cheats or trainign dummies (as training dummy can not die)
   */
  private _godMode: boolean = false;
  /**
   * The character's current health.
   * Is always whole integer (all heal / damage functions set health, etc are rounding the values to nearest integer)
   */
  private _health: number = 20;
  /**
   * The actual percentage of health for the character
   * From interval <0,1>
   */
  private _healthPercentage: number = 1;
  /**
   * The unique identifier of the character
   */
  private _characterId: number;
  /**
   * The character's base max health (excluding item / buff effects).
   * If you want current max health, where irtem and stat modifiers are included,
   * use the getter
   * Is always whole integer (all heal / damage functions set health, etc are rounding the values to nearest integer)
   */
  public _maxHealth: number = 100;

  /**
   * The id of the player who did the most damage to the character (can be null)
   * Set when called _onHealthZero
   */
  protected mostDamagePlayerID: string | number = null;
  /**
   * Timer used for player movement.
   * This really can not be in CharacterCommand, because if the command is aborted, the time must be measured anyway
   */
  public moveTimer: Timer = new Timer(30);
  /**
   * Counts time. When time is up, character is considered to be out of combat
   */
  private outOfCombatTimer: Timer;
  private player: boolean;
  /**
   *
   * Character is created with 100 health as default
   * @param _characterId [description]
   * @param player       Whether this character is a player
   */
  protected constructor(_characterId: number, player: boolean = false) {
    super();
    this._characterId = _characterId;
    //this.futureDamage = new CharacterFutureDamage(this);

    //this.factoryDamage = this.scene.getGlobalAttribute<DamageFactory>(Attributes.DAMAGE_FACTORY);
    //30 seconds
    this.outOfCombatTimer = new Timer(30 * 1000);
    this.outOfCombatTimer.stop();
  }
  /**
   *
   * @param t Can be null to stop attacking
   */
  public attack(t: Character): void {
    this.attackTarget = t;
  }
  public clearAttackTarget(): void {
    this.attackTarget = null;
  }
  /**
 * clears the cache for most damage player ID,
 * and data telling who dealth how much damage
 */
  public clearDamageReceivedFromPlayers(): void {
    this.mostDamagePlayerID = null;
    this.damageReceivedFromPlayers.clear();
  }
  /**
   * Reduces character's health by some amount
   * Also builds up network data cache for data to be sent to client later
   * @param  {[type]} by  even if is not whole number, is is rounded down to nearest integer
   * If you pass here 0, it means, that damage was blocked
   * If value is < 0, it is considered as poison damage
   * If value is > 0, it is considered as normal damage
   *
   * Poison damage can not be critical, and even if set as critical, is is not displayed on clients differently
   *
   * @param  number abilityID can be null, if null, character who dealt damage will not get xp
   * @param  {[type]} from can be null, if null, character who dealt damage will not get xp
   * @return {Boolean}    True if character died. False if is still alive
   */
  public damage(by: number, from: Character, abilityID: number = null, critical: boolean = false): boolean {
    if (this.isDead())
      return true;

    by = Math.floor(by);
    let poison: boolean = (by < 0);
    if (by < 0) by *= -1;

    //reset out of combat timer
    this.outOfCombatTimer.reset();

    //if the damage was inflicted by player
    if (from !== null && from.isPlayer() && from.id != this.id) {
      //damage from player
      if (this.damageReceivedFromPlayers.contains(from.id)) {
        //increase the number of damage received from that particular player
        this.damageReceivedFromPlayers.insertOverwrite(from.id,
          this.damageReceivedFromPlayers.get(from.id) + by);
      }
      else
        this.damageReceivedFromPlayers.insert(from.id, by);
    }

    //console.log(`${this._health} - ${by} <= 0    ${this._health - by <= 0}`)
    //character would die
    if (this._health - by <= 0) {

      if (this._godMode) {
        this.healToMax();
        return false;
      }

      if (from !== null && from.isPlayer() && from.id != this.id) {
        if (this.damageReceivedFromPlayers.contains(from.id)) {
          //increase the number of damage received from that particular player
          this.damageReceivedFromPlayers.insertOverwrite(from.id,
            this.damageReceivedFromPlayers.get(from.id) + this._health - by);
        }
        else
          this.damageReceivedFromPlayers.insert(from.id, this._health - by);
      }
      //create gfx
      DamageFactory.createDamageFor(this.owner, this._health);
      //this.scene.sendMessage(new ECSA.Message(Messages.DAMAGE_RECEIVED, null, null, this._health));
      //  console.log('killing character')
      this.kill();
      return true;
    }

    //set the attacker as target, if no target is set,
    //and if auto retailate is [on]
    /*  if (this.ai.combat.getAutoRetaliate() && !this.ai.combat.hasValidTarget() && from !== null)
        this.ai.addCommandAttack(from);*/

    this._health -= by;
    //create gfx
    DamageFactory.createDamageFor(this.owner, by);
    //this.scene.sendMessage(new ECSA.Message(Messages.DAMAGE_RECEIVED, null, null, by));
    this.onHealthChanged();
    return false;
  }
  public getLostHealth(): number { return this.maxHealth - this._health; }
  /**
   * Heals the character by specified amount of health points
   * @param by Non negative number
   * @param displayGraphically Whether it should be graphically displayed on the client side
   * as a hitsplat
   * @return true if any health was healed, false if no health was healed
   */
  public heal(by: number, displayGraphically: boolean = true): boolean {
    if (this._health === this.maxHealth || by < 1)
      return false;
    by = Math.floor(by);

    //here, the GETTER [maxHealth] MUST BE USED!!!
    if (this._health + by > this.maxHealth) {
      this._health = this.maxHealth;
    }
    else {
      this._health += by;
    }
    this.onHealthChanged();
    return true;
  }
  /**
   *
   * @param displayGraphically Whether it should be graphically displayed on the client side
   * as a hitsplat
   */
  public healToMax(displayGraphically: boolean = true): void { this.heal(this.maxHealth, displayGraphically); }
  public hasFullHealth(): boolean { return this._health >= this.maxHealth; }
  public get health(): number { return this._health; }
  /**
   * Returns percentage of health from interval <0,1>
   * @return [description]
   */
  public get healthPercentage(): number { return this._healthPercentage; }
  public get characterID(): number { return this._characterId; }
  public isDead(): boolean { return this._health <= 0; }
  /**
   * Whether character is currently in combat
   * Note: The state lasts also X seconds after the combat ends
   * @return
   */
  public isInCombat(): boolean { return this.outOfCombatTimer.isRunning(); }
  public isPlayer(): boolean { return this.player; }
  public isMonster(): boolean { return !this.player; }
  public kill(): void {
    if (this.isDead())
      return;

    this._health = 0;
    this._onHealthZero();
    this.onHealthChanged();
  }
  public get level(): number { return 1; }
  /**
   * Returns character's base max health plus / minus all stat, buffs, item modifiers that modify it.
   * @return [description]
   */
  public get maxHealth(): number { return this._maxHealth; }
  /**
   * You can override this
   */
  protected onAttacked(): void { }
  protected onHealthChanged(): void {
    //calculate percentage
    //max .... 100%
    //current  x
    if (this.maxHealth === 0)
      this._healthPercentage = 0;
    else
      this._healthPercentage = this.health / this.maxHealth;
  }
  /**
   * stops movement, invalidates attack key, clears combat target,
   * sets character as out of combat
   */
  protected _onHealthZero(): void {
    if (this._godMode)
      return;

    //find player who did most damage (if null, it is ok...)
    this.mostDamagePlayerID = null;
    var mostDamage: number = 0;
    for (let i = this.damageReceivedFromPlayers.iterator(); !i.isEnd(); i.next()) {
      if (i.value > mostDamage)
        this.mostDamagePlayerID = i.key;
    }

    //set as out of combat
    this.outOfCombatTimer.stop();
  }
  onInit(): void {
    super.onInit();

    //messages
    this.subscribe(Messages.MONSTER_KILLED);
  }
  public onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.MONSTER_KILLED) {
      //console.log('Character received MONSTER_KILLED message with data ' + msg.data);
      //clear attack target (if)
      if (this.attackTarget !== null && this.attackTarget.characterID == (msg.data as Character).characterID)
        this.clearAttackTarget();
    }
  }
  //public setCurrentPathNode(n: PathNode): void { this.currentPathNode = n; }
  /**
   * Allows to set health to concrete number. Used by loading functions to
   * load state to this object from database
   *
   * @param value Whole number, > 1.
   * If is < 1, then is set to 1 (so, if you want to kill the chracter, call kill() or damage())
   * If is grater than max health (considering ALL stat modifiers), then it is set to max health
   */
  public setHealth(value: number): void {
    value = Math.floor(value);
    if (value < 1)
      value = 1;
    //here, the GETTER [maxHealth] MUST BE USED!!!
    if (value > this.maxHealth)
      this._health = this.maxHealth;
    else
      this._health = value;
  }
  /**
   * Sets the base max health to the value.
   * The base max value is health max value, without any stat modifiers
   * @param value     [description]
   * @param healToMax [description]
   */
  public setMaxHealth(value: number, healToMax: boolean): void {
    value = Math.floor(value);
    this._maxHealth = value;

    //here, the GETTER [maxHealth] MUST BE USED!!!
    if (healToMax)
      this._health = this.maxHealth;
    else if (this._health > this.maxHealth)
      this._health = this.maxHealth;
  }
  public setPositionToVAI(x: number, y: number): void {
    this.setPositionToVAIPoint(new Point(x, y));
  }
  public setPositionToVAIPoint(vai: Point): void {
    var squareCentre: Point = PositionModulator.getSquareCentrePosition(vai.x, vai.y);
    this.feetPosition = squareCentre.clone();

    this.updateRenderPosition();
  }
  onUpdate(delta: number, absolute: number) {
    super.onUpdate(delta, absolute);
    this.update();
  }
  /**
   * [update description]
   * @return {Boolean} Useless
   */
  public update(): boolean {
    /*if (this.isDead())
      return false;*/

    this.outOfCombatTimer.update();

    if (this.attackTarget !== null) {//attacking target
      /*  if (this.attackTarget.isDead()) {
          this.attackTarget = null;
          return false;
        }*/
      if (this.attackTimer.update()) {
        this.attackTimer.reset();

        let dmg: number = Functions.generateRandomNumber(0, 5);
        this.attackTarget.damage(dmg, this);
        this.onAttacked();
        //if (this.isPlayer())
        GameConsoleHTML.getInstance().displayMessageRaw(`You dealt ${dmg} damage`);
      }
    }
    return false;
  }
  /**
   * Updates the [draw] position for the texture
   */
  protected updateRenderPosition(): void {
    //update the drawn position
    this.owner.pixiObj.position.x = this.feetPosition.x;
    this.owner.pixiObj.position.y = -1 * this.feetPosition.y;//- this.owner.pixiObj.getBounds().height;

    //origin (if any)
    if (!this.currentAnimationOrigin.isZero()) {
      //console.log('applying otigin position')
      //this.owner.pixiObj.position.x -= this.currentAnimationOrigin.x;
      this.owner.pixiObj.position.y -= this.currentAnimationOrigin.y;
    }
  }
}
