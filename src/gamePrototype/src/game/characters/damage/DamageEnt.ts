import * as ECSA from '../../../../libs/pixi-component';
import { Timer } from '../../../other/Timer';

/**
 * Represents one damage entity
 */
export class DamageEnt extends ECSA.Component {
  /**
   * How long will this be displayed
   */
  private lifeTimer = new Timer(2010);
  public onInit(): void {
    super.onInit();
    /*let objBounds = this.owner.parent.pixiObj.getBounds();
    this.owner.pixiObj.position.x += objBounds.width;*/
  }
  onUpdate(delta: number, absolute: number) {
    super.onUpdate(delta, absolute);

    if (this.lifeTimer.update())
      this.owner.remove();


    // check boundaries
    /*let globalPos = this.owner.pixiObj.toGlobal(new Point(0, 0));
    if (globalPos.x < 0 || globalPos.x > this.scene.app.screen.width || globalPos.y < 0 || globalPos.y > this.scene.app.screen.height) {
    }*/
  }
}
