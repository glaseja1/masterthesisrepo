import * as ECSA from '../../../../libs/pixi-component';
import { Assets, POS_CONSTS, Messages, DIRECTION } from '../../../Constants';
import { Point } from '../../../other/math/geometry/Point';
import { Functions } from '../../../other/Functions';
import { Angle } from '../../../other/math/geometry/Angle';
import { LinkedList } from '../../../other/containers/linkedList/LinkedList';
import { HashSet } from '../../../other/containers/hashSet/HashSet';
import { PositionModulator } from '../../../game/map/PositionModulator';
import { Timer } from '../../../other/Timer';
import { NavigationPathResult } from '../../../game/map/navigation/NavigationPathResult';
import { NavigationMesh } from '../../../game/map/navigation/NavigationMesh';
import { Character } from '../Character';
import { GameConsoleHTML } from '../../../ui/GameConsoleHTML';
import { DroppedItemManager } from '../../../game/map/DroppedItemManager';
import { GameEndedScreenHTML } from '../../../ui/GameEndedScreenHTML';

export class Player extends Character {
  private direction: DIRECTION = null;
  private _isMoving: boolean = false;
  private lerpStartPosition: Point = null;
  private lerpStep: number = 0;
  /**
   * VAIs to which the player shall go (the entire path)
   */
  private moveTargets: LinkedList<Point> = null;

  public constructor(_id: number) {
    super(_id, true);
  }
  private changeAnimTo(ASSET_NAME: string): void {
    this.scene.sendMessage(new ECSA.Message(Messages.ANIM_CHANGE, this, this.owner,
      { data: ASSET_NAME, id: 'player' }));
  }
  private changeDirectionWhenMovingTo(targRealPos: Point): void {
    //calc move angle
    let a: Angle = Angle.createFromPoints(
      new Point(this.feetPosition.x, this.feetPosition.y),
      new Point(targRealPos.x, targRealPos.y));
    //change direction
    let d: DIRECTION = Functions.getDirectionFromAngle(a.getValueDegrees());
    this.changeDirection(d);
    //console.log(this.direction)
  }
  /**
   * Changes the direction of the curent animation
   * @param d [description]
   */
  private changeDirection(d: DIRECTION): void {
    if (this.direction == d)
      return;

    this.direction = d;
    this.scene.sendMessage(new ECSA.Message(Messages.DIRECTION_CHANGE, this, this.owner,
      { data: this.direction, id: 'player' }));
  }
  private getPositionAtFeet(): Point {
    return new Point(
      this.owner.pixiObj.position.x,
      this.owner.pixiObj.position.y + this.owner.pixiObj.getBounds().height);
  }
  /**
   * [lerp description]
   * @param start [description]
   * @param end   [description]
   * @param delta     The time, from interval <0,1>, meaning that 1 is the last tick
   */
  private lerp(start: number, end: number, delta: number): number {
    if (delta > 1) delta = 1;
    return start * (1 - delta) + end * delta;
  }
  /**
   * OVERRIDE
   */
  protected onAttacked(): void {
    this.changeAnimTo(Assets.RESOURCE_ATTACK);
  }
  /**
   * OVERRIDE
   * @return [description]
   */
  onInit(): void {
    super.onInit();

    //mesages
    this.subscribe(Messages.MOVE_TO);
    this.subscribe(Messages.ANIM_ORIGIN);
  }
  /**
   * OVERRIDE
   * @param  msg [description]
   * @return     [description]
   */
  public onMessage(msg: ECSA.Message) {
    if (msg.action === Messages.MOVE_TO) {
      if (this._isMoving)
        return;

      //stop any attacking
      this.clearAttackTarget();

      var vai: Point = msg.data as Point;

      //calc the path
      var destinations: HashSet<number | string> = new HashSet<number | string>();
      destinations.insert(Functions.createPathNodeKeyFromIndexPoint(vai));

      //get current vai position
      var fromVai: Point = PositionModulator.getVirtualArrayIndex(this.feetPosition.x, this.feetPosition.y);
      var startID = Functions.createPathNodeKeyFromIndexPoint(fromVai);

      //console.log(NavigationMesh.getInstance().getAllPathNodes());
      var findRes: NavigationPathResult = NavigationMesh.getInstance().findPath(startID, destinations);
      if (findRes.path.isEmpty() || findRes.path.length() == 1) {
        GameConsoleHTML.getInstance().displayMessageRaw('Can not reach that!');
        return;
      }
      //remove the node we are standing at
      findRes.path.popFront();
      //console.log(findRes.path)
      //get the path  TODO
      let path: LinkedList<Point> = new LinkedList<Point>();
      //convert IDs to VAI
      for (let i = findRes.path.iterator(); !i.isEnd(); i.next()) {
        //console.log(i.value.toString())
        //id to vai
        let tmpVai: Point = Functions.getVaiFromPathNodeKey(i.value as string);
        var squareCentre: Point = PositionModulator.getSquareCentrePosition(tmpVai.x, tmpVai.y);
        //console.log(squareCentre)
        path.pushBack(squareCentre);
      }
      //**************************************************

      this.changeAnimTo(Assets.RESOURCE_WALK);
      //var vais: LinkedList<Point> = msg.data;
      /*console.log('Player received MOVE_TO message with data ' + msg.data);
      console.log('from ' + fromVai.toString())
      console.log('to ' + vai.toString());*/

      this.resetLerp();
      this.moveTargets = path;

      /*console.log('interpolate')
      console.log('from ' + this.lerpStartPosition.toString())
      console.log('to ');*/
      /*for (let i = this.moveTargets.iterator(); !i.isEnd(); i.next())
        console.warn(i.value.toString())*/

      this._isMoving = true;
    }
    else if (msg.action === Messages.ANIM_ORIGIN) {
      //console.log('Player received ANIM_ORIGIN message with data ' + msg.data);
      var origin: Point = msg.data;
      this.currentAnimationOrigin = origin.clone();
      this.updateRenderPosition();
    }
    else
      super.onMessage(msg);
  }
  public onUpdate(delta: number, absolute: number): void {
    super.onUpdate(delta, absolute);

    if (this.moveTargets === null || this.moveTargets.isEmpty())
      return;

    let targRealPos: Point = this.moveTargets.peekFront();
    this.changeDirectionWhenMovingTo(targRealPos);

    if (this.moveTimer.update()) {
      this.moveTimer.reset();
      //move towards
      this.lerpStep += 0.03;
      var newPoint: Point = new Point(
        this.lerp(this.lerpStartPosition.x, this.moveTargets.peekFront().x, this.lerpStep),
        this.lerp(this.lerpStartPosition.y, this.moveTargets.peekFront().y, this.lerpStep)
      );
      this.feetPosition = newPoint;

      this.updateRenderPosition();

      if (this.lerpStep >= 1) {
        this.moveTargets.popFront();
        this.resetLerp();

        if (this.moveTargets.isEmpty()) {
          this._isMoving = false;
          this.changeAnimTo(Assets.RESOURCE_IDLE);

          var vai: Point = PositionModulator.getVirtualArrayIndex(this.feetPosition.x, this.feetPosition.y);
          //console.log("movement stopped at ")
          //console.log(vai)
          if (DroppedItemManager.getInstance().existsItemOn(vai)) {
            DroppedItemManager.getInstance().removeItem(vai)
            GameConsoleHTML.getInstance().displayMessageRaw(`You picked up a bag. ${DroppedItemManager.getInstance().getCnt()} left to pick`);

            if (DroppedItemManager.getInstance().getCnt() == 0)
              GameEndedScreenHTML.getInstance().show();
          }
        }
      }
    }
  }
  private resetLerp(): void {
    this.lerpStep = 0;
    this.lerpStartPosition = this.feetPosition.clone();
  }
}
