import * as ECSA from '../../../libs/pixi-component';

/**
 * NOT USED...DISSICULT TO MAKE WORK IN PIXI
 * Component displaying health bar
 */
export class HealthBar extends ECSA.Component {
  /**
   * The current value
   */
  private value: number = 0;
  public onInit(): void {
    super.onInit();

    let sprite = <ECSA.Sprite>this.owner.pixiObj;
    //sprite.texture = PIXI.Texture.from(`progressbar`);

    sprite.texture.frame.width = 50;
  }
  public onMessage(msg: ECSA.Message): void {
    /*var data = msg.data.data;
    if (msg.action === Messages.DIRECTION_CHANGE) {
      console.log('character animation received DIRECTION_CHANGE message with data ' + data);
      this.changeAnimationToDirection(data);
    }*/
  }
  onUpdate(delta: number, absolute: number) {
    super.onUpdate(delta, absolute);

    /*if (this.lifeTimer.update())
      this.owner.remove();*/

    // check boundaries
    /*let globalPos = this.owner.pixiObj.toGlobal(new Point(0, 0));
    if (globalPos.x < 0 || globalPos.x > this.scene.app.screen.width || globalPos.y < 0 || globalPos.y > this.scene.app.screen.height) {
    }*/
  }
}
