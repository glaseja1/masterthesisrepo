import * as ECSA from '../../../libs/pixi-component';
import * as PIXI from 'pixi.js';
import { HashMap } from '../../other/containers/hashMap/HashMap';
import { Point } from '../../other/math/geometry/Point';
import { Functions } from '../../other/Functions';

export class DroppedItemManager {
  private static instance: DroppedItemManager = undefined;
  private items: HashMap<string, ECSA.GameObject> = new HashMap<string, ECSA.GameObject>();

  public addItem(vai: Point, obj: ECSA.GameObject): void {
    this.items.insert(Functions.hashPoisition(vai.x, vai.y), obj);
  }
  public existsItemOn(vai: Point): boolean {
    return this.items.contains(Functions.hashPoisition(vai.x, vai.y));
  }
  public getCnt(): number {
    return this.items.length();
  }
  public static getInstance(): DroppedItemManager {
    if (this.instance === undefined)
      this.instance = new DroppedItemManager();
    return this.instance;
  }
  public removeItem(vai: Point): void {
    /*console.log(this.items)
    console.log(vai)*/
    this.items.get(Functions.hashPoisition(vai.x, vai.y)).remove();
    this.items.delete(Functions.hashPoisition(vai.x, vai.y));
    //console.log(this.items)
  }
}
