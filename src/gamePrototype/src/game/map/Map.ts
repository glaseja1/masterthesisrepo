import * as PIXI from 'pixi.js';
import * as ECSA from '../../../libs/pixi-component';
import { GraphicsAnimation, ANIMATION_DIRECTION } from '../graphics/animation/GraphicsAnimation';

export class Map extends ECSA.Component {
  private loader: PIXI.Loader;
  private screen: PIXI.Rectangle;
  private stage: PIXI.Container;

  public constructor(stage: PIXI.Container, screen: PIXI.Rectangle, loader: PIXI.Loader) {
    super();

    this.screen = screen;
    this.stage = stage;
    this.loader = loader;
  }

  onInit() {
    console.log('init')
    super.onInit();
    this.subscribe('test');
  }
  onUpdate(delta: number, absolute: number) {
    console.log('update')
  }
}
