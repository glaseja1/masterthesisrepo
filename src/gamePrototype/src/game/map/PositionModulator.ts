import { Point } from '../../other/math/geometry/Point';
import { LineSegment } from '../../other/math/geometry/LineSegment';
import { Rectangle } from '../../other/math/geometry/Rectangle';

/**
 * takes care for recalculating positions.
 *
 * The mgame engine is not grid-based, however,
 * sor some entities (such as floortile textures, mouse clicks, pathNodes),
 * grid is required (for sake of simplicity).
 * Therefore, some class with some grid size constant and recalculating abilities is needed
 *
 * Singleton
 */
export class PositionModulator {
  static readonly MAP_FLOORTILE_W: number = 160;
  static readonly MAP_FLOORTILE_H: number = 79;
  /**
   * protected for mocker being able to inherit
   */
  protected constructor() { }
  /**
   *
   * Protected for being able to mock and test it
   * Given point (world coordinates), finds bounding rectangle around that, and returns array indexes describing upper left corner of rectangle
   * @param  worldX [description]
   * @param  worldY [description]
   * @return        [description]
   */
  protected static getVirtualArrayIndexCandidateForOddRectangles(worldX: number, worldY: number): Point {
    let rectUpperLeftCornerIndexes = new Point(
      Math.floor(worldX / this.MAP_FLOORTILE_W),
      Math.floor(worldY / this.MAP_FLOORTILE_H)
    );

    if (worldX < 0)
      rectUpperLeftCornerIndexes.x = Math.ceil(worldX / this.MAP_FLOORTILE_W);
    if (worldY < 0)
      rectUpperLeftCornerIndexes.y = Math.ceil(worldY / this.MAP_FLOORTILE_H);

    //ConsoleLogger.logDebug(`restX = ${worldX} - ${rectUpperLeftCornerIndexes.x} * ${this.MAP_FLOORTILE_W};`);
    let restX = worldX - rectUpperLeftCornerIndexes.x * this.MAP_FLOORTILE_W;

    //ConsoleLogger.logDebug('rest x: ' + restX);

    if (restX < 0)
      rectUpperLeftCornerIndexes.x--;

    //ConsoleLogger.logDebug(`restY = ${worldY} - ${rectUpperLeftCornerIndexes.y} * ${this.MAP_FLOORTILE_H};`);
    let restY = worldY - rectUpperLeftCornerIndexes.y * this.MAP_FLOORTILE_H;

    //ConsoleLogger.logDebug('rest y: ' + restY);
    if (restY > 0)
      rectUpperLeftCornerIndexes.y++;

    //ConsoleLogger.logDebug(rectUpperLeftCornerIndexes.toString());
    return rectUpperLeftCornerIndexes;
  }
  /**
   * Given virtual array index, returns point (in world coordinates) which is
   * at centre of the floortile
   * @param  vaiX the virtual array index
   * @param  vaiY the virtual array index
   * @param  TILE_W custom width of tile (used mainly for minimap / minified tiles)
   * @param  TILE_H custom height of tile (used mainly for minimap / minified tiles)
   * @return        [description]
   */
  public static getSquareCentrePosition(vaiX: number, vaiY: number,
    TILE_W: number = this.MAP_FLOORTILE_W, TILE_H: number = this.MAP_FLOORTILE_H): Point {
    //get the square top-left corner
    let corner = new Point(
      vaiX * TILE_W / 2,
      vaiY * TILE_H / 2
    );
    corner.x += vaiY * (TILE_W / 2);
    corner.y -= vaiX * (TILE_H / 2);

    //move it to be in centre
    corner.x += TILE_W / 2;
    corner.y -= TILE_H / 2;
    return corner;
  }
  /**
   * Just a helper. see getSquareCentrePosition()
   * @param  vai the virtual array index point
   * @param  TILE_W custom width of tile (used mainly for minimap / minified tiles)
   * @param  TILE_H custom height of tile (used mainly for minimap / minified tiles)
   * @return        [description]
   */
  public static getSquareCentrePositionFromPoint(vai: Point,
    TILE_W: number = this.MAP_FLOORTILE_W, TILE_H: number = this.MAP_FLOORTILE_H): Point {
    return this.getSquareCentrePosition(vai.x, vai.y, TILE_W, TILE_H);
  }
  /**
   * Given the world position, will always return the virtual array index of
   * the square which is located at the position
   * @param  x the world X coordinate
   * @param  y the world Y coordinate
   * @return   [description]
   */
  public static getVirtualArrayIndex(x: number, y: number): Point {
    let p = new Point();

    //get upper left corner of rectangle in which the point is.
    //(much easier than directly computing virtual array index)
    let rectUpperLeftCorner: Point = this.getVirtualArrayIndexCandidateForOddRectangles(x, y);

    //using the rectangle upper left corner indexes, determine
    //the related virtual array index
    let vArrayIndexCandidate = new Point();
    vArrayIndexCandidate.x = rectUpperLeftCorner.x;
    //for each y increment, the x will change aswell
    vArrayIndexCandidate.x -= rectUpperLeftCorner.y;
    //*************
    vArrayIndexCandidate.y = rectUpperLeftCorner.y;
    //for each x increment, the y will change aswell
    vArrayIndexCandidate.y += rectUpperLeftCorner.x;

    //cretae the rectangle object
    let rect: Rectangle = new Rectangle(
      rectUpperLeftCorner.x * this.MAP_FLOORTILE_W,
      rectUpperLeftCorner.y * this.MAP_FLOORTILE_H,
      this.MAP_FLOORTILE_W,
      this.MAP_FLOORTILE_H
    );
    /*ConsoleLogger.logDebug('bounding rectangle: ' + rect.toString());
    ConsoleLogger.logDebug('bounding rectangle index: ' + rectUpperLeftCorner.toString());*/

    let rectX: number = vArrayIndexCandidate.x * this.MAP_FLOORTILE_W;
    //we want the spper left corner of rectangle, therefore add 1
    let rectY: number = vArrayIndexCandidate.y * this.MAP_FLOORTILE_H;

    //ConsoleLogger.logDebug('virtual array index candidate: ' + vArrayIndexCandidate.toString());
    //form outer rectangle
    //no tneeded for calculation...
    //let outerRect = new Rectangle(rectX, rectY, MAP_FLOORTILE_W, MAP_FLOORTILE_H);

    let rectHalfX: number = rect.x + rect.w / 2;
    let rectHalfY: number = rect.y - rect.h / 2;

    //check in which quadrant of the rectangle does the point lay
    if (x >= rectHalfX) {
      if (y > rectHalfY) {
        //I quadrant
        //ConsoleLogger.logDebug('quadrant I');

        //form line and ask of what side is the point...
        let l = new LineSegment(new Point(rectHalfX, rect.y), new Point(rect.getRight(), rectHalfY));
        let pointReference = new Point(rect.getRight(), rect.y);
        if (l.areTwoPointsOnSameSide(pointReference, new Point(x, y), true)) {
          //the point is outside this isometric square
          return new Point(vArrayIndexCandidate.x, vArrayIndexCandidate.y + 1);
        }
        else
          return vArrayIndexCandidate;
      }
      else {
        //IV quadrant
        //ConsoleLogger.logDebug('quadrant IV');

        //form line and ask of what side is the point...
        let l = new LineSegment(
          new Point(rectHalfX, rect.getDown()),
          new Point(rect.getRight(), rectHalfY));
        let pointReference = new Point(rect.getRight(), rect.getDown());
        //ConsoleLogger.logDebug(l.toString());
        /*ConsoleLogger.logDebug(`line: ${l.toString()}`);
        ConsoleLogger.logDebug(`reference point: ${pointReference.toString()}`);
        ConsoleLogger.logDebug(`${(new Point(x, y)).toString()}, ${pointReference.toString()}`)
        ConsoleLogger.logDebug(`${l.areTwoPointsOnSameSide(new Point(x, y), pointReference)}`)*/

        if (l.areTwoPointsOnSameSide(pointReference, new Point(x, y), true)) {
          //the point is outside this isometric square
          //ConsoleLogger.logDebug('point outside square');
          return new Point(vArrayIndexCandidate.x + 1, vArrayIndexCandidate.y);
        }
        else
          return vArrayIndexCandidate;
      }
    }
    else {
      if (y > rectHalfY) {
        //II quadrant
        //ConsoleLogger.logDebug('quadrant II');

        //form line and ask of what side is the point...
        let l = new LineSegment(new Point(rect.x, rectHalfY), new Point(rectHalfX, rect.y));
        let pointReference = new Point(rect.x, rect.y);
        //ConsoleLogger.logDebug(l.toString());
        //ConsoleLogger.logDebug(pointReference.toString());
        if (l.areTwoPointsOnSameSide(pointReference, new Point(x, y), true)) {
          //the point is outside this isometric square
          return new Point(vArrayIndexCandidate.x - 1, vArrayIndexCandidate.y);
        }
        else
          return vArrayIndexCandidate;
      }
      else {
        //III quadrant
        //ConsoleLogger.logDebug('quadrant III');

        //form line and ask of what side is the point...
        let l = new LineSegment(new Point(rectHalfX, rect.getDown()), new Point(rectX, rectHalfY));
        let pointReference = new Point(rect.x, rect.getDown());

        //ConsoleLogger.logDebug(l.toString());
        //ConsoleLogger.logDebug(`${(new Point(x, y)).toString()}, ${pointReference.toString()}`)
        //ConsoleLogger.logDebug(`${l.areTwoPointsOnSameSide(new Point(x, y), pointReference)}`)
        if (l.areTwoPointsOnSameSide(pointReference, new Point(x, y), true)) {
          //the point is outside this isometric square
          return new Point(vArrayIndexCandidate.x, vArrayIndexCandidate.y - 1);
        }
        else
          return vArrayIndexCandidate;
      }
    }
    throw new Error("never gets here...");
  }
  public static getVirtualArrayIndexPoint(inP: Point): Point {
    return this.getVirtualArrayIndex(inP.x, inP.y);
  }
  /**
   * Given virtual array index, returns world coordinates of the square upper left corner
   * @param  vaiIndexX [description]
   * @param  vaiIndexY [description]
   * @param  TILE_W custom width of tile (used mainly for minimap / minified tiles)
   * @param  TILE_H custom height of tile (used mainly for minimap / minified tiles)
   * @return           [description]
   */
  public static getWorldPositionRecctangleUpperLeftCorner(vaiIndexX: number, vaiIndexY: number,
    TILE_W: number = this.MAP_FLOORTILE_W, TILE_H: number = this.MAP_FLOORTILE_H): Point {
    //get the square top-left corner
    let corner = new Point(
      vaiIndexX * TILE_W / 2,
      vaiIndexY * TILE_H / 2
    );
    //magic
    corner.x += vaiIndexY * (TILE_W / 2);
    corner.y -= vaiIndexX * (TILE_H / 2);
    return corner;
  }
}
