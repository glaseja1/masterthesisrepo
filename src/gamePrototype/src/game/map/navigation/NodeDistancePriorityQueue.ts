import { HashMap } from '../../../other/containers/hashMap/HashMap';
import { Pair } from '../../../other/containers/pair/Pair';

/**
 * Binary heap, modified to work as a priority queue
 * Sorts by the second argument
 */
export class NodeDistancePriorityQueue<K, V> {
  /**
   * an array index of the top element
   */
  private top: number = 0;
  /**
   * a function for an array index for the parent element
   */
  private parent = i => ((i + 1) >>> 1) - 1;

  /**
   * a function for an array index for the left element
   */
  private left = i => (i << 1) + 1;

  /**
   * a function for an array index for the right element
   */
  private right = i => (i + 1) << 1;

  /**
   * the heap array
   */
  private heapArr: any = [];
  /**
   * Used for getting an index within the heap array for the specified key
   * value: index within array
   */
  private keyMap: HashMap<K, number> = new HashMap<K, number>();
  /**
   * Function for comparing the values
   */
  private _comparator: (a: V, b: V) => number;

  constructor(comparator: (a: V, b: V) => number = null) {
    var defaultComparator = function(a: V, b: V): number {
      if (a === b) return 0;
      if (a > b) return 1;
      return -1;
    }
    this._comparator = (comparator === null) ? defaultComparator : comparator;
  }
  /**
   * For a key identifier, changes the [value] to a new value.
   * Also sorts automatically all the elements in the container
   * @param  key      [description]
   * @param  newValue [description]
   * @return          [description]
   */
  public changeValue(key: K, newValue: V): boolean {
    var index: number = this.findGetIndex(key);
    if (index === null)
      return false;

    if (this._valuesEqual(this.heapArr[index].value, newValue))
      return false;

    //console.log(`++++    changed ${newValue} to ${this.heapArr[index].value}`);

    var oldValue: V = this.heapArr[index].value;
    //change value in heap
    this.heapArr[index].value = newValue;

    //if previous value was greater than new one
    if (this.isValueGreater(oldValue, this.heapArr[index].value)) {
      //console.log('shift up');
      this._shiftUp(index);
    } else {
      //console.log('shift down');
      this._shiftDown(index);
    }
    return true;
  }
  public isEmpty(): boolean { return this.size() === 0; }
  /**
   * Returns the top element from the container but does not modify the container in any way
   * @return [description]
   */
  public peek(): Pair<K, V> {
    var v = this.heapArr[this.top];
    return (v === undefined) ? null : new Pair<K, V>(v.key, v.value);
  }
  /**
   * Removes the top element from the container and returns it
   * Can return null, if empty
   * @return [description]
   */
  public pop(): Pair<K, V> {
    const poppedValue: Pair<K, V> = this.peek();
    if (poppedValue === null)
      return null;

    const bottom: number = this.size() - 1;
    if (bottom > this.top) {
      this._swap(this.top, bottom);
    }
    this.heapArr.pop();
    this._shiftDown(this.top);

    this.keyMap.delete(poppedValue.first);
    return poppedValue;
  }
  public push(k: K, val: V): boolean {
    //if key exists
    if (this.keyMap.contains(k))
      return false;

    this.heapArr.push({ key: k, value: val });
    this.keyMap.insertOverwrite(k, this.size() - 1);
    this._shiftUp(this.size() - 1);
    return true;
  }
  public size(): number { return this.heapArr.length; }
  /**
   * Returns an index within the heap array for the specified key
   * @param  key [description]
   * @return     [description]
   */
  private findGetIndex(key: K): number {
    if (!this.keyMap.contains(key)) return null;
    return this.keyMap.get(key);
  }
  /**
   * Tells whether the value of one node is grater than the value of the another node
   * @param  i The index of the node within the heap array
   * @param  j The index of the node within the heap array
   * @return   [description]
   */
  private isGreater(i: number, j: number): boolean {
    return this._comparator(this.heapArr[i].value, this.heapArr[j].value) === 1;
  }
  private isValueGreater(valueA: V, valueB: V): boolean {
    return this._comparator(valueA, valueB) === -1;
  }
  private _valuesEqual(valueA: V, valueB: V): boolean {
    return this._comparator(valueA, valueB) === 0;
  }
  /**
   * Swaps two nodes in the heap
   * @param i Index of the node within the heap array
   * @param j Index of the node within the heap array
   */
  private _swap(i: number, j: number): void {
    this.keyMap.insertOverwrite(this.heapArr[i].key, j);
    this.keyMap.insertOverwrite(this.heapArr[j].key, i);

    var tmp = this.heapArr[j];
    this.heapArr[j] = this.heapArr[i];
    this.heapArr[i] = tmp;

    //[this.heapArr[i], this.heapArr[j]] = [this.heapArr[j], this.heapArr[i]];
  }
  /**
   *
   * @param nodeIndex Index of the node in the heap array
   */
  private _shiftUp(nodeIndex: number): void {
    let node: number = nodeIndex;
    while (node > this.top && this.isGreater(node, this.parent(node))) {
      this._swap(node, this.parent(node));
      node = this.parent(node);
    }
  }
  /**
   *
   * @param nodeIndex Index of the node in the heap array
   */
  private _shiftDown(nodeIndex: number): void {
    let node: number = nodeIndex;
    while (
      (this.left(node) < this.size() && this.isGreater(this.left(node), node)) ||
      (this.right(node) < this.size() && this.isGreater(this.right(node), node))
    ) {
      let maxChild = (this.right(node) < this.size() && this.isGreater(this.right(node), this.left(node))) ? this.right(node) : this.left(node);
      this._swap(node, maxChild);
      node = maxChild;
    }
  }
}
