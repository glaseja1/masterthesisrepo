import { GameObject } from '../../GameObject';
import { HashMap } from '../../../other/containers/hashMap/HashMap';
import { Point } from '../../../other/math/geometry/Point';
import { NavigationMesh } from './NavigationMesh';
import { PathNodeTransition } from './PathNodeTransition';
import { PositionModulator } from '../PositionModulator';

export class PathNode extends GameObject {
  private targetPathNodes: HashMap<number | string, PathNodeTransition> = new HashMap<number | string, PathNodeTransition>();
  public isPlayerSpawn: boolean = false;

  /**
   * creates basic pathNode, with Bumper collision of predefined size for pathNodes
   * @param {[type]} ID As ID pass here string. A number would be converted to string anyway, as
   * it is needed for hashing the value
   */
  constructor(ID: number | string) {
    super(ID);
  }
  /**
   * Creates transition from this node to another node
   * @param {PathNode} targetNode           [description]
   * @param {[type]} pathNodeTransition [description]
   */
  public addPathNodeLink(targetNode: PathNode, pathNodeTransition: PathNodeTransition): boolean {
    if (this.id === targetNode.id)
      return false;

    if (this.targetPathNodes.contains(targetNode.id))
      return false;

    this.targetPathNodes.insert(targetNode.id, pathNodeTransition.clone());
    return true;
  }
  /**
   * Disconnect the two nodes (in both way), if they are connected
   * @param n [description]
   * @return number of links removed (so 2 as maximum)
   */
  public disconnectFrom(n: PathNode, twoWay: boolean = true): number {
    var removed: number = 0;
    if (this.targetPathNodes.delete(n.id))
      removed++;

    if (twoWay)
      removed += n.disconnectFrom(this, false);
    return removed;
  }
  /**
   * Given another pathNode,
   * Computes air distance between this and target node, ignoring any transitions (no path finding is done.
   * Returns number of squares (number of nodes) that we would pass through, if we would go
   * from this node to the target one (if the path would exist).
   * Consideres only movements aligned with X and Y axis
   * @param  n [description]
   * @param  round [description]
   * @return   [description]
   */
  public getAirDistanceFrom(n: PathNode, round: boolean): number {
    let p1: Point = PositionModulator.getVirtualArrayIndex(this.x, this.y);
    let p2: Point = PositionModulator.getVirtualArrayIndex(n.x, n.y);

    let xDiff: number = Math.abs(p1.x - p2.x);
    let yDiff: number = Math.abs(p1.y - p2.y);
    if (round)
      return Math.floor(Math.sqrt(xDiff * xDiff + yDiff * yDiff));
    return Math.sqrt(xDiff * xDiff + yDiff * yDiff);
  }
  public getManhattanDistanceFrom(n: PathNode): number {
    let p1: Point = PositionModulator.getVirtualArrayIndex(this.x, this.y);
    let p2: Point = PositionModulator.getVirtualArrayIndex(n.x, n.y);

    let xDiff: number = Math.abs(p1.x - p2.x);
    let yDiff: number = Math.abs(p1.y - p2.y);
    return Math.floor(Math.sqrt(xDiff + yDiff));
  }
  /**
   * [targetPathNodes description]
   * @return {list {nodeID, NodeObject}} [description]
   */
  public getTargetPathNodes(): HashMap<number | string, PathNodeTransition> {
    return this.targetPathNodes;
  }
  public hasTransitionTo(i: number | string): boolean { return this.targetPathNodes.contains(i); }
  /**
   * Returns true, if there is a direct connection between this and the target node.
   * You can use argument to specify, whether two way link is required
   *
   * If you pass as argument the node itself, false is returned (because connection to itself is not possible, unless you hack this class code...)
   * @param  p                 [description]
   * @param  requireTwoWayLink If true, node A must be directly connected to B and vice versa in order for true to be returned
   * If false, then only the object on which you call this must have connection to the argument node in order for true to be returned
   * @return                   [description]
   */
  public isConnectedTo(p: PathNode, requireTwoWayLink: boolean = true): boolean {
    if (!this.targetPathNodes.contains(p.id))
      return false;
    if (requireTwoWayLink)
      return p.isConnectedTo(this, false);
    return true;
  }
  /**
   * OVERRIDE
   * @return [description]
   */
  public isPathNode(): boolean { return true; }
  public print(): void {
    console.log(`node of id: ${this.id}, target nodes:`);
    for (let i = this.targetPathNodes.iterator(); !i.isEnd(); i.next()) {
      console.log(`\t${i.key}`);
    }
  }
  /**
   * If link (transition) from this node to target path node exists, it is removed.
   * @param nodeID [description]
   */
  public removeTransitionToPathNode(nodeID: string | number, bothWays: boolean = false): boolean {
    if (!this.targetPathNodes.delete(nodeID))
      return false;

    if (bothWays) {
      if (!NavigationMesh.getInstance().existsNode(nodeID))
        return false;

      let tn: PathNode = NavigationMesh.getInstance().getPathNode(nodeID);
      return tn.removeTransitionToPathNode(this.id, false);
    }
    return true;
  }
}
