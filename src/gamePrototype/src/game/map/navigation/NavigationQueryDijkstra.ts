import { NodeDistancePriorityQueue } from './NodeDistancePriorityQueue';
import { HashSet } from '../../../other/containers/hashSet/HashSet';
import { HashMap } from '../../../other/containers/hashMap/HashMap';
import { LinkedList } from '../../../other/containers/linkedList/LinkedList';
import { PathNode } from './PathNode';
import { PRIORITY_QUEUE } from './NavigationMesh';
import { NavigationPathResult } from './NavigationPathResult';

/**
 * A structure to hold data for dijkstra query.
 * Holds distances of nodes, how to get to all nodes.
 *
 * Implements the Dijkstra algorithm to process graph.
 * Also allows to find (backtrack) path, if the graph was already precomputed
 *
 * Next, having same start node, but different end node, you can use this instance to get resulting path, which is precomputed
 */
export class NavigationQueryDijkstra {
  /**
   * reference to container containing all existing nodes...
   */
  protected nodes: HashMap<number | string, PathNode>;

  protected _startNodeID: string | number;
  protected _BLACK: HashSet<string | number> = new HashSet<string | number>();
  protected _GREY: HashMap<string | number, string | number> = new HashMap<string | number, string | number>();
  /**
   * the priority queue
   * key: nodeID
   * value: the distance for the node
   *
   * All elements are sorted by the [value]
   */
  protected _WHITE: NodeDistancePriorityQueue<string | number, number>;
  /**
   * node distances (key = node ID, value = distance to such a node from start)
   */
  protected _nodeDistances: HashMap<string | number, number> = new HashMap<string | number, number>();
  /**
   * Type of the priority queue to use
   */
  private queue: PRIORITY_QUEUE = PRIORITY_QUEUE.HEAP;

  /**
   * Creates a graph for distances from node A to all other nodes
   * @param startNodeID The ID os the starting node
   * @param nodes reference to container containing all existing nodes...
   */
  public constructor(startNodeID: string | number,
    nodes: HashMap<number | string, PathNode>,
    queue: PRIORITY_QUEUE = PRIORITY_QUEUE.HEAP) {
    this.nodes = nodes;
    this.queue = queue;
    //sorts the nodes by their assigned distance
    var whiteNodeIDComparator = function(a: number, b: number): number {
      if (a === b)
        return 0;
      if (a < b)
        return 1;
      return -1;
    }

    this._startNodeID = startNodeID;

    //priority queue sorting by distance of node
    this._WHITE = new NodeDistancePriorityQueue<string | number, number>(whiteNodeIDComparator);
  }

  protected addToBlack(nodeID: string | number): boolean { return this._BLACK.insert(nodeID); }
  protected addToGrey(from: string | number, to: string | number): void {
    //replace, the value, even when it exists (we found shorter path)
    this._GREY.insertOverwrite(to, from);
  }
  protected addToWhite(nodeID: string | number, distance: number): boolean {
    var res: boolean = this._WHITE.push(nodeID, distance);
    if (!res)
      return false;
    this.changeNodeDistanceIfSmaller(nodeID, distance);
    return res;
  }
  /**
   * In inner distance array changes / updates the distance value for a node.
   * Also updates the value in WHITE priority queue
   * @param  {Integer} NodeID      The ID of node
   * @param  {double} newDistance The new distance of node
   * @return {boolean}             Whether value was changed / added
   */
  protected changeNodeDistanceIfSmaller(nodeID: string | number, newDistance: number): boolean {
    if (!this._nodeDistances.contains(nodeID)) {
      this._nodeDistances.insert(nodeID, newDistance);
      return true;
    }

    if (this._nodeDistances.get(nodeID) <= newDistance)
      return false;

    this._nodeDistances.insertOverwrite(nodeID, newDistance);

    //change it in white queue aswell
    this._WHITE.changeValue(nodeID, newDistance);
    return true;
  }
  /**
   * @param nodeID ID of the node
   * @return The distance from start node to the requested note.
   * If Requested node can NOT be reached, throws exception
   */
  protected getNodeDistance(nodeID: string | number): number {
    if (!this._nodeDistances.contains(nodeID))
      throw new Error(`Node with id ${nodeID} can not be reached from ${this._startNodeID}`);
    return this._nodeDistances.get(nodeID);
  }
  /**
    * Uses Dijkstra BFS algorithm.
    * The graph must be precomputed to make this function work
    *
    * Use this to get the path from start point to closest ofreference to container containing all existing nodes... the the destination points.
    * You can use it multiple times for different destination points, once the arrays are filled through algorithm
    * Not optimalised for multiple same queries, not using any database
    * @param destinationNodes group of node IDs where you want to finish
    * @return list containing 'path' and 'distance'.
    * 'path' is list of IDs if nodes, forming path from first node to the last one, included.
    If no such a path exists, allthough start and end nodes do exist, it is empty list
    * 'distance' is the total distance from start to the end node. If no path exists, it is 0
    * @throw If any node ID is not found in the array. For non existing start / end nodes make check before you call this
    */
  public getPath(destinationNodes: HashSet<number | string>): NavigationPathResult {
    //find the closest node from the end nodes
    var smallestDistanceNodeFound: boolean = false;
    var smallestDistanceNode_ID: string | number;
    var smallestDistanceNode_distance: number = -1;

    for (let it = destinationNodes.iterator(); !it.isEnd(); it.next()) {
      var distance: number = 0;
      try {
        distance = this.getNodeDistance(it.value);
      }
      catch (e) {//the node is unreachable from start node
        continue;
      }

      if (smallestDistanceNode_distance > distance || !smallestDistanceNodeFound) {
        smallestDistanceNode_ID = it.value;
        smallestDistanceNode_distance = distance;
        smallestDistanceNodeFound = true;
      }
    }
    if (!smallestDistanceNodeFound)
      return new NavigationPathResult(); //no such connected path exists

    //console.log(`smallest node distance from all target nodes: ${smallestDistanceNode_ID}`);
    //**********************************
    var returnedPath = new LinkedList<string | number>();
    if (!this.nodes.contains(smallestDistanceNode_ID))
      throw new Error(`Node with ID ${smallestDistanceNode_ID} does not exist`);

    //push if to the front of array
    returnedPath.pushFront(smallestDistanceNode_ID);

    //look for the smallest distance node as end node
    var currentID: string | number = smallestDistanceNode_ID;

    while (currentID !== this._startNodeID) {
      if (!this._GREY.contains(currentID)) {
        //no such connected path exists
        return new NavigationPathResult();
      }
      currentID = this._GREY.get(currentID);

      if (!this.nodes.contains(currentID))
        throw new Error(`Node with ID ${currentID} does not exist`);

      returnedPath.pushFront(currentID);
    }
    return NavigationPathResult.create(returnedPath, smallestDistanceNode_distance);
  }
  protected isInBLACK(nodeID: string | number): boolean { return this._BLACK.contains(nodeID); }
  /**
   * runs dijkstra through entire graph ->
   * precomputes entire graph and allows to later on get path to any destination,
   * as long as the start ID stays the same and graph does not change
   * @param  startID [description]
   * @return         [description]
   */
  public precomputeEntireGraph(): void {
    //set distance to 0
    this.addToWhite(this._startNodeID, 0);

    var currentlyExploredNode_ID: string | number = -1;
    while (this.sizeWhite() > 0) {
      //get top one and also remove it
      currentlyExploredNode_ID = this.WHITE_popTop().first;

      var nodeObj = this.nodes.get(currentlyExploredNode_ID);

      var currentDistanceOfNode = this.getNodeDistance(currentlyExploredNode_ID);

      //console.log(`*** Exploring: [ID: ${currentlyExploredNode_ID}] [CurrDistance: ${currentDistanceOfNode}]`);

      //mark as explored
      this.addToBlack(currentlyExploredNode_ID);

      //get transitions
      var transitions = nodeObj.getTargetPathNodes();

      //loop for each transitions
      for (let it = transitions.iterator(); !it.isEnd(); it.next()) {
        var targetNodeID = it.key;

        if (this.isInBLACK(targetNodeID))
          continue; //already explored

        //find the node
        if (!this.nodes.contains(targetNodeID))
          throw new Error(`Node ${targetNodeID} does not exist`);

        var transitionDistance = it.value.distance;

        //current distance + new distance
        var newDistance: number = currentDistanceOfNode + transitionDistance;

        if (this.changeNodeDistanceIfSmaller(targetNodeID, newDistance)) {
          //we have shorter path
          this.addToWhite(targetNodeID, newDistance);
          //console.log('_createQueryAccordingToGraph: adding to grey ' + currentlyExploredNode_ID + ', ' + targetNodeID);
          this.addToGrey(currentlyExploredNode_ID, targetNodeID);
        }
      }
    }
  }
  protected sizeWhite(): number { return this._WHITE.size(); }
  protected WHITE_popTop() { return this._WHITE.pop(); }
}
