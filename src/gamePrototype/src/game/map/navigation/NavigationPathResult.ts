import { LinkedList } from '../../../other/containers/linkedList/LinkedList';

export class NavigationPathResult {
  public path: LinkedList<number | string> = new LinkedList<string | number>();
  public distance: number = 0;

  public static create(path: LinkedList<number | string>, distance: number): NavigationPathResult {
    var a = new NavigationPathResult();
    a.path = path;
    a.distance = distance;
    return a;
  }
  /**
   * Creates a deep independent copy
   * @return [description]
   */
  public clone(): NavigationPathResult {
    var a: NavigationPathResult = new NavigationPathResult();
    a.path = this.path.clone();
    a.distance = this.distance;
    return a;
  }
}
