import { NODE_TRANSITION } from '../../../Constants';

export class PathNodeTransition {

  private _distance: number;
  private _transitionType: NODE_TRANSITION;

  constructor(distance: number, TRANSITION_TYPE_enum: NODE_TRANSITION = NODE_TRANSITION.MOVE) {
    this._distance = distance;
    this._transitionType = TRANSITION_TYPE_enum;
  }
  public clone(): PathNodeTransition {
    return new PathNodeTransition(this._distance, this._transitionType);
  }
  public get distance(): number { return this._distance; }
  public getTransitionType(): NODE_TRANSITION { return this._transitionType; }
}
