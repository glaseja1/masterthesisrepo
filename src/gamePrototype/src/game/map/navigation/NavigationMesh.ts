import { PathNode } from './PathNode';
import { PathNodeTransition } from './PathNodeTransition';
import { NavigationQueryDijkstra } from './NavigationQueryDijkstra';
import { Functions } from '../../../other/Functions';
import { NavigationPathResult } from './NavigationPathResult';
import { NavigationQueryAStar, HEURISTIC } from './NavigationQueryAStar';
import { Point } from '../../../other/math/geometry/Point';
import { LinkedList } from '../../../other/containers/linkedList/LinkedList';
import { HashSet } from '../../../other/containers/hashSet/HashSet';
import { HashMap, HashMapIterator } from '../../../other/containers/hashMap/HashMap';

export enum ALGORITHM {
  BFS,
  A_STAR
}
export enum PRIORITY_QUEUE {
  HEAP,
  FIBBONACI_HEAP
}
/**
 *
 * Represents navigation map for monsters ans players.
 Can be used for searching path, also remembers (caches) query history and precomputed dijkstra graphs (if dijkstra is used...).
 <b>Precomputed Graphs</b> are used as follows:<br>
 Assume, that query for path from A to D is asked, using Dijkstra, and no precomputed graph with A as start exists yet.
 Then, dijkstra runs and counts distance coeficients for all nodes in graph, with A as start. (O(VERTICES + EDGES))./n
 Then, path is found, if exists (O(VERTICES_IN_PATH + EDGES_IN_PATH))<br>
 <br>
 Whenever another query starting in node A is created, precounted graph is used to return desired path.
 (O(VERTICES_IN_PATH + EDGES_IN_PATH)).
 <br><br><br>

 <b>Query history</b> is used as follows:/n
 Lets say that query for path from A to D is asked, and no such exactly same query (with same algorithm specified) was ever asked before.
 If Dijkstra shall be use, and precomputed graph exists, it is used.
 If the graph does not exist, it is created, anyway returning the desired path.
 O(VERTICES + EDGES) + O(VERTICES_IN_PATH + EDGES_IN_PATH) if no precomputed graph exists<br>
 O(VERTICES_IN_PATH + EDGES_IN_PATH) if precounted graph exists<br>
 <br>
 If such a query is asked again (the exactly same query, with same algorithm), result is only returned, making it O(1) <br>
 theoretically, the resulting list of path is copied, however, as it is return value, move is probably used by compiler,<br>
 so it might be O(LENGTH_OF_PATH).<br>
 Even though, extra checks and conditions used in path backtracking are ommited, as function for backtracking is NOT used.

 If A* algorithm is supposed to be used, no precomputed graph can be used because of the nature of the A* and heuristic function.
 So it is computed always you call it, with exception of repetivive queries, which are returned from cache
 <br>
 <br>
 All precomputed data is erased, if node is added to this class, or when DeletePrecountedGraphs() is called,<br>
 wich is called whenever a graph change is done.

 <b>If you modify Node pointer from inside this class, after precounted graps are done, then behaviour is undefined</b>
 *
 */
export class NavigationMesh {
  private static instance: NavigationMesh = undefined;
  /**
   * All pathnodes sorted by their ID
   */
  private pathNodes: HashMap<number | string, PathNode> = new HashMap<number | string, PathNode>();
  /**
   * All precounted dijkstra graphs, sorted by starting node ID
   */
  private _precomputedGraphs: HashMap<number | string, NavigationQueryDijkstra> = new HashMap<number | string, NavigationQueryDijkstra>();
  /**
   * All queried path queries. Start node and destintion nodes are hashed
   * and they form the key.
   * The value is the resulting path data that was returned in past.
   */
  private _queryCache: HashMap<string, NavigationPathResult>;

  constructor() {
    this._queryCache = new HashMap<string, NavigationPathResult>();
  }

  /**
   * Adds node to the navigation mesh. All precounted data is erased, if the node is really added
   * @param {[type]} node Pointer to Node to add. Pointer is <b>freed</b> upon destruction of this navMesh.
   * Node contains transitions inside already
   * @return Whether Node was added or not. If Node was not added, <b>the pointer is not deleted</b>
   */
  public addNode(node: PathNode): boolean {
    if (this.pathNodes.contains(node.id))
      return false;

    this.pathNodes.insert(node.id, node);

    //all precounted graphs must be removed
    this.deletePrecountedGraphs();
    return true;
  }
  /**
   *
   * @param key [description]
   * @param r   is cloned
   */
  private addToQueryHistory(key: string, r: NavigationPathResult): void {
    this._queryCache.insert(key, r.clone());
  }
  /**
   * creates new precompuuted query and runs
   *  dijkstra through entire graph
   * @param  startID The iD os node where to start
   * @return         [description]
   */
  private createQueryAccordingToGraph(startID: string | number): NavigationQueryDijkstra {
    var query: NavigationQueryDijkstra = new NavigationQueryDijkstra(startID, this.pathNodes);
    query.precomputeEntireGraph();
    return query;
  }
  /**
   * from a requested start node and set of end nodes,
   * creates an unique key identiying this query
   * @param  startNodeID  The ID of starting node
   * @param  destinations [description]
   * @param  a            [description]
   * @return              [description]
   */
  private createUniqueKeyFromPathFindQuery(startNodeID: string | number, destinations: HashSet<number | string>, a: ALGORITHM): string {
    var str: string = `${startNodeID}`;
    for (let it = destinations.iterator(); !it.isEnd(); it.next())
      str += `x${it.value}`;
    return `${str}${a}`;
  }
  /**
   * deletes node from navMesh.
   * Does NOT unlink the node from others!!!
   * Deletes all cache (as graph was changed...)
   * @param  nodeID [description]
   * @return        [description]
   */
  public deleteNode(nodeID: string | number): boolean {
    if (!this.existsNode(nodeID))
      return false;
    this.pathNodes.delete(nodeID);
    this.deletePrecountedGraphs();
    return true;
  }
  /**
   * Deletes all precounted graphs and query history.
   * Used when node transitions change.
   * When adding a node, this is called automatically
   */
  public deletePrecountedGraphs(): void {
    this._precomputedGraphs.clear();
    this._queryCache.clear();
  }
  public existsNode(nodeID: string | number): boolean { return this.pathNodes.contains(nodeID); }
  public existsNodeOnVAI(vai: Point): boolean { return this.existsNode(Functions.createPathNodeKeyFromIndexPoint(vai)); }
  public existsPath(startID: number | string, destID: number | string): boolean {
    if (startID == destID) return true;
    var r: NavigationPathResult = this.findPath(startID, HashSet.createFromItem(destID));
    return r.distance !== 0;
  }
  /**
    * Given a set of destination nodes, finds path from start node to any of the destination ones,
    * looking for shortest path.
    * First, looks to database of queries, then to database of precounted graphs, else, runs dijkstra
    * @param startID Set of IDs of Nodes where the path begins
    * @param destination ID of Node where we want to get to
    * @param a           Algorithm to use
    * @param queue       Priority queue to use. This is only compatible with A*. When using BFS, heap only can be used
    * @return list containing 'path' and 'distance'.
    * 'path' is list of IDs if nodes, forming path from first node to the last one, included.
    * If no such a path exists, allthough start and end nodes do exist, it is empty list
    * 'distance' is the total distance from start to the end node. If no path exists, it is 0
    * @throw If start or end node do not exist
    * @throw If any of destination nodes do not exist
    *
    * Note: If you try to go from node A to node A, then it returns empty path, and therefore,
    * it is not possible to distinquish from result whether path does not exist, or we are just in
    * the final node.
    * For that case, caller should perform trivial check for whether start is among destinations,
    * and according to that do some game logic, if needed...
    */
  public findPath(startID: number | string,
    destinations: HashSet<number | string>,
    a: ALGORITHM = ALGORITHM.BFS,
    queue: PRIORITY_QUEUE = PRIORITY_QUEUE.HEAP): NavigationPathResult {
    //check if this exact same query was asked before
    //hash it and look up to database
    var key: string = this.createUniqueKeyFromPathFindQuery(startID, destinations, a);
    //find if the query is already exactly the same in history
    if (this._queryCache.contains(key)) {
      //console.log('used cache');
      //exactly same query was asked before, and we can return the result
      //The start / end nodes ARE valid, as the result was in past returned, and graph did NOT change since,
      //as it is still in history
      let res: NavigationPathResult = this._queryCache.get(key);
      //important to clone it
      return res.clone();
    }

    //if start is among destinations, return list with the node
    if (destinations.contains(startID)) {
      //add to history
      var arr = new LinkedList<string | number>();
      arr.pushBack(startID);

      let res: NavigationPathResult = NavigationPathResult.create(arr, 0);
      this.addToQueryHistory(key, res);
      return res;
    }

    //check for start / end correctness
    //check if start node exists
    if (!this.existsNode(startID))
      throw new Error(`Node with ID ${startID} does not exist`);

    //check if all destinations exist
    for (let it = destinations.iterator(); !it.isEnd(); it.next()) {
      if (!this.existsNode(it.value))
        throw new Error(`Node with ID ${it.value} does not exist`);
    }

    if (a === ALGORITHM.A_STAR) {
      var navigationQuery1: NavigationQueryAStar = new NavigationQueryAStar(
        startID,
        this.pathNodes,
        HEURISTIC.AIR_DISTANCE,
        queue);

      let pathResult1: NavigationPathResult = navigationQuery1.findPath(destinations);
      //add to history
      this.addToQueryHistory(key, pathResult1);
      return pathResult1;
    }

    var navigationQuery: NavigationQueryDijkstra = null;
    //find if precomputed graph for start node already exists
    if (this._precomputedGraphs.contains(startID))
      navigationQuery = this._precomputedGraphs.get(startID);
    else {
      //precompute graph with all the distances
      //create new precounted query and run dijkstra through entire graph
      navigationQuery = this.createQueryAccordingToGraph(startID);
      this._precomputedGraphs.insert(startID, navigationQuery);
    }

    var pathResult: NavigationPathResult = navigationQuery.getPath(destinations);

    //add to history
    this.addToQueryHistory(key, pathResult);
    return pathResult;
  }
  /**
   * returns all pathNdoes in the navmesh.
   * They are returned as REFERENCE, so be carefull not to change it.
   * It is here for the needs of map saving
   * @return [description]
   */
  public getAllPathNodes(): HashMap<number | string, PathNode> { return this.pathNodes; }
  public static getInstance(): NavigationMesh {
    if (this.instance === undefined)
      this.instance = new NavigationMesh();
    return this.instance;
  }
  /**
   * Throws an exception, if he requested node does not exist
   * @param  nodeID [description]
   * @return        [description]
   */
  public getPathNode(nodeID: string | number): PathNode {
    if (!this.existsNode(nodeID))
      throw new Error(`Node with ID ${nodeID} does not exist`);
    return this.pathNodes.get(nodeID);
  }
  /**
   * If there are no nodes available, returns null
   * @return A randomly chosen path node
   */
  public getRandomPathNode(): PathNode {
    if (this.pathNodes.isEmpty())
      return null;
    return this.pathNodes.getAny();
  }
  private _getTargetPathNodes(nodeID: string | number): HashMap<number | string, PathNodeTransition> {
    if (!this.existsNode(nodeID))
      throw new Error(`Node with ID ${nodeID} does not exist`);
    return this.pathNodes.get(nodeID).getTargetPathNodes();
  }
  /**
   * prints all added pathnodes. Used for visualization
   */
  public print(): void {
    for (let i = this.pathNodes.iterator(); !i.isEnd(); i.next())
      i.value.print();
  }
}
