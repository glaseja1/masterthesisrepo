import { NodeDistancePriorityQueue } from './NodeDistancePriorityQueue';
import { HashSet } from '../../../other/containers/hashSet/HashSet';
import { HashMap } from '../../../other/containers/hashMap/HashMap';
import { LinkedList } from '../../../other/containers/linkedList/LinkedList';
import { PathNode } from './PathNode';
import { NavigationQueryDijkstra } from './NavigationQueryDijkstra';
import { PRIORITY_QUEUE } from './NavigationMesh';
import { NavigationPathResult } from './NavigationPathResult';

export enum HEURISTIC {
  AIR_DISTANCE,
  AIR_DISTANCE_ROUNDED,
  MANHATAN_DISTANCE
}

/**
 * A structure to hold data for A* query.
 * Holds distances of nodes, how to get to all nodes.
 */
export class NavigationQueryAStar extends NavigationQueryDijkstra {
  /**
   * The type of heuristic to be used
   */
  private heuristic: HEURISTIC = HEURISTIC.AIR_DISTANCE;
  private hValues: HashMap<string | number, number> = new HashMap<string | number, number>();

  /**
   * Creates a graph for distances from node A to all other nodes
   * @param startNodeID The ID os the starting node
   * @param nodes       [description]
   * @param h           [description]
   */
  constructor(startNodeID: string | number, nodes: HashMap<number | string, PathNode>,
    h: HEURISTIC = HEURISTIC.AIR_DISTANCE,
    queue: PRIORITY_QUEUE = PRIORITY_QUEUE.HEAP
  ) {
    super(startNodeID, nodes, queue);
    this.heuristic = h;
  }
  private backtrackPath(finalNodeID: string | number): NavigationPathResult {
    var res: NavigationPathResult = new NavigationPathResult();
    res.distance = this._nodeDistances.get(finalNodeID);

    //push it to the front of array
    res.path.pushFront(finalNodeID);

    //look for the smallest distance node as end node
    var currentID: string | number = finalNodeID;
    while (currentID !== this._startNodeID) {
      currentID = this._GREY.get(currentID);
      res.path.pushFront(currentID);
    }
    return res;
  }
  /**
   * Given node ID, if the heuristic value was not computed yet, it is computed.
   * Smallest heuristic value among all target nodes is used
   * @param nodeID [description]
   * @param destinationNodes [description]
   */
  private computeHeuristicValueForNode(nodeID: string | number, destinationNodes: HashSet<number | string>): number {
    if (this.hValues.contains(nodeID))
      return this.hValues.get(nodeID);

    let smallestDistance: number = null;

    var node1: PathNode = this.nodes.get(nodeID);

    for (let i = destinationNodes.iterator(); !i.isEnd(); i.next()) {
      let alternative: number = 0;

      if (this.heuristic === HEURISTIC.AIR_DISTANCE || this.heuristic === HEURISTIC.AIR_DISTANCE_ROUNDED)
        alternative = node1.getAirDistanceFrom(this.nodes.get(i.value), this.heuristic === HEURISTIC.AIR_DISTANCE_ROUNDED);
      else if (this.heuristic === HEURISTIC.MANHATAN_DISTANCE)
        alternative = node1.getManhattanDistanceFrom(this.nodes.get(i.value));

      if (smallestDistance === null || alternative < smallestDistance)
        smallestDistance = alternative;

      //console.log('heuristic distance for ' + nodeID + ': ' + smallestDistance);
    }
    this.hValues.insert(nodeID, smallestDistance);
    return smallestDistance;
  }
  /**
   * Runs a* algorithm and terminates once closest destination is found, or,
   * when entire graph was searched through and no path was found
   * @param nodes all nodes in graph, sorted by their IDs
   */
  public findPath(destinationNodes: HashSet<number | string>): NavigationPathResult {
    //set distance to 0
    this.addToWhite(this._startNodeID, 0);

    var currentlyExploredNode_ID: number | string = -1;
    while (this.sizeWhite() > 0) {
      //get top one and also remove it
      currentlyExploredNode_ID = this.WHITE_popTop().first;

      var nodeObj: PathNode = this.nodes.get(currentlyExploredNode_ID);

      //calculate heuristic value, if not done already
      let heuristicCurrent: number = this.computeHeuristicValueForNode(currentlyExploredNode_ID, destinationNodes);

      var currentDistanceOfNode = this.getNodeDistance(currentlyExploredNode_ID);

      /*      console.log(`*** Exploring:
                [ID: ${currentlyExploredNode_ID}]
                [CurrDistance (without heuristics): ${currentDistanceOfNode}],
                [heuristic value: ${heuristicCurrent}]`);
      */
      //mark as explored
      this.addToBlack(currentlyExploredNode_ID);

      //get transitions
      var transitions = nodeObj.getTargetPathNodes();

      //loop for each transitions
      for (let it = transitions.iterator(); !it.isEnd(); it.next()) {
        var targetNodeID = it.key;

        if (this.isInBLACK(targetNodeID))
          continue; //already explored

        var transitionDistance: number = it.value.distance;

        //reached destination
        if (destinationNodes.contains(targetNodeID)) {
          this._nodeDistances.insertOverwrite(targetNodeID, currentDistanceOfNode + transitionDistance);
          this.addToGrey(currentlyExploredNode_ID, targetNodeID);
          return this.backtrackPath(targetNodeID);
        }
        //find the node
        if (!this.nodes.contains(targetNodeID))
          throw new Error(`Node ${targetNodeID} does not exist`);

        //calculate heuristic value, if not done already
        let heuristic: number = this.computeHeuristicValueForNode(targetNodeID, destinationNodes);

        //current distance + new distance
        var newDistance: number = currentDistanceOfNode + transitionDistance;

        //        console.log(`${targetNodeID}: ${currentDistanceOfNode} + ${transitionDistance} + ${heuristic}`)
        if (this.changeNodeDistanceIfSmaller(targetNodeID, newDistance + heuristic)) {
          //we have shorter path
          this.addToWhite(targetNodeID, newDistance + heuristic);

          //overwrite the distance with value without heuristic value
          this._nodeDistances.insertOverwrite(targetNodeID, newDistance);

          //console.log('_createQueryAccordingToGraph: adding to grey ' + currentlyExploredNode_ID + ', ' + targetNodeID);
          this.addToGrey(currentlyExploredNode_ID, targetNodeID);
        }
      }
    }
    //not found
    return new NavigationPathResult();
  }
}
