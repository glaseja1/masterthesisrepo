import * as PIXI from 'pixi.js';
import * as ECSA from '../libs/pixi-component';
import { GraphicsAnimation } from './game/graphics/animation/GraphicsAnimation';
import { Map } from './game/map/Map';
import { Player } from './game/characters/player/Player';
import { Messages, Assets } from './Constants';
import { PlayerFactory } from './factories/PlayerFactory';
import { MapFactory } from './factories/MapFactory';
import { Point } from './other/math/geometry/Point';
import { PositionModulator } from './game/map/PositionModulator';
import { LinkedList } from './other/containers/linkedList/LinkedList';
import { NavigationMesh } from './game/map/navigation/NavigationMesh';
import { MonsterFactory } from './factories/MonsterFactory';
import { RightClickContextMenuHTML } from './ui/RightClickContextMenuHTML';
import { GameConsoleHTML } from './ui/GameConsoleHTML';

class DOLGame {
  private engine: ECSA.GameLoop;

  constructor() {
    this.engine = new ECSA.GameLoop();

    let canvas = (document.getElementById('gameCanvas') as HTMLCanvasElement);
    this.engine.init(canvas, 800, 600, 1, null, true);

    var self = this;
    this.engine.app.loader
      .reset()
      .add(`player-${Assets.RESOURCE_IDLE}-anim`, './assets/animation/game/map/character/player/idle.json')
      .add(`player-${Assets.RESOURCE_WALK}-anim`, './assets/animation/game/map/character/player/walk.json')
      .add(`player-${Assets.RESOURCE_ATTACK}-anim`, './assets/animation/game/map/character/player/attackMelee.json')

      .add(`player-${Assets.RESOURCE_IDLE}D`, './assets/img/game/map/character/player/idleD.png')
      .add(`player-${Assets.RESOURCE_IDLE}L`, './assets/img/game/map/character/player/idleL.png')
      .add(`player-${Assets.RESOURCE_IDLE}LD`, './assets/img/game/map/character/player/idleLD.png')
      .add(`player-${Assets.RESOURCE_IDLE}LU`, './assets/img/game/map/character/player/idleLU.png')
      .add(`player-${Assets.RESOURCE_IDLE}R`, './assets/img/game/map/character/player/idleR.png')
      .add(`player-${Assets.RESOURCE_IDLE}RD`, './assets/img/game/map/character/player/idleRD.png')
      .add(`player-${Assets.RESOURCE_IDLE}RU`, './assets/img/game/map/character/player/idleRU.png')
      .add(`player-${Assets.RESOURCE_IDLE}U`, './assets/img/game/map/character/player/idleU.png')
      .add(`player-${Assets.RESOURCE_WALK}D`, './assets/img/game/map/character/player/walkD.png')
      .add(`player-${Assets.RESOURCE_WALK}L`, './assets/img/game/map/character/player/walkL.png')
      .add(`player-${Assets.RESOURCE_WALK}LD`, './assets/img/game/map/character/player/walkLD.png')
      .add(`player-${Assets.RESOURCE_WALK}LU`, './assets/img/game/map/character/player/walkLU.png')
      .add(`player-${Assets.RESOURCE_WALK}R`, './assets/img/game/map/character/player/walkR.png')
      .add(`player-${Assets.RESOURCE_WALK}RD`, './assets/img/game/map/character/player/walkRD.png')
      .add(`player-${Assets.RESOURCE_WALK}RU`, './assets/img/game/map/character/player/walkRU.png')
      .add(`player-${Assets.RESOURCE_WALK}U`, './assets/img/game/map/character/player/walkU.png')

      .add(`player-${Assets.RESOURCE_ATTACK}D`, './assets/img/game/map/character/player/attackMeleeD.png')
      .add(`player-${Assets.RESOURCE_ATTACK}L`, './assets/img/game/map/character/player/attackMeleeL.png')
      .add(`player-${Assets.RESOURCE_ATTACK}LD`, './assets/img/game/map/character/player/attackMeleeLD.png')
      .add(`player-${Assets.RESOURCE_ATTACK}LU`, './assets/img/game/map/character/player/attackMeleeLU.png')
      .add(`player-${Assets.RESOURCE_ATTACK}R`, './assets/img/game/map/character/player/attackMeleeR.png')
      .add(`player-${Assets.RESOURCE_ATTACK}RD`, './assets/img/game/map/character/player/attackMeleeRD.png')
      .add(`player-${Assets.RESOURCE_ATTACK}RU`, './assets/img/game/map/character/player/attackMeleeRU.png')
      .add(`player-${Assets.RESOURCE_ATTACK}U`, './assets/img/game/map/character/player/attackMeleeU.png')

      .add(`monster-${Assets.RESOURCE_IDLE}-anim`, './assets/animation/game/map/character/monster/spider/idle.json')
      .add(`monster-${Assets.RESOURCE_WALK}-anim`, './assets/animation/game/map/character/monster/spider/walk.json')
      .add(`monster-${Assets.RESOURCE_IDLE}D`, './assets/img/game/map/character/monster/spider/idleD.png')
      .add(`monster-${Assets.RESOURCE_IDLE}L`, './assets/img/game/map/character/monster/spider/idleL.png')
      .add(`monster-${Assets.RESOURCE_IDLE}LD`, './assets/img/game/map/character/monster/spider/idleLD.png')
      .add(`monster-${Assets.RESOURCE_IDLE}LU`, './assets/img/game/map/character/monster/spider/idleLU.png')
      .add(`monster-${Assets.RESOURCE_IDLE}R`, './assets/img/game/map/character/monster/spider/idleR.png')
      .add(`monster-${Assets.RESOURCE_IDLE}RD`, './assets/img/game/map/character/monster/spider/idleRD.png')
      .add(`monster-${Assets.RESOURCE_IDLE}RU`, './assets/img/game/map/character/monster/spider/idleRU.png')
      .add(`monster-${Assets.RESOURCE_IDLE}U`, './assets/img/game/map/character/monster/spider/idleU.png')
      .add(`monster-${Assets.RESOURCE_WALK}D`, './assets/img/game/map/character/monster/spider/walkD.png')
      .add(`monster-${Assets.RESOURCE_WALK}L`, './assets/img/game/map/character/monster/spider/walkL.png')
      .add(`monster-${Assets.RESOURCE_WALK}LD`, './assets/img/game/map/character/monster/spider/walkLD.png')
      .add(`monster-${Assets.RESOURCE_WALK}LU`, './assets/img/game/map/character/monster/spider/walkLU.png')
      .add(`monster-${Assets.RESOURCE_WALK}R`, './assets/img/game/map/character/monster/spider/walkR.png')
      .add(`monster-${Assets.RESOURCE_WALK}RD`, './assets/img/game/map/character/monster/spider/walkRD.png')
      .add(`monster-${Assets.RESOURCE_WALK}RU`, './assets/img/game/map/character/monster/spider/walkRU.png')
      .add(`monster-${Assets.RESOURCE_WALK}U`, './assets/img/game/map/character/monster/spider/walkU.png')

      .add(`damage`, './assets/img/game/hitmarks/hitSplat.png')
      .add(`block`, './assets/img/game/hitmarks/block.png')
      .add(Assets.TEX_MAP_TILE, './assets/img/game/map/decoration/floor/grass.png')
      .add(`progressbar`, './assets/img/game/HUD/progressbar.png')
      .add(`d-rock1`, './assets/img/game/map/decoration/stone1.png')
      .add(`d-rock2`, './assets/img/game/map/decoration/stone2.png')
      .add(`d-tree1`, './assets/img/game/map/decoration/treeLeaves1.png')
      .add(`d-tree2`, './assets/img/game/map/decoration/treeLeaves2.png')
      .add(`d-tree3`, './assets/img/game/map/decoration/treeLeaves3.png')
      .add(`d-pickup`, './assets/img/game/map/decoration/droppedItem.png')
      .load(() => { // wait for the spritesheet to be loaded
        self.onAssetsLoaded();
      });
  }
  /**
   * Hooks the input events
   */
  public hookKeyEvents(): void {
    var im = this.engine.scene.app.renderer.plugins.interaction;

    var self = this;
    this.engine.scene.stage.pixiObj.interactive = true;
    this.engine.scene.stage.pixiObj.on('mousedown', function(e) {

      //console.log(e)
      let instance: RightClickContextMenuHTML = RightClickContextMenuHTML.getInstance();
      if (e.button === 2) //right click
      {
        instance.setPosition(e.clientX, e.clientY);
        instance.show();
        return;
      }
      instance.hide();

      //console.log('stage click')
      //extract the mouse position
      //console.log(im.mouse.global);
      //calc vai
      var vai: Point = PositionModulator.getVirtualArrayIndex(
        im.mouse.global.x,
        -1 * im.mouse.global.y
      );
      //console.log('click vai ' + vai.toString());

      if (!NavigationMesh.getInstance().existsNodeOnVAI(vai)) {
        console.warn(`No path node on vai ${vai.toString()}`);
        return;
      }
      var squareCentre: Point = PositionModulator.getSquareCentrePosition(vai.x, vai.y);

      /*  var destinations: HashSet<number | string> = new HashSet<number | string>();
        destinations.insert(Functions.createPathNodeKeyFromIndexPoint(vai));
        var startID = Functions.createPathNodeKeyFromIndexPoint(vai);

        NavigationMesh.getInstance().findPath(startID, destintions);*/
      //get the path  TODO
      let path: LinkedList<Point> = new LinkedList<Point>();
      path.pushBack(squareCentre);
      self.engine.scene.sendMessage(new ECSA.Message(Messages.MOVE_TO, null, null, vai));
    });
  }
  public onAssetsLoaded(): void {
    this.engine.scene.clearScene();

    GameConsoleHTML.getInstance().displayMessageRaw('Welcome to the Darkness or Light prototype');
    GameConsoleHTML.getInstance().displayMessageRaw(`You can move the character by clicking on the tile
    where you want to go. Collect the brown bags to finish the level`);

    //register the ECSA input
    this.engine.scene.stage.addComponent(new ECSA.KeyInputComponent());

    //init
    PlayerFactory.init(this.engine.app.loader);
    MonsterFactory.init(this.engine.app.loader);
    MapFactory.init(this.engine.app.loader);

    //add map data
    MapFactory.createMap(this.engine.scene);

    //add player to the game
    PlayerFactory.createPlayer(this.engine.scene);
    //add monster
    MonsterFactory.createMonster(this.engine.scene);

    this.hookKeyEvents();
  }
  private get stage(): ECSA.GameObject { return this.engine.scene.stage; }
}

export default new DOLGame();
