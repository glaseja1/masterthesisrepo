import bpy, math

#hide / show all children recursivelly from render
def hideRender(ob, visibilityStates):
    for child in ob.children:
        #print(child.name)
        #save state
        visibilityStates[child.name] = child.hide_render
         
        child.hide_render = True
        hideRender(child, visibilityStates)

def revertHideRender(ob, visibilityStates):
    for child in ob.children:
        #print(child.name)
        #save state
        child.hide_render = visibilityStates[child.name]                
        revertHideRender(child, visibilityStates)


#show and render origin point (if any)
if bpy.data.objects.get("originReference") is not None:
    bpy.data.scenes[0].render.filepath = 'C:\\tmp\\originPointReference'
    
    #hide objects, but remember their visibility states
    visibilityStates = {}
    hideRender(bpy.data.objects['modelBase'], visibilityStates)
    print(visibilityStates)
    bpy.data.objects['originReference'].hide_render = False
    #render reference point
    bpy.ops.render.render(use_viewport = True, write_still=True, animation=False)

    #unhide
    revertHideRender(bpy.data.objects['modelBase'], visibilityStates)
    bpy.data.objects['originReference'].hide_render = True

for i in range(8):
    print('[+] Current angle: ' + str(i) + '°')
    bpy.data.scenes[0].render.filepath = 'C:\\tmp\\' + str(i) + '_'
    #D.scenes[0].render.filepath = '.\\render\\'

    #set rotation
    bpy.data.objects['modelBase'].rotation_euler = (0, 0, i * 45 * (math.pi/180))

    #render model
    bpy.ops.render.render( use_viewport = True, write_still=True, animation=True)

#reset all
bpy.data.objects['modelBase'].rotation_euler = (0, 0, 0)
print('[+] Finished')
