#!/bin/bash

check_for_istalled_app()
{
  if [ ! -x "$(command -v $1 2>&1)" ]; then
    printf "[$COL_RED-$COL_RESET] '$1' is not installed. Quit\n"
    exit 1
  fi
}
help()
{
  echo "Script for creating class diagram UML variables text from real code. That text can be easily copied to the UML class diagram,
for example at www.draw.io"
  exit 0
}


COL_CYAN="\e[38;5;74m"
COL_GREEN="\e[32m"
COL_RED="\e[31m"
COL_YELLOW="\e[93m"
COL_RESET="\e[0m"
#START
#-------------------------------------------------------
#check for installed applications
#-------------------------------------------------------
check_for_istalled_app "grep"
check_for_istalled_app "sed"
check_for_istalled_app "cat"

#-------------------------------------------------------
#get arguments
#-------------------------------------------------------
if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  help
fi


I=./input
if [ ! -f "$I" ]; then
  printf "[$COL_RED-$COL_RESET] $I: no such file. Quit\n"
  echo "[+] You can type --help or -h for help"
  exit 1
fi

cat "$I" | sed 's/private/-/g' | sed 's/public/+/g' | grep -v '\/'|grep -v '\*' | sed 's/^ *//g' | sed 's/= new.*$//g' | sed 's/protected/#/g'
exit 1

